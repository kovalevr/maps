<?php

use skewer\components\config\PatchPrototype;


class PatchInstall extends PatchPrototype
{

    public $sDescription = 'Добавит таблицы maps и geoObjects ';

    public function execute(){

        $this->executeSQLQuery(
            "CREATE TABLE IF NOT EXISTS `maps` (
             `id` int(11) NOT NULL AUTO_INCREMENT,
             `center` varchar(255) NOT NULL,
             `zoom` varchar(255) NOT NULL,
             PRIMARY KEY (`id`)
           ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );

        $this->executeSQLQuery(
            "CREATE TABLE IF NOT EXISTS `geoObjects` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `map_id` int(11) NOT NULL,
              `latitude` varchar(255) NOT NULL,
              `longitude` varchar(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );

    }

}