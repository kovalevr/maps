<?php


namespace skewer\components\seo;


use skewer\base\orm\state\StateSelect;
use skewer\base\queue\Task;
use skewer\components\search;
use skewer\base\queue as QM;
use skewer\base\SysVar;

/**
 * Задача на обновление поискового индекса
 */
class SearchTask extends Task{

    /** @var StateSelect */
    private $oQuery = null;

    private $iCount = 0;

    private $iLimit = 500;

    /**
     * @inheritdoc
     */
    public function init(){
        SysVar::set('Search.updated', 0);
    }


    /**
     * @inheritdoc
     */
    public function recovery(){
        $this->iCount = SysVar::get('Search.updated');
    }

    /**
     * @inheritdoc
     */
    public function beforeExecute(){

        $this->oQuery = search\SearchIndex::find()->where('status', 0);

    }


    /**
     * @inheritdoc
     */
    public function execute(){

        /**
         * Делаем искуственные ограничения, yii валится на тестовом из-за логера
         */
        if (!$this->iLimit){
            $this->setStatus(static::stInterapt);
            return false;
        }
        $this->iLimit--;

        /** @var search\Row $oRow */
        $oRow = $this->oQuery->each(); //прочитали одну запись поисковой таблицы

        if (!$oRow){
            $this->setStatus( static::stComplete );
            return false;
        }

        //Получили поисковый класс для записи
        $oSearch =  search\Api::getSearch($oRow->class_name);

        if ( !$oSearch )
            return false;

        //обновим запись в поиске
        if ($oSearch->updateByObjectId($oRow->object_id, false))
            $this->iCount++;

        return true;

    }


    /**
     * @inheritdoc
     */
    public function afterExecute(){
        SysVar::set('Search.updated', $this->iCount);
    }

    /**
     * Метод, вызываемый по завершении задачи
     */
    public function complete()
    {
        /**
         * Цепляем задачу на сайтмап, если ее нет
         */
        QM\Api::addTask([
            'class' => '\skewer\components\seo\SitemapTask',
            'priority' => QM\Task::priorityHigh,
            'title' => 'sitemap update'
        ]);
    }


}