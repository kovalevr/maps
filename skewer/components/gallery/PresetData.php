<?php

$aLanguage = array();

$aLanguage['ru']['profile_catalog_name'] = 'Изображения для каталога';
$aLanguage['ru']['format_catalog_mini_name'] = 'Иконка';
$aLanguage['ru']['format_catalog_small_name'] = 'Миниатюра';
$aLanguage['ru']['format_catalog_medium_name'] = 'Большая миниатюра';
$aLanguage['ru']['format_catalog_big_name'] = 'Детальный просмотр';

$aLanguage['ru']['profile_collection_name'] = 'Изображения для коллекций каталог';
$aLanguage['ru']['format_collection_min_name'] = 'для краткого описания и главной';
$aLanguage['ru']['format_collection_med_name'] = 'для подробного описания';
$aLanguage['ru']['format_collection_max_name'] = 'полноразмерное изображение';

$aLanguage['ru']['profile_section_name']        = 'Изображения для фотогалереи';
$aLanguage['ru']['format_section_mini_name']    = 'иконка для фоторамы';
$aLanguage['ru']['format_section_preview_name'] = 'Миниатюра';
$aLanguage['ru']['format_section_med_name']     = 'Детальный просмотр';

$aLanguage['ru']['format_news_mini_name']     = 'иконка для фоторамы';
$aLanguage['ru']['format_news_list_name']     = 'для списка';
$aLanguage['ru']['format_news_on_main_name']  = 'для главной';
$aLanguage['ru']['format_news_big_name']      = 'большое изображение';

$aLanguage['ru']['format_openGraph']          = 'для OpenGraph';
// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['profile_catalog_name'] = 'The images catalog';
$aLanguage['en']['format_catalog_mini_name'] = 'Icon';
$aLanguage['en']['format_catalog_small_name'] = 'Thumbnail';
$aLanguage['en']['format_catalog_medium_name'] = 'Large thumbnail';
$aLanguage['en']['format_catalog_big_name'] = 'Detailed view';

$aLanguage['en']['profile_collection_name'] = 'images for the collectionsг';
$aLanguage['en']['format_collection_min_name'] = 'for a brief description and the main';
$aLanguage['en']['format_collection_med_name'] = 'for a detailed description';
$aLanguage['en']['format_collection_max_name'] = 'larger image';

$aLanguage['en']['profile_section_name']        = 'Images for gallery';
$aLanguage['en']['format_section_mini_name']    = 'icon for fotorama';
$aLanguage['en']['format_section_preview_name'] = 'Thumbnail';
$aLanguage['en']['format_section_med_name']     = 'Detailed view';

$aLanguage['en']['format_news_mini_name']     = 'icon for fotorama';
$aLanguage['en']['format_news_list_name']     = 'for list';
$aLanguage['en']['format_news_on_main_name']  = 'for main page';
$aLanguage['en']['format_news_big_name']      = 'large image';

$aLanguage['en']['format_openGraph']          = 'for OpenGraph';

return $aLanguage;