<?php

namespace skewer\components\lp;


/**
 * Объект - запись для парсера
 * Class Row
 * @package skewer\components\lp
 */
class Row {

    /** @var string Название поля */
    public $title = '';

    /** @var string системное имя поля */
    public $name = '';

    /** @var string значение поля */
    public $value = '';

    /** @var string имя типа поля */
    public $type = '';

} 