<?php

namespace skewer\components\lp;
use skewer\base\section\Tree;
use skewer\base\section\Parameters;
use skewer\base\site_module;
use skewer\base\Twig;

/**
 * Прототип модуля для клиентского модуля
 * Class Prototype
 * @package skewer\components\lp
 */
abstract class PagePrototype extends site_module\page\ModulePrototype implements ModuleInterface {

    /** @var int id раздела */
    var $sectionId;

    /**
     * Отдает id раздела
     */
    public function sectionId() {
        return (int)$this->sectionId;
    }

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        $this->validateProcessCall();

        $aTpl = Parameters::getByName( $this->sectionId(), Api::groupMain, Api::pageTpl, true );
        $sTpl = $aTpl ? $aTpl[ 'show_val' ] : "no tpl for section [$this->sectionId()]";

        $aData = array(
            'css' => Css::getParsedText( $this->sectionId() ),
            'class' => Css::getBlockClass( $this->sectionId() ),
            'text' => $this->getRenderedText(),
            'alias' => Tree::getSectionAlias( $this->sectionId() )
        );

        $sContent = Twig::renderSource( $sTpl, $aData );

        $this->setOut( $sContent );

        return psRendered;

    }

    /**
     * Проверяет возможность запуска модуля в текущем окружении
     * нет родителя - запуск блока из корня - редирект на главную LP
     * @return void
     */
    private function validateProcessCall() {

        $response = \Yii::$app->getResponse();

        if ( $this->oContext->getParentProcess() )
            return;

        $iParentId = Tree::getSectionParent( $this->sectionId() );
        if ( !$iParentId ) {
            $response->setStatusCode(500);
            $response->send();
        }

        $sUrl = \Yii::$app->router->rewriteURL( "[$iParentId]" );
        if ( !$sUrl ) {
            $response->setStatusCode(500);
            $response->send();
        }


        $sAlias = Tree::getSectionAlias( $this->sectionId() );
        if ( $sAlias )
            $sUrl = sprintf( '%s#%s', $sUrl, $sAlias );

        \Yii::$app->getResponse()->redirect($sUrl,'301')->send();

    }

    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    abstract protected function getRenderedText();

}