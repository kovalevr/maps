<?php

namespace skewer\components\ext\field;

/**
 * Редактор "Число"
 */
class IntField extends Prototype {

    function getView() {
        return 'num';
    }

} 