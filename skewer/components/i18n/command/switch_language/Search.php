<?php

namespace skewer\components\i18n\command\switch_language;


use skewer\base\command\Action;
use skewer\components\search\SearchIndex;
use skewer\components\seo\Service;

/**
 * Сброс поиска
 */
class Search extends Action
{
    /**
     * @inheritDoc
     */
    protected function init()
    {

    }

    /**
     * @inheritDoc
     */
    function execute()
    {
        Service::rebuildSearchIndex();
        SearchIndex::update()->set('status',0)->where('status',1)->get();
        Service::updateSearchIndex();
        Service::updateSiteMap();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}