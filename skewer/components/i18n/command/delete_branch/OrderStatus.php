<?php

namespace skewer\components\i18n\command\delete_branch;


use skewer\base\orm\Query;

/**
 * Статусы заказа
 */
class OrderStatus extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {

        Query::DeleteFrom('orders_status_lang')
            ->where('language', $this->getLanguageName())
            ->get();

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}