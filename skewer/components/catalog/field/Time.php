<?php

namespace skewer\components\catalog\field;


class Time extends Prototype {

    protected function build( $value, $rowId, $aParams ) {

        $out = '';

        if ( $value and $value!='00:00:00' ) {
            list( $sH, $sM, ) = explode( ':', $value );
            $out = $sH . ':' . $sM;
        }

        return [
            'value' => $value,
            'html' => $out
        ];
    }
}