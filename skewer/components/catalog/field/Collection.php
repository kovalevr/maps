<?php

namespace skewer\components\catalog\field;

use skewer\components\catalog\Section;
use yii\helpers\ArrayHelper;
use skewer\components\catalog\Parser;

class Collection extends Prototype {

    protected $subSection = 0;


    protected function build( $value, $rowId, $aParams ) {

        $item = $this->getSubDataValue( $value );
        $itemTitle = ArrayHelper::getValue( $item, 'title', '' );
        $itemAlias = ArrayHelper::getValue( $item, 'alias', '' );

        $iSectionId = $this->subSection;

        $href = Parser::buildUrl($iSectionId, $value, $itemAlias);

        $out = $href ? ' <a href="'. $href .'">' . $itemTitle . '</a>' : $itemTitle;

        return [
            'value' => $value,
            'item' => $item,
            'html' => $out
        ];
    }


    protected function load() {

        // todo перенести это в кэш
        // fixme не решена проблема с одноименными полями в разных карточках

        $this->subSection = Section::get4CollectionField( $this->name );

    }

}