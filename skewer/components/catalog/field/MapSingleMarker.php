<?php

namespace skewer\components\catalog\field;

use skewer\base\site\Layer;
use skewer\base\site_module;
use skewer\build\Page\CatalogMaps\Api;
use skewer\build\Page\CatalogMaps\models\GeoObjects;
use skewer\build\Page\CatalogViewer;
use yii\helpers\ArrayHelper;

class MapSingleMarker extends Prototype{

    /**
     * @inheritdoc
     */
    protected function build($value, $rowId, $aParams) {

        $aOut = [
            'value' => $value,
            'html'  => $value
        ];

        if ( $value && ($oGeoObject = GeoObjects::findOne($value)) ){
            $aOut['latitude']  = $oGeoObject->latitude;
            $aOut['longitude'] = $oGeoObject->longitude;
            $aOut['map_id']    = $oGeoObject->map_id;
        }

        return $aOut;
    }

    /**
     * @inheritdoc
     */
    public function afterParseGood($aGoodData, $aFieldData){

        if ( empty($aFieldData['latitude']) && empty($aFieldData['longitude']) && empty($aFieldData['map_id']) ){
            $aFieldData['html'] = $aFieldData['value'] = '';
            return $aFieldData;
        }

        $sCatViewerClassName = site_module\Module::getClass(CatalogViewer\Module::getNameModule(), Layer::PAGE);
        $sCatViewerDir = dirname( \Yii::getAlias('@' . str_replace('\\', '/', $sCatViewerClassName) . '.php') ) . DIRECTORY_SEPARATOR;
        $sCatViewerTplDir = $sCatViewerDir . 'templates'. DIRECTORY_SEPARATOR;

        $aMarker = [
            'latitude'      => ArrayHelper::getValue($aFieldData, 'latitude'),
            'longitude'     => ArrayHelper::getValue($aFieldData, 'longitude'),
            'title'         => ArrayHelper::getValue($aGoodData, 'title', ''),
            'popup_message' => \Yii::$app->getView()->renderPhpFile(
                $sCatViewerTplDir . 'infoWindowInMap.php',
                [ 'aGood' => $aGoodData ]
            )
        ];


        $iMapId = ArrayHelper::getValue($aFieldData, 'map_id');

        $sHtml = Api::buildMap( [ $aMarker ], $iMapId );

        if ( $sHtml === false )
            $aFieldData['html'] = $aFieldData['value'] = '';
        else
            $aFieldData['html'] = $sHtml;

        return $aFieldData;
    }

}