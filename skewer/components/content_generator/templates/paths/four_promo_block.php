<div class="b-promocontact">
    <h2><?=Yii::t('content_generator','title_callback')?></h2>
    <div class="promocontact__telephon">
        <p><?=Yii::t('content_generator','call_me')?>:</p>
        <span>+7 (900) 500-50-00</span>
    </div>
    <div class="promocontact__email">
        <p><?=Yii::t('content_generator','write_me')?>:</p>
        <a href="#">test.adress@mail.ru</a>
    </div>
    <div class="promocontact__soto"><span class="fa fa-phone"></span>
    </div>
</div>