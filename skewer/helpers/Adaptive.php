<?php

namespace skewer\helpers;
use skewer\base\site\Layer;

/**
 * Класс помощник для адаптивного режима
 */
class Adaptive {

    public static function modeIsActive() {
        static $bOn = null;
        if ( is_null($bOn) )
            $bOn = \Yii::$app->register->moduleExists('AdaptiveMode', Layer::PAGE);
        return $bOn;
    }

    /**
     * Выдает текст переданной переменной только если включен адаптивный режим
     *
     * twig:
     *  * {{ Adaptive.write('') }}
     *  * {% if Adaptive.modeIsActive() %}{% endif %}
     *
     * php:
     *  * <?= Adaptive::write('') ?>
     *  * <? if (Adaptive::modeIsActive()): ?><? endif; ?>
     *
     * @param string $sText
     * @return string
     */
    public static function write( $sText ) {
        return self::modeIsActive() ? $sText : '';
    }

}
