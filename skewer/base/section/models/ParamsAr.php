<?php

namespace skewer\base\section\models;

use skewer\base\section\params\Type;
use skewer\base\section;
use skewer\build\Design\Zones\Api;
use yii\base\Event;

/**
 * This is the model class for table "parameters".
 *
 * @property string $id
 * @property integer $parent Раздел
 * @property string $group Группа
 * @property string $name Имя
 * @property string $value Значение
 * @property string $title Название
 * @property integer $access_level Уровень доступа
 * @property string $show_val Текстовое значение
 */
class ParamsAr extends \yii\db\ActiveRecord
{

    /** Виртуальное поле, отсутствующее в таблице, для хранения доп. настроек для редакторов полей в админке */
    public $settings;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'access_level'], 'integer'],
            [['name', 'parent', 'group'], 'required'],
            [['name'], 'unique', 'targetAttribute' => ['name', 'parent', 'group']],
            [['show_val'], 'string'],
            [['group', 'name'], 'string', 'max' => 50],
            [['value'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 50],
            [['parent'], 'integer', 'min' => 1],
            ['value', '\skewer\base\section\params\TemplateValidator', 'when' => function( $model ){
                /** @var ParamsAr $model */
                return ( $model->group == section\Parameters::settings && $model->name == section\Parameters::template  );
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'group' => 'Group',
            'name' => 'Name',
            'value' => 'Value',
            'title' => 'Title',
            'access_level' => 'Access Level',
            'show_val' => 'Show Val',
        ];
    }


    /**
     * Список аттрибутов модели
     * @return array
     */
    public static function getAttributeList(){
        return [
            'id',
            'parent',
            'group',
            'name',
            'value',
            'title',
            'access_level',
            'show_val'
        ];
    }


    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null) {

        $this->parent = (int)$this->parent;
        $this->name = (string)$this->name;
        $this->group = (string)$this->group;
        $this->value = (string)$this->value;
        $this->show_val = (string)$this->show_val;
        $this->title = (string)$this->title;
        $this->access_level = (int)$this->access_level;

        /** заменяем val на show_val в параметрах-зонах  */
        if ( ($this->group === Api::layoutGroupName) && (!in_array($this->name, [Api::layoutTitleName, Api::layoutOrderName])) && ($this->value !== '{show_val}')){
            $this->show_val = $this->value;
            $this->value = '{show_val}';
        }

        return parent::save($runValidation, $attributeNames);
    }


    /**
     * Проверка на висивиг
     * @return bool
     */
    public function isWysWyg(){
        return ( abs($this->access_level) == Type::paramWyswyg );
    }

    /**
     * Использование расширенного значения
     * @return bool
     */
    public function hasUseShowVal(){
        return in_array( abs($this->access_level), Type::getShowValFieldList());
    }

    /**
     * Удаление по разделу
     * @param Event $event
     */
    public static function removeSection( Event $event ){

        self::deleteAll(['parent' => $event->sender->id]);

    }
    
    public function afterSave($insert, $changedAttributes){
        TreeSection::updateLastModify($this->parent);
        section\ParamCache::clear();
    }
    
    
    public function afterDelete(){
        TreeSection::updateLastModify($this->parent);
        section\ParamCache::clear();
    }
    
    public static function deleteAll($condition = '', $params = []){
        \Yii::$app->router->updateModificationDateSite();
        $iRes = parent::deleteAll($condition, $params);
        section\ParamCache::clear();
        return $iRes;
    }

}
