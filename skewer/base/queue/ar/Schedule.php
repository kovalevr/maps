<?php

namespace skewer\base\queue\ar;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $command
 * @property integer $priority
 * @property integer $resource_use
 * @property integer $target_area
 * @property integer $status
 * @property integer $c_min
 * @property integer $c_hour
 * @property integer $c_day
 * @property integer $c_month
 * @property integer $c_dow
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }


    public function setDefaultValues()
    {
        $this->setAttributes(
            [
                'id'=>0,
                'priority'=>2,
                'resource_use'=>4,
                'target_area'=>1,
                'status'=>1,
            ]
        );
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name', 'title', 'command'], 'string'],
            [['name', 'title', 'command', 'priority', 'resource_use', 'target_area', 'status'], 'required'],
            [['c_min', 'c_hour', 'c_day', 'c_month', 'c_dow', 'priority', 'resource_use', 'target_area', 'status'], 'integer'],
            ['c_min', 'integer', 'min'=>0, 'max'=>59],
            ['c_hour', 'integer', 'min'=>0, 'max'=>23],
            ['c_day', 'integer', 'min'=>1, 'max'=>31],
            ['c_month', 'integer', 'min'=>1, 'max'=>12],
            ['c_dow', 'integer', 'min'=>1, 'max'=>7],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>     \Yii::t('schedule', 'id'),
            'title' =>  \Yii::t('schedule', 'title'),
            'name' =>   \Yii::t('schedule', 'name'),
            'command' => \Yii::t('schedule', 'command'),
            'priority' => \Yii::t('schedule', 'priority'),
            'resource_use' => \Yii::t('schedule', 'resource_use'),
            'target_area' => \Yii::t('schedule', 'target_area'),
            'status' => \Yii::t('schedule', 'status'),
            'c_min' =>  \Yii::t('schedule', 'c_min'),
            'c_hour' => \Yii::t('schedule', 'c_hour'),
            'c_day' =>  \Yii::t('schedule', 'c_day'),
            'c_month' => \Yii::t('schedule', 'c_month'),
            'c_dow' =>  \Yii::t('schedule', 'c_dow'),
        ];
    }

    /**
     * получение ид задания в рассписании по имени
     * @static
     * @param $name
     * @return bool
     */
    public static function getIdByName($name){

        if ($res = self::findOne(['name'=>$name]))
            return $res->id;

        return false;

    }


}
