<?php
namespace skewer\libs\jquery;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends  AssetBundle{
    public $sourcePath = '@skewer/libs/jquery/web/';
    public $css = [
    ];
    public $js = [
        'jquery.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];
}