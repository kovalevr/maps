/**
 * Created by na on 31.10.2016.
 */
CKEDITOR.plugins.setLang( 'popupform', 'ru', {
    text_caption: 'Текст ссылки',
    ajaxform_caption: 'Ajax',
    width_caption: 'Ширина формы',
    section_caption: 'ID раздела с формой',
    title:'Генератор форм',
    error_400:'Код ответа сервера 400',
    not_200_error:'Произошла ошибка.',
    error_section:'Неверный ID раздела',
    error_width:'Ширина должна быть целым числом',
    error_text:'Необходимо заполнить тест ссылки'
} );