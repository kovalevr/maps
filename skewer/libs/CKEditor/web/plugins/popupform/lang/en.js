/**
 * Created by na on 31.10.2016.
 */
CKEDITOR.plugins.setLang( 'popupform', 'en', {
    text_caption: 'Link text',
    ajaxform_caption: 'Ajax',
    width_caption: 'Form width',
    section_caption: 'Form section ID',
    title:'Form generator',
    error_400:'There was an error 400',
    not_200_error:'something else other than 200 was returned',
    error_section:'Incorrect section ID',
    error_width:'Width must be integer',
    error_text:'Incorrect link text'
} );
