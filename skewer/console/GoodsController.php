<?php

namespace app\skewer\console;

use skewer\build\Adm\Order;
use skewer\components\catalog\GoodsRow;
use skewer\models\TreeSection;
use skewer\base\section\Tree;
use skewer\base\section\Visible;
use skewer\base\section\Parameters;

/**
 * Класс для работы с товарами
 */
class GoodsController extends Prototype
{

    /**
     * Добавляет в зеденный раздел заданное количество рандомных товаров
     * @param int $section id разедеда
     * @param int $cnt количество добавляемых товаров
     * @param string $card имя карточки
     */
    public function actionGenerate( $section, $cnt, $card = 'dopolnitelnye_parametry' ){

        echo "\nДобавляем $cnt записей в раздел №$section\n\n";

        $sRand = rand(10000, 99999);

        for ($i=1; $i<=$cnt; $i++) {
            $row = GoodsRow::create( $card );
            $row->setData([
                'title' => sprintf('good %d/%d sect %d', $i, $sRand, $section) ,
                'price' => rand(10000, 99999),
                'article' => sprintf('art%d-%d', $i, $sRand)
            ]);
            $row->save();
            $row->setViewSection([$section]);

            echo '.';

            if ( !($i % 100) )
                echo " № $i\n";

        }

        echo "\n\nАвтоматическое добавление завершено\n";

    }

    /**
     * Добавляет в раздел верхнего меню заданное количество рандомных каталожных разделов
     * @param int $iCount количество добавляемых разделов
     * @param string $sCardName имя карточки
     */
    public static function actionGeneratetree( $iCount, $sCardName = 'dopolnitelnye_parametry' ){

        $iParentSection = \Yii::$app->sections->topMenu();

        echo "\nДобавляем $iCount каталожных разделов в раздел №$iParentSection\n\n";

        for ($i=1; $i<=$iCount; $i++) {

            $oSection = new TreeSection();
            $oSection->setAttributes([
                'parent' => $iParentSection,
                'type'   => Tree::typeSection,
                'title'  => "Test section №$i",
                'visible' => Visible::HIDDEN_FROM_MENU,
            ]);
            $oSection->save(true);
            $oSection->setTemplate(254);

            Parameters::setParams($oSection->id, 'content', 'defCard', $sCardName);

            if ( !($i % 20) )
                echo " № $i\n";
        }

        echo "\n\nАвтоматическое добавление завершено\n";
    }
}
