<?php

namespace skewer\build\Design\Header;

use skewer\build\Cms;
use skewer\components\auth\CurrentAdmin;
use skewer\base\log\Logger;


/**
 * Модуль верхней панели дизайнерского режима
 * Class Module
 * @package skewer\build\Design\Header
 */
class Module extends Cms\Frame\ModulePrototype {

    /**
     * Первичная инициализация
     * @return int
     */
    public function actionInit() {
    }

    /**
     * Выход из системы
     * @return int
     */
    protected function actionLogout(){

        // задать состояние
        $this->setCmd('logout');

        Logger::addNoticeReport("Выход пользователя из системы администрирования", \skewer\base\log\Logger::buildDescription(array('ID пользователя'=>CurrentAdmin::getId(), 'Логин'=>$_SESSION['auth']['admin']['userData']['login'])), Logger::logUsers,$this->getModuleName());

        // попытка авторизации
        $bLogOut = CurrentAdmin::logout();

        // результат авторизации
        $this->setData('success',$bLogOut);

        // отдать результат работы метода
        return psComplete;

    }

    protected function actionDropCacheAndReload() {
        \Yii::$app->rebuildCss();
        $this->fireJSEvent('reload_display_form');
    }

}
