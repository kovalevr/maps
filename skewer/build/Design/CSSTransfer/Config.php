<?php

use skewer\base\site\Layer;

$aConfig['name']     = 'CSSTransfer';
$aConfig['title']    = 'Перенос изменений';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Перенос изменений';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::DESIGN;
$aConfig['languageCategory']     = 'design';

return $aConfig;
