<?php

namespace skewer\build\Design\CSSEditor;

use skewer\base\ui;
use skewer\build\Cms;
use skewer\build\Design\CSSEditor\models\CssFiles;
use skewer\components\design\Design;
use skewer\components\ext;
use yii\base\UserException;

/**
 * Модуль работы с редактором пользовательского css кода
 */
class Module extends Cms\Tabs\ModulePrototype {

    protected $name = 'CSS Редактор'; /** @fixme почему тут русский текст? */

    /** @var string режим отображения (по-умолчанию или pda версия) */
    protected $sViewMode = Design::versionDefault;

    /**
     * Пользовательская функция инициализации модуля
     */
    protected function onCreate() {
        $this->addJSListener( 'urlChange', 'checkVersion' );
    }

    protected function actionCheckVersion() {

        // получить тип отображения
        list( $sUrl ) = $this->get('params');
        if ( !$sUrl ) throw new \Exception('Url не задан');
        $sType = Design::getVersionByUrl( $sUrl );

        // если не совпадает с текущим
        if ( $sType !== $this->sViewMode ) {
            $this->sViewMode = $sType;
            $this->actionInit();
        }

    }

    /**
     * Метод, выполняемый в начале обработки
     */
    protected function preExecute() {

        // тип отображения сайта
        $this->sViewMode = Design::getValidVersion( $this->getStr('type', $this->sViewMode) );

        // протестировать наличие необходимх файлов
        $this->testFileAccess();

    }

    /**
     * Состояние. Выбор корневого набора разделов
     */
    protected function actionInit() {

       $this->actionList();

    }

    /**
     * Список
     * @throws \Exception
     */
    protected function actionList(){

        $aCssFiles = CssFiles::find()
            ->orderBy(['priority'=>SORT_ASC])
            ->asArray()
            ->all();

        $oFormBuilder = new ui\StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder
            ->field('id', 'ID', 'string', ['listColumns' => ['flex' => 1]])
            ->field('name', \Yii::t('design', 'field_name'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('last_upd', \Yii::t('design', 'field_last_upd'), 'string')
            ->field('active', \Yii::t('design', 'field_active'), 'check')

            ->buttonRowUpdate()
            ->buttonRowDelete()
            ->buttonRow('export',\Yii::t('design', 'export_button'),'icon-clone')
        ;

        $oFormBuilder->buttonAddNew('show');

        $oFormBuilder->setValue($aCssFiles);
        $oFormBuilder->setEditableFields(array('active'),'saveFromList');
        $oFormBuilder->enableDragAndDrop( 'sort' );

        $this->setInterface($oFormBuilder->getForm());

    }

    protected function actionExport(){

        $iId = $this->getInDataValInt( 'id' );

        if ($iId){
            $aData = CssFiles::find()
                ->where(['id'=>$iId])
                ->asArray()
                ->one();

            $sWebFilePath = WEBPROTOCOL.WEBROOTPATH.'files/design/out/editor_data.txt';

            $sFileName = WEBPATH.'files/design/out/editor_data.txt';

            if (!file_exists(WEBPATH.'files/design/out/'))
                mkdir(WEBPATH.'files/design/out/',0775);

            if ( !is_writable(WEBPATH.'files/') )
                throw new \Exception("Не могу записать файл [$sFileName]");

            $handle = fopen($sFileName,"a");

            $bRes = file_put_contents(WEBPATH.'files/design/out/editor_data.txt',json_encode($aData));

            fclose($handle);

            if ( !$bRes )
                throw new \Exception("Не удалось записать файл [$sFileName]");

            $sText = "\r\n";
            $sText.= "Создан файл $sWebFilePath";

            $sText.= "\r\n";

            $oForm = new ext\FormView();

            $oForm->setAddText($sText);

            $oForm->addExtButton(ext\docked\Api::create('Назад' )
                ->setAction( 'list' )
                ->setIconCls(ext\docked\Api::iconCancel)
                ->unsetDirtyChecker());

            $this->setInterface($oForm);
        }

    }

    protected function actionSort() {

        $aItemDrop   = $this->get('data');
        $aItemTarget = $this->get('dropData');
        $sOrderType  = $this->get('position');

        if ($aItemDrop and $aItemTarget and $sOrderType)
            CssFiles::sortValues($aItemDrop, $aItemTarget, $sOrderType);

        self::rebuildFile();
// перегрузить фрейм отображения
        $this->fireJSEvent('reload_display_form');
        $this->actionList();
    }


    /**
     * Детальная
     */
    protected function actionShow(){

        $iId = $this->getInDataValInt( 'id' );

        if ($iId)
            $aData = CssFiles::find()
                ->where(['id'=>$iId])
                ->asArray()
                ->one();
        else
            $aData = [];

        $oFormBuilder = new ui\StateBuilder();
        $oFormBuilder = $oFormBuilder::newEdit();
        $oFormBuilder
            ->field('id', 'ID', 'hide', ['listColumns' => ['flex' => 1]])
            ->field('name', \Yii::t('design', 'field_name'), 'string', ['listColumns' => ['flex' => 3]])
            ->field('last_upd', \Yii::t('design', 'field_last_upd'), 'hide')
            ->field('active', \Yii::t('design', 'field_active'), 'check')
            ->field('data',\Yii::t('design', 'field_data'),'text_css',[
                                                                            'hideLabel' => true,
                                                                            'heightInPanel' => true,
                                                                            'height' => '100%',
                                                                            'margin' => '0 1 0 0',
                                                                        ])

            ->buttonSave()
            ->buttonBack();

        $oFormBuilder->setValue($aData);
        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionDelete(){

        $iId = $this->getInDataValInt( 'id' );
        CssFiles::deleteAll(['id'=>$iId]);

        $this->actionList();

    }

    /**
     * Сохранение записи
     * @throws UserException
     */
    protected function actionSave(){

        $aData = $this->get( 'data' );

        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( $iId ) {
            $oCssFile = CssFiles::findOne(['id'=>$iId]);
            if ( !$oCssFile )
                throw new UserException( "Запись [$iId] не найдена" );
        } else {
            $oCssFile = new CssFiles();

        }

        $aData['last_upd'] = date("Y-m-d H:i:s");

        $oCssFile->setAttributes($aData);

        $oCssFile->save(false);

        if (!$iId){
            $oCssFile->setAttribute('priority',$oCssFile->id);
            $oCssFile->save(false);
        }

        self::rebuildFile();
// перегрузить фрейм отображения
        $this->fireJSEvent('reload_display_form');
        $this->actionList();
    }

    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {
        $sTitle = sprintf('%s: %s сайта', $this->name, Design::getVersionTitle( $this->sViewMode ));
        $oIface->setPanelTitle($sTitle);
    }

    /**
     * Собирает новый выходной файл
     */
    public static function rebuildFile(){

        $oFiles = CssFiles::find()
            ->where(['active'=>'1'])
            ->orderBy(['priority'=>SORT_ASC])
            ->asArray()
            ->all();

        $sText = '';

        foreach ($oFiles as $item){
            $sText.=$item['data'];
        }

        $sText = str_replace("\r",' ',$sText);
        $sText = str_replace("\n",' ',$sText);
        $sText = str_replace("\t",' ',$sText);
        $sText = preg_replace('/\s{2,}/',' ',$sText);

        $sFilePath = Design::getAddCssFilePath(Design::versionDefault);
        // создать
        $handle = fopen($sFilePath, 'w+');
        // записать
        fwrite($handle, $sText);
        // сохранить
        fclose($handle);

    }

    /**
     * Проверяет наличие файлов и доступность их на запись
     */
    protected function testFileAccess() {

        try {
            Api::testFileAccess();
        } catch (\Exception $e) {
            // объект для построения списка
            $oForm = new ext\FormView();
            $this->setInterface($oForm);

            throw $e;
        }

    }

    /**
     * Сохраняет записи из спискового интерфейса
     */
    protected function actionSaveFromList() {

        $iId = $this->getInDataValInt( 'id' );

        $sFieldName = $this->get('field_name');

        $oRow = CssFiles::findOne(['id'=>$iId]);
        /** @var CssFiles $oRow */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" );

        $oRow->$sFieldName = $this->getInDataVal( $sFieldName );

        $oRow->last_upd = date("Y-m-d H:i:s");

        $oRow->save(false);

        self::rebuildFile();
        // перегрузить фрейм отображения
        $this->fireJSEvent('reload_display_form');
        $this->actionInit();
    }

}
