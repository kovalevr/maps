<?php

namespace skewer\build\LandingPage\Gallery;


use skewer\base\section\Parameters;
use skewer\components\lp;
use skewer\components\gallery;
use skewer\base\Twig;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Gallery
 */
class Module extends lp\PagePrototype {

    /**
     * Отдает собранный html код для сборки блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = lp\Api::getViewForSection( $this->sectionId() );

        $iGalleryId = Parameters::getValByName( $this->sectionId(), Parameters::object, 'iCurrentAlbumId', false );

        \Yii::$app->router->setLastModifiedDate(gallery\models\Albums::getMaxLastModifyDate());
        
        if (!$iGalleryId) return '';
        
        $aData = gallery\Photo::getFromAlbum(explode(',', $iGalleryId), true);

        // НАЧАЛО: Операция для совместимости с шаблоном в БД. todo переделать шаблон, см \skewer\build\Page\GalleryViewer\templates\slider.twig
        if ($aData){
            $iCount = count($aData);

            for ($i=0; $i<$iCount; $i++) {
                if ( (!isset($aData[$i]['images_data']['preview']['file']) OR empty($aData[$i]['images_data']['preview']['file'])) OR
                    (!isset($aData[$i]['images_data']['med']['file']) OR empty($aData[$i]['images_data']['med']['file'])) ) unset($aData[$i]);
                /** @var \skewer\components\gallery\models\Photos [] $aData */
                $aData[$i] = $aData[$i]->toArray();
                $aData[$i]['min_src'] = $aData[$i]['images_data']['preview']['file'];
                $aData[$i]['med_src'] = $aData[$i]['images_data']['med']['file'];
                $aData[$i]['min_width'] = isset($aData[$i]['images_data']['preview']['width']) ? $aData[$i]['images_data']['preview']['width'] : '';
                $aData[$i]['min_height'] = isset($aData[$i]['images_data']['preview']['height']) ? $aData[$i]['images_data']['preview']['height'] : '';
            }
            // КОНЕЦ: Операция для совместимости с шаблоном в БД.
        }

        $aData = array(
            'list' => $aData,
            'section' => $this->sectionId(),
            'titleOnMain' => ''
        );

        return Twig::renderSource( $sContent, $aData );
    }
}