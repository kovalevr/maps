<?php

namespace skewer\build\LandingPage\Editor;

use skewer\components\lp;
use skewer\base\section\Tree;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends lp\PagePrototype {

    /** @var int id шаблона раздела */
    var $template;

    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    protected function getRenderedText() {

        $sRenderedText = lp\Api::getViewForSection( $this->sectionId() );

        /* #40102 замена метки '[Year]' на текущий год в блоке Подвала */

        $aSections = Tree::getCachedSection();

        // Если шаблон текущего раздела соответтсвует Подвалу, то выполнить замену метки
        if ( ($this->template and isset($aSections[$this->template])) and
             (in_array($aSections[$this->template]['alias'], ['footer-block'])) )
            $sRenderedText = str_replace('[Year]', date('Y'), $sRenderedText);

        return $sRenderedText;
    }
}