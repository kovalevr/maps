<?php

namespace skewer\build\Cms\Log;

use skewer\build\Cms;
use skewer\base\site\Site;

/**
 * Class Module
 * @package skewer\build\Cms\Log
 */
class Module extends Cms\Frame\ModulePrototype {

    public function execute() {

        $sHeader = sprintf(
            "%s (ver %s)",
            \Yii::t('forms', 'logPanelHeader'),
            Site::getCmsVersion()
        );

        $this->setModuleLangValues(
            array(
                'logPanelHeader'=> $sHeader,
                'clear'=>'clear',
                'log'=>'log',
                'err'=>'err'
            ));
        $this->setData('cmd', 'init' );

        return psComplete;

    }// func

}// class
