<?php

namespace skewer\build\LandingAdm\Gallery;

use skewer\base\ui;
use skewer\components\lp;
use skewer\build\Adm;
use skewer\components\gallery;

/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 * @package skewer\build\Adm\LandingPageEditor
 * @deprecated НЕ ИСПОЛЬЗУЕТСЯ ПОКА
 */
class Module extends Adm\Tree\ModulePrototype {


}