<?php

namespace skewer\build\Tool\Payments;
use skewer\base\site\Site;
use skewer\base\site_module\Request;
use skewer\build\Adm\Order\Api as ApiOrder;


/**
 * Class YandexkassaPayment
 * @see https://money.yandex.ru/doc.xml?id=526537
 * @package skewer\build\Tool\Payments
 */
class YandexkassaPayment extends Payment{

    /** Продакшен-шлюз */
    const prodUrl = 'https://money.yandex.ru/eshop.xml';

    /** Тестовый шлюз */
    const devUrl = 'https://demomoney.yandex.ru/eshop.xml';

    /** Рубль */
    const valRub = 643;

    /** Тестовая валюта */
    const valTest = 10643;

    /** @var string  */
    private $url = '';

    /** @var int Идентификатор Контрагента */
    private $shopId = 0;

    /** @var int Номер витрины Контрагента */
    private $scid = 0;

    /** @var int Код валюты */
    private $sCurrencyCode = 0;

    /** @var string Пароль магазина */
    private $shopPassword = '';

    /** @var string Способ оплаты */
    private $paymentType = '';

    /** @var  string Текст ответа яндексу */
    private $xml;

    /**
     * @var [] Список полей для редактирования
     */
    protected static $aFields = [
        ['shopId', 'YandexKassa_shopId_field', 'i', 'str'],
        ['scid', 'YandexKassa_scid_field', 'i', 'str'],
        ['shopPassword', 'YandexKassa_shopPassword_field', 's', 'pass']
    ];


    public function init() {

        if ( defined('YANDEXKASSA_DEBUG') && YANDEXKASSA_DEBUG ){
            $this->url = static::devUrl;
            $this->sCurrencyCode = static::valTest;
        } else {
            $this->url = static::prodUrl;
            $this->sCurrencyCode = static::valRub;
        }

        parent::init();
    }


    /**
     * Тип агрегатора систем оплат
     * @return mixed
     */
    public function getType(){
        return 'yandexkassa';
    }


    /**
     * Сумма заказа
     * @return float|string
     */
    public function getSum(){
        return number_format( $this->sum, 2, '.', '' );
    }


    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    public function getFail() {
        header("Content-type: text/xml; charset=utf-8");
        return $this->xml;
    }

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    public function getSuccess() {
        header("Content-type: text/xml; charset=utf-8");
        return $this->xml;
    }

    /** @inheritdoc */
    public function checkResult(){

        $sAction   = Request::getStr('action');
        $md5       = Request::getStr('md5');
        $shopId    = Request::getStr('shopId');
        $invoiceId = Request::getStr('invoiceId');

        $iOrderId = (int)Request::getStr('orderNumber');
        if (!$iOrderId or (!$iSum = ApiOrder::getOrderSum( $iOrderId )))
            return false;

        $this->setOrderId($iOrderId);
        $this->setSum($iSum);

        $hash = md5(implode(';', [
            $sAction,
            $this->getSum(),
            $this->sCurrencyCode,
            Request::getStr( 'orderSumBankPaycash' ),
            $this->shopId,
            $invoiceId,
            Request::getStr( 'customerNumber' ),
            $this->shopPassword,
        ]));

        $aCallbackParams = [
            'action'    => $sAction,
            'invoiceId' => $invoiceId,
        ];

        $bCheckHash = ( ($shopId == $this->shopId) and (strcasecmp($hash, $md5) == 0) );

        switch ($sAction) {

            // Проверка заказа
            case 'checkOrder':
                if ($bCheckHash) {
                    $this->sendCode( $aCallbackParams, 0 );
                    return null; // Не менять статус заказа
                } else {
                    $this->sendCode( $aCallbackParams, 100 );
                    return false; // Отменить заказ
                }
                break;

            // Оплата заказа
            case 'paymentAviso':
                if ($bCheckHash) {
                    $this->sendCode( $aCallbackParams, 0 );
                    return true; // Установить пометку оплаты
                } else {
                    $this->sendCode( $aCallbackParams, 1 );
                    return false; // Отменить заказ
                }
                break;

            // Отмена заказа
            case 'cancelOrder':
                if ($bCheckHash) {
                    $this->sendCode( $aCallbackParams, 0 );
                    return false; // Отменить заказ
                } else {
                    $this->sendCode( $aCallbackParams, 1 );
                    return null; // Не менять статус заказа
                }
                break;
        }

        $this->sendCode( $aCallbackParams, 1 );
        return null;
    }

    /**
     * Текст для отправки сообщения о принятии платежа
     * @param $callbackParams
     * @param $code
     */
    private function sendCode($callbackParams, $code){

        $this->xml = '<?xml version="1.0" encoding="UTF-8"?>
			<'.$callbackParams['action'].'Response performedDatetime="'.date("c").'" code="'.$code.'" invoiceId="'.$callbackParams['invoiceId'].'" shopId="'.$this->shopId.'"/>';

    }


    /**
     * Вывод формы для оплаты
     * @return string
     */
    public function getForm(){

        if (!$this->active){
            return '';
        }

        $oForm = new Form();
        $oForm->setAction($this->url);
        $oForm->setMethod('POST');

        $aFields = [
            'shopId'         => $this->shopId,
            'scid'           => $this->scid,
            'sum'            => $this->getSum(),
            'customerNumber' => $this->getOrderId(),
            'orderNumber'    => $this->getOrderId(),
            'paymentType'    => $this->paymentType,
            'shopSuccessURL' => $this->getSuccessUrl(),
            'shopFailURL'    => $this->getCancelUrl(),
            'yaKassaPayment'   => '1',
        ];

        $oForm->setFields( $aFields );

        return $this->parseForm( $oForm );
    }


    /**
     * Инициализация параметров
     * @param [] $aParams
     */
    public function initParams($aParams = []) {
        if ($aParams){
            foreach($aParams as $sKey => $aParam){
                switch ( $sKey ){
                    case 'active':
                        $this->active = $aParam;
                        break;
                    case 'shopId':
                        $this->shopId = $aParam;
                        break;
                    case 'scid':
                        $this->scid = $aParam;
                        break;
                    case 'shopPassword':
                        $this->shopPassword = $aParam;
                        break;
                }
            }
            $this->bInitParams = true;
        }
    }

    protected function getSuccessUrl()
    {
        return Site::httpDomain().\Yii::$app->router->rewriteURL('[' . \Yii::$app->sections->getValue('payment_success') . ']') . '?orderNumber=' . $this->orderId;
    }

    protected function getCancelUrl()
    {
        return Site::httpDomain().\Yii::$app->router->rewriteURL('[' . \Yii::$app->sections->getValue('payment_fail') . ']') . '?orderNumber=' . $this->orderId;
    }

}