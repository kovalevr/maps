<?php

$aLanguage = array();

$aLanguage['ru']['FormOrders.Tool.tab_name'] = 'Заказы из форм';
$aLanguage['ru']['add_date'] = 'Дата добавления';
$aLanguage['ru']['section'] = 'Добавлено из раздела';
$aLanguage['ru']['status'] = 'Статус заявки';
$aLanguage['ru']['status_new'] = 'новая';
$aLanguage['ru']['status_ready'] = 'обработано';
$aLanguage['ru']['not_found'] = 'Такой формы не существует';
$aLanguage['ru']['bad_status'] = 'Неправильно настроена форма. Обработчик не сохраняет в базу';

$aLanguage['en']['FormOrders.Tool.tab_name'] = 'Orders from order forms';
$aLanguage['en']['add_date'] = 'Add date';
$aLanguage['en']['section'] = 'Section';
$aLanguage['en']['status'] = 'status';
$aLanguage['en']['status_new'] = 'new';
$aLanguage['en']['status_ready'] = 'ready';
$aLanguage['en']['not_found'] = 'Form not found';
$aLanguage['en']['bad_status'] = 'Bad status';


return $aLanguage;