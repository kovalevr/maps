<?php

namespace skewer\build\Tool\SeoGen;

use skewer\components\import\provider\Xls;

class XlsWriter extends Xls{

    protected $oExcel;

    public $skip_row = 1;

    /**
     * Физическое создание нового файла Excel
     * @param array $aHeaderFields
     */
    public function createXlsFile($aHeaderFields = []){

        $this->oExcel = new \PHPExcel();
        $this->sheet = $this->oExcel->setActiveSheetIndex(0);

        for($iColumnIndex = 0; $iColumnIndex < count($aHeaderFields); $iColumnIndex++){
            $this->sheet->getColumnDimensionByColumn($iColumnIndex)->setWidth($aHeaderFields[$iColumnIndex]['width']);
            $this->sheet->setCellValueByColumnAndRow($iColumnIndex, 1, $aHeaderFields[$iColumnIndex]['title']);
            $this->sheet->getStyleByColumnAndRow($iColumnIndex, 1)->applyFromArray(Styles::$HEADER);
        }

        $this->save();
    }

    public function beforeExecute(){
        $row = $this->getConfigVal('row');
        if ($row <= $this->skip_row){
            $row = $this->skip_row + 1;
            $this->setConfigVal( 'row', $row );
        }
        $this->oExcel = \PHPExcel_IOFactory::load($this->file);
        $this->sheet = $this->oExcel->setActiveSheetIndex(0);

    }


    /**
     * Записывает строку данных в текущий активный лист
     * @param int $iRowIndex - индекс строки
     * @param array $aBuffer массив данных
     */
    public function writeRow($iRowIndex, $aBuffer){

        $iColumnIndex = 0;
        foreach ($aBuffer as $value) {
            if (is_array($value)){
                $this->sheet->setCellValueByColumnAndRow($iColumnIndex, $iRowIndex, $value['value']);

                if (!empty($value['style']))
                    $this->sheet
                        ->getStyleByColumnAndRow($iColumnIndex, $iRowIndex)
                        ->applyFromArray($value['style']);

            }else{
                $this->sheet->setCellValueByColumnAndRow($iColumnIndex, $iRowIndex, $value);
            }

            $iColumnIndex++;
        }
    }


    /**
     * Записывает объект excel в файл
     * @throws \PHPExcel_Writer_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    protected function save(){
        $objWriter = \PHPExcel_IOFactory::createWriter($this->oExcel, 'Excel2007');
        $objWriter->save($this->file);
    }


    public function init(){
        $row = $this->getConfigVal('row');
        if (!$row){
            $this->setConfigVal( 'row', $this->skip_row + 1 );
        }
    }


    /** Метод вызываемый после очередной итерации на запись */
    public function afterExecute(){
        $this->save();
    }


}