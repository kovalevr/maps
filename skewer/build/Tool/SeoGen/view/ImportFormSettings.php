<?php

namespace skewer\build\Tool\SeoGen\view;

use skewer\base\ft\Editor;
use skewer\build\Tool\LeftList\ModulePrototype;
use skewer\build\Tool\SeoGen\Api;
use skewer\build\Tool\SeoGen\ImportTask;
use skewer\components\catalog\Section;
use skewer\components\ext\view\FormView;

class ImportFormSettings extends FormView{

    /**
     * @var string Тип данных
     */
    public $sTypeData = '';

    /** @var string Режим импорта */
    public $sMode = '';

    /**
     * @var array Значения полей формы
     */
    public $aValues = [];

    /**
     * @var ModulePrototype Ссылка на модуль
     */
    protected $oModule;


    public function __construct($oModule, array $config = []){
        $this->oModule = $oModule;
        parent::__construct($config);
    }

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    public function build(){

        $this->_form
            ->headText(\Yii::$app->getView()->renderPhpFile(
                $this->oModule->getModuleDir() . DIRECTORY_SEPARATOR . $this->oModule->getTplDirectory() . DIRECTORY_SEPARATOR . 'head.php',
                ['headTitle' => \Yii::t('SeoGen', 'import_headTitle'), 'linkFile' => Api::getWebPathImportBlankFile(), 'titleLink' => \Yii::t('SeoGen', 'import_filename')]
            ))
            ->field('file', 'Файл', 'file')
            ->fieldSelect('data_type', 'Тип данных', Api::getDataTypes(), ['onUpdateAction' => 'updateImportState'])
        ;

        switch ($this->sTypeData){

            case Api::DATATYPE_SECTIONS:

                $this->_form->fieldSelect('mode', 'Действие',ImportTask::getModesExecute(), ['onUpdateAction' => 'updateImportState']);

                if ($this->sMode == ImportTask::$sCreateMode)
                    $this->_form->fieldString('sectionId', 'Введите Id раздела');

                $this->_form->fieldCheck('enable_staticContents', 'Обновить данные из полей "текст раздела" и "текст раздела 2"');

                break;

            case Api::DATATYPE_GOODS:

                $this->_form
                    ->fieldMultiSelect('catalog_sections', 'Раздел каталога', ['all' => 'Все'] + Section::getList())
                    ->fieldWithValue('mode', 'Действие', Editor::SELECT, ImportTask::$sUpdateMode,
                        [
                            'show_val' => [ ImportTask::$sUpdateMode => ImportTask::getModesExecute()[ImportTask::$sUpdateMode]],
                            'emptyStr' => false,
                        ])
                ;

                break;

        }

        $this->_form
            ->button('importRun','Импорт', 'icon-reload','save',['unsetFormDirtyBlocker' => true])
            ->button('showLastLog','Лог последнего запуска', 'icon-list')
            ->buttonBack()

            ->setValue($this->aValues)
        ;

    }

    public function beforeBuild(){

        // Убираем данные, которые не должны протягиваться между состояниями
        foreach ($this->aValues as $key => &$item) {
            if ( !in_array($key, ['file', 'data_type', 'mode', 'enable_staticContents']) )
                unset($this->aValues[$key]);
        }

    }


}