<?php

namespace skewer\build\Tool\SeoGen;

class Styles{


    /**
     * Заголовок
     * @var array
     */
    public static $HEADER = array(
        'fill' => array(
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'color'=> array(
                'rgb' => 'CFCFCF'
            )
        ),
        'alignment' => array(
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
        ),
        'font' => array(
            'bold' => true
        )
    );

    /**
     * Зелёная заливка
     * @var array
     */
    public static $GREEN = array(
        'fill' => array(
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array(
                'rgb' => '98FB98'
            )
        )
    );

    /**
     * Красная заливка
     * @var array
     */
    public static $RED = array(
        'fill' => array(
            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array(
                'rgb' => 'FF7373'
            )
        )
    );

}