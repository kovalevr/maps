<?php

/**
 *  @var array aLogParams
 */

?>
<div style="margin-left: 30px;">
    <? if (isset($aLogParams['status'])): ?>
        <p><?=Yii::t('seoGen', 'status')?>: <?=$aLogParams['status']?></p>
    <? endif; ?>

    <? if (isset($aLogParams['start'])): ?>
        <p><?=Yii::t('seoGen', 'start')?>: <?=$aLogParams['start']?></p>
    <? endif; ?>

    <? if (isset($aLogParams['finish'])):?>
        <p><?=Yii::t('seoGen', 'finish')?>: <?=$aLogParams['finish']?></p>
    <?endif;?>

    <? if (isset($aLogParams['newRecords'])):?>
        <p>Добавлено: <?=$aLogParams['newRecords']?> записей</p>
    <?endif;?>

    <? if (isset($aLogParams['updateRecords'])):?>
        <p>Обновлено: <?=$aLogParams['updateRecords']?> записей</p>
    <?endif;?>

    <? if (isset($aLogParams['linkFile'])):?>
        <p><a href="<?=$aLogParams['linkFile']?>">Ссылка на файл</a></p>
    <? endif;?>

    <? if (!empty($aLogParams['create_section'])): ?>
        <hr>
        <div>
            <p><b><?=Yii::t('seoGen','create_section')?>:</b></p>
            <table>
                <? foreach ($aLogParams['create_section'] as $item):?>
                    <tr>
                        <td><?=$item?></td>
                    </tr>
                <? endforeach;?>

            </table>
        </div>
    <?endif;?>

    <? if (!empty($aLogParams['error_list'])):?>
    <div style="margin-top: 15px;">
        <p><b>Ошибки:</b></p>
        <table>
            <? foreach ($aLogParams['error_list'] as $error):?>
            <tr>
                <td><?=$error?></td>
            </tr>
            <?endforeach;?>
        </table>
    </div>
    <?endif?>


    <? if (!empty($aLogParams['notAdded'])):?>
        <div style="margin-top: 15px;">
            <p><b>Не добавленные разделы:</b></p>
            <table>
                <? foreach ($aLogParams['notAdded'] as $section):?>
                    <tr>
                        <td><?=$section?></td>
                    </tr>
                <?endforeach;?>
            </table>
        </div>
    <? endif;?>



    <? if (!empty($aLogParams['notUpdated'])):?>
        <div style="margin-top: 15px;">
            <p><b>Не обновленные разделы:</b></p>
            <table>
                <? foreach ($aLogParams['notUpdated'] as $section):?>
                    <tr>
                        <td><?=$section?></td>
                    </tr>
                <?endforeach;?>
            </table>
        </div>
    <? endif;?>




</div>