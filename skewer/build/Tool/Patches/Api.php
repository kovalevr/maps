<?php
namespace skewer\build\Tool\Patches;

use skewer\components\config\ConfigUpdater;
use skewer\components\config\PatchInstaller;
use skewer\components\config\UpdateException;
use skewer\components\design\Design;
use skewer\build\Tool\Patches\models\Patch;
use yii\data\ActiveDataProvider;
use yii\web\ServerErrorHttpException;

class Api {

    /**
     * Возвращает список доступных к установке патчей в директории сайта либо false в случае ошибки или отсутствия патчей
     * @static
     * @param string $sRootPatchesDir Путь к корневой директории с патчами
     * @return array|bool
     */
    public static function getAvailablePatches($sRootPatchesDir) {
        if(!is_dir($sRootPatchesDir)) return false;

        $aOut = false;

        /** @var \Directory $oDir */
        $oDir = dir($sRootPatchesDir);

        /** @noinspection PhpUndefinedFieldInspection */
        if($oDir->handle)
            while(false !== ($sFile = $oDir->read())){

                if($sFile == '.' and $sFile == '..') continue;
                if(!is_dir($sRootPatchesDir.$sFile)) continue;

                $sPatchFile = $sFile.DIRECTORY_SEPARATOR.$sFile.'.php';

                if(!file_exists($sRootPatchesDir.$sPatchFile)) continue;

                $aOut[] = $sPatchFile;
            }// h

        $oDir->close();

        return $aOut;
    }// func

    /**
     * Возвращает список примененных патчей на текущей площадке
     * @static
     * @param int $iPage страница постраничного
     * @param int $iOnPage Количество элементов на страницу
     * @return bool|array
     */
    public static function getAppliedPatches($iPage = 0, $iOnPage = 0) {

        $iPage = ($iPage)? --$iPage: $iPage;

        $provider = new ActiveDataProvider([
            'query'      => Patch::find()
                ->orderBy(['install_date'=>SORT_DESC])
                ->asArray(),
            'pagination' => [
                'page'      => $iPage,
                'pageSize'  => $iOnPage,
            ],
        ]);

        return  $provider->getTotalCount() ? $provider->getModels() : false;

    }// func

    /**
     * Возвращает список доступных и установленных патчей
     * @static
     * @throws UpdateException
     * @return array
     */
    public static function getLocalList() {

        /* Запросили список доступных к установке патчей */
        $aAvailable = static::getAvailablePatches(PATCHPATH);
        /* Запросили список установленных патчей */
        $aApplied   = static::getAppliedPatches();


        if(!$aAvailable AND !$aApplied) return array();

        $aAppliedUIDs = array();
        foreach($aApplied as $iKey=>$aPatch) {

            $aApplied[$iKey]['is_install'] = true;
            $aAppliedUIDs[] = $aPatch['patch_uid'];
        }// each installed patch

		if(is_array($aAvailable) AND count($aAvailable))
	        foreach($aAvailable as $sPatch) {

                $sPatchUID = basename($sPatch);

                if(empty($sPatchUID)) continue;
                if(in_array($sPatchUID, $aAppliedUIDs)) continue;

                $aNewPatch['patch_uid']     = $sPatchUID;
                $aNewPatch['install_date']  = 'Не установлен';
                $aNewPatch['description']   = static::getDescFormFile( $sPatch );
                $aNewPatch['is_install']    = false;
                $aNewPatch['file']          = $sPatch;

                array_unshift($aApplied, $aNewPatch);
	
	        }// each available patch

        return $aApplied;
    }// func

    /**
     * Возвращает true, если патч с UID $sPatchUID ранее устанавливался на данной площадке
     * @param string $sPatchUID
     * @return bool
     */
    public static function alreadyInstalled($sPatchUID) {

        return (bool)Patch::findOne(['patch_uid'=>$sPatchUID]);

    }

    /**
     * Отдать описание из файла
     * @static
     * @param $sPatch
     * @return string
     */
    public static function getDescFormFile( $sPatch ) {

        // попробовать открыть и прочитать
        $sCont = file_get_contents( PATCHPATH.$sPatch );
        if ( !$sCont ) return '';

        // попробовать достать описание
        if ( preg_match( '/\$sDescription\s*=\s*[\'"]{1}(?<desc>.*)[\'"]{1};/i', $sCont, $aMatch ) ) {
            return $aMatch['desc'];
        }

        return $sCont;

    }


    /**
     * Установка патча.
     * @param $patch_file
     * @throws ServerErrorHttpException
     * @return array|bool
     */
    public static function installPatch( $patch_file ){

        $oInstaller = new PatchInstaller(PATCHPATH . $patch_file);

        ConfigUpdater::init();

        $mResult = $oInstaller->install();

        //Установка времени последнего обновления
        Design::setLastUpdatedTime();

        ConfigUpdater::commit();

        /* Все прошло нормально - пишем о том, что патч поставили */
        $p = new Patch();

        $p->patch_uid   = basename($patch_file);
        $p->file        = $patch_file;
        $p->install_date = date('Y-m-d h:i:s');
        $p->description = $oInstaller->getDescription();

        $p->save();

        return $mResult;

    }

}
