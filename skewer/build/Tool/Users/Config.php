<?php

use skewer\build\Tool\LeftList;
use skewer\base\site\Layer;

$aConfig['name']     = 'Users';
$aConfig['title']    = 'Пользователи';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Управление пользовательскими аккаунтами';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'auth';

return $aConfig;
