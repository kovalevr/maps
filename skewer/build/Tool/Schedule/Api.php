<?php

namespace skewer\build\Tool\Schedule;

use yii\base\Exception;

class Api {

    /*Статусы задач в планировщике*/
    const iStatusActive = 1;
    const iStatusStopped = 2;
    const iStatusPaused = 3;

    public static function getPriorityArray(){

        return [
            1 => \Yii::t('schedule', 'priority_low'),
            2 => \Yii::t('schedule', 'priority_normal'),
            3 => \Yii::t('schedule', 'priority_high'),
            4 => \Yii::t('schedule', 'priority_critical'),
        ];

    }

    public static function getResourceArray(){

        return [
            3 => \Yii::t('schedule', 'resource_background'),
            4 => \Yii::t('schedule', 'resource_normal'),
            7 => \Yii::t('schedule', 'resource_intensive'),
            9 => \Yii::t('schedule', 'resource_critical'),
        ];

    }


    public static function getTargetArray(){

        return [
            1 => \Yii::t('schedule', 'target_site'),
            2 => \Yii::t('schedule', 'target_server'),
            3 => \Yii::t('schedule', 'target_system'),

        ];

    }

    public static function getStatusArray(){

        return [
            self::iStatusActive => \Yii::t('schedule', 'status_active'),
            self::iStatusStopped => \Yii::t('schedule', 'status_stopped'),
            self::iStatusPaused => \Yii::t('schedule', 'status_paused'),
        ];

    }

    /**
     * Проверяет значение можно ли его использовать для крона
     * @param $sVal
     * @param $sName
     * @param $sMax
     * @return string
     * @throws Exception
     */
    public static function validateVal($sVal,$sName,$sMax,$lockEvery = false){

        /*Попытаемся разобрать как "интервал запука"*/
        $matches = array();
        preg_match("/[0-9]{1,2}-[0-9]{1,2}/",$sVal, $matches, PREG_OFFSET_CAPTURE);

        if (!empty($matches)){
            return $matches[0][0];
        }

        if (!$lockEvery){
            /*Попытамся разобрать как "запуск каждые"*/
            $matches = array();
            preg_match("/\*\/[0-9]{1,5}/",$sVal, $matches, PREG_OFFSET_CAPTURE);

            if (!empty($matches)){
                return $matches[0][0];
            }
        }

        if ((is_numeric($sVal)) and ($sVal<=$sMax))
            return $sVal;

        if (($sVal=='') or ($sVal=='*'))
            return '*';

        throw new Exception(\Yii::t('schedule', 'invalid_value',['name'=>$sName]));

    }

}
