<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Редиректы 301';

$aLanguage['ru']['id'] = 'ID правила';
$aLanguage['ru']['old_url'] = 'Старый адрес';
$aLanguage['ru']['new_url'] = 'Новый адрес';
$aLanguage['ru']['input_url'] = 'Адреса для теста (Указывать с новой строки)';
$aLanguage['ru']['test'] = 'Тестировать';
$aLanguage['ru']['test_results'] = 'Результат тестирования';
$aLanguage['ru']['no_data_for_test'] = 'Нет данных для тестирования';
$aLanguage['ru']['invalid_redirect'] = 'Редирект не сработал';
$aLanguage['ru']['error_rule'] = 'Правило уже существует';
$aLanguage['ru']['urlList'] = 'Список адресов';
$aLanguage['ru']['add'] = 'Добавить';
$aLanguage['ru']['newRedirect'] = 'Добавление нового редиректа';
$aLanguage['ru']['save'] = 'Сохранить';
$aLanguage['ru']['saveText'] = 'Добавить новое правило?';
$aLanguage['ru']['editText'] = 'Сохранить изменения в правиле?';
$aLanguage['ru']['editUrl'] = 'Редактирование правила';
$aLanguage['ru']['validationError'] = 'Ошибка "{message}"';
$aLanguage['ru']['no_redirect'] = 'Редирект не сработал';
$aLanguage['ru']['source_url'] = 'Исходный';
$aLanguage['ru']['url_target'] = 'Конечный';
// ********************************************************************************************************************
// ***************************************************** ENGLISH ******************************************************
// ********************************************************************************************************************

$aLanguage['en']['tab_name'] = 'Redirects 301';

$aLanguage['en']['id'] = 'ID';
$aLanguage['en']['old_url'] = 'Old URL';
$aLanguage['en']['new_url'] = 'New URL';
$aLanguage['en']['input_url'] = 'URLs for test';
$aLanguage['en']['test'] = 'Test';
$aLanguage['en']['test_results'] = 'Test results';
$aLanguage['en']['no_data_for_test'] = 'No data for test';
$aLanguage['en']['invalid_redirect'] = 'Invalid redirect';
$aLanguage['en']['error_rule'] = 'Rule already exists';
$aLanguage['en']['urlList'] = 'URL list';
$aLanguage['en']['add'] = 'Add';
$aLanguage['en']['newRedirect'] = 'Add new redirect';
$aLanguage['en']['save'] = 'Save';
$aLanguage['en']['saveText'] = 'Add a new rule?';
$aLanguage['en']['editText'] = 'Save the changes to the rule?';
$aLanguage['en']['editUrl'] = 'Edit rules';
$aLanguage['en']['validationError'] = 'Error "{message}"';
$aLanguage['en']['no_redirect'] = 'No redirect';
$aLanguage['en']['source_url'] = 'Source url';
$aLanguage['en']['url_target'] = 'Target url';

return $aLanguage;