<?php

namespace skewer\build\Tool\Forms;

use skewer\base\site\Type;
use skewer\components\catalog;
use skewer\base\orm\Query;
use skewer\base\ui;
use skewer\components\forms;
use skewer\build\Tool;
use skewer\build\Page;
use skewer\components\auth\CurrentAdmin;
use skewer\base\site\Site;
use skewer\components\targets\models\Targets;

/**
 * Class Module
 * @package skewer\build\Tool\Forms
 */
class Module extends Tool\LeftList\ModulePrototype {

    public $iCurrentForm = 0;
    public $enableSettings = 0;

    protected function preExecute() {

        // id текущего раздела
        $this->iCurrentForm = $this->getInt('form_id');

        if ( !$this->iCurrentForm )
            $this->iCurrentForm = $this->getEnvParam('form_id');

    }


    protected function actionInit() {
        $this->actionList();
    }


    /**
     * Список форм
     */
    protected function actionList() {

        // -- обработка данных
        $this->iCurrentForm = 0;

        $oQuery = forms\Table::find()->order('form_sys');
        if ( !CurrentAdmin::isSystemMode() ) $oQuery
            ->where( 'form_handler_type<>?', 'toMethod' )
            ->andWhere( 'form_sys', 0 )
        ;
        $aItems = $oQuery->getAll();

        // добавляем отдельное поле для отображения надписи,
        // чтобы не перекрывалось при сохранении
        foreach ($aItems as $oItem)
            $oItem->form_handler_type_show = $oItem->form_handler_type;

        $sHandlerLabel = CurrentAdmin::isSystemMode() ? \Yii::t('forms', 'form_handler_value') :
            \Yii::t('forms', 'formHandlerEmailTitle') ;


        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newList();

        $this->setPanelName( \Yii::t('forms', 'form_list') );

        // добавляем поля
        $oFormBuilder
            ->field('form_title', \Yii::t('forms', 'form_title'), 'string', ['listColumns' => ['flex' => 1]])
            ->fieldIf(
                CurrentAdmin::isSystemMode(), 'form_handler_type_show', \Yii::t('forms', 'form_handler_type'), 'string', ['listColumns' => ['width' => 140]]
            )
            ->field('form_handler_value', $sHandlerLabel, 'string', ['listColumns' => ['width' => 240]])
            ->fieldIf(CurrentAdmin::isSystemMode(), 'form_sys', \Yii::t('forms', 'form_sys'), 'check', ['listColumns' => ['width' => 70]])

            ->widget( 'form_handler_type_show', 'skewer\components\forms\Table', 'getTypeTitle' )

            ->setHighlighting('form_sys', \Yii::t('forms', 'form_sys'), '1', 'color: #999999')

            ->setEditableFields(['form_sys'], 'SaveFromList')

            // добавляем данные
            ->setValue( $aItems )

            // элементы управления
            ->buttonAddNew('EditForm', \Yii::t('forms', 'add_new_form'))
            ->buttonRowUpdate( 'FieldList' )
            ->buttonRow('Clone', \Yii::t('adm', 'clone'), 'icon-clone', '')
            ->buttonRowDelete( 'delete' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Редактирование параметров формы
     * @param array $aSubData
     * @return int
     */
    protected function actionEditForm( $aSubData = array() ) {

        // -- обработка данных
        $aData = $this->getInData();

        if ( is_array($aSubData) && !empty($aSubData) )
            $aData = array_merge($aData, $aSubData);

        $iFormId =  isset( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;
        $this->iCurrentForm = $iFormId ? $iFormId : $this->iCurrentForm;

        $oFormRow = $this->iCurrentForm ? forms\Table::find( $this->iCurrentForm ) : forms\Table::getNewRow();

        $sHandlerLabel = CurrentAdmin::isSystemMode() ? \Yii::t('forms', 'form_handler_value') :
            \Yii::t('forms', 'formHandlerEmailTitle') ;

        $aFormTypeList = forms\Table::getTypeList();
        if ( ! CurrentAdmin::isSystemMode() && isSet($aFormTypeList['toMethod']) )
            unSet( $aFormTypeList['toMethod'] );

        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->field('form_id', 'id', 'hide')
            ->field('form_title', \Yii::t('forms', 'form_title'), 'string')
            ->fieldIf(CurrentAdmin::isSystemMode(), 'form_name', \Yii::t('forms', 'form_name'), 'string', [])
            ->fieldSelect( 'form_handler_type', \Yii::t('forms', 'form_handler_type'), $aFormTypeList, [], false )
            ->field(
                'form_handler_value', $sHandlerLabel, 'string', [
                    'subtext' => sprintf(
                        '%s <b>%s</b>',
                        \Yii::t('forms', 'form_handler_value_default'),
                        Site::getAdminEmail()
                    )
                ]
            )
            ->field('form_captcha', \Yii::t('forms', 'form_captcha'), 'check', array('margin' => '15 5 0 0'));

        if ( CurrentAdmin::isSystemMode() or CurrentAdmin::canDo(\skewer\build\Tool\Policy\Module::className(),'useFormsReachGoals') ) {
            $oFormBuilder->fieldSelect( 'form_target', \Yii::t('forms', 'form_target'), Targets::getByTypeArray('yandex') );
            $oFormBuilder->fieldSelect( 'form_target_google', \Yii::t('forms', 'form_target_google'), Targets::getByTypeArray('google') );
        }

        $oFormBuilder
            ->field('form_redirect', \Yii::t('forms', 'form_redirect'), 'string', array(
                'subtext' => \Yii::t('forms', 'form_redirect_default')))
            ->field('form_send_crm', \Yii::t('forms', 'form_send_crm'), 'check');

        if (CurrentAdmin::isSystemMode())
            $oFormBuilder->fieldString('form_template', \Yii::t('editor', 'template'));

        $oFormBuilder->fieldCheck('form_show_required_fields', \Yii::t('forms', 'show_phrase_required_fields'));
        $oFormBuilder->fieldCheck('form_show_header', \Yii::t('forms', 'form_show_header'));

        $oFormBuilder
            ->fieldWysiwyg('form_succ_answer', \Yii::t('forms', 'form_succ_answer'), 300, '', ['margin' => '40 0 0 0'])

            // устанавливаем значения
            ->setValue($oFormRow)

            // добавляем элементы управления
            ->buttonSave()
            ->buttonBack($this->iCurrentForm ? 'FieldList' : 'Init')
        ;

        if ( $this->iCurrentForm ) {
            if( CurrentAdmin::isSystemMode() )
                $oFormBuilder
                    ->buttonSeparator( '->' )
                    ->buttonDelete();
        }

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }

    /** Действие: сохранение параметров формы из списка и возврат в состояние списка форм */
    protected function actionSaveFromList() {

        $this->actionSave();
        $this->actionList();
    }


    /**
     * Сохранение параметров формы
     */
    protected function actionSave() {

        try {

            $aData = $this->getInData();

            Api::validateHandler($aData['form_handler_type'],$aData['form_handler_value']);

            $oFormRow = forms\Table::getById($this->iCurrentForm);
            if ( !$oFormRow )
                $oFormRow = forms\Table::getNewRow();
            $oFormRow->setData( $aData );

            Api::patchSections($aData['form_id'],$aData['form_handler_type']);

            $oFormRow->save();

            if ( $oFormRow->getError() )
                throw new \Exception( $oFormRow->getError() );

            $this->iCurrentForm = $oFormRow->getId();

            $this->actionFieldList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаление формы
     */
    protected function actionDelete() {

        try {

            $aData = $this->getInData();

            $iFormId = isSet( $aData['form_id'] ) ? $aData['form_id'] : false;

            if ( !$iFormId )
                throw new \Exception("not found form id=$iFormId");

            $oFormRow = forms\Table::find( $iFormId );

            if ( !$oFormRow )
                throw new \Exception("not found form id=$iFormId");

            $oFormRow->delete();

            $this->actionList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }
    }


    /**
     * Клонирование формы
     */
    protected function actionClone() {

        $aData = $this->getInData();

        $iFormId = isSet( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;

        if ( $iFormId ) {

            $oForm = forms\Table::getById( $iFormId );

            $oForm->form_id = 0;
            $oForm->form_title .= ' (clone)';

            $oForm->setUniqName();

            $iNewForm = $oForm->save();

            $aFields = forms\FieldTable::find()
                ->where( 'form_id', $iFormId )
                ->getAll();

            /** @var forms\FieldRow $oFieldRow */
            foreach ( $aFields as $oFieldRow ) {

                $oFieldRow->param_id = 0;
                $oFieldRow->form_id = $iNewForm;

                $oFieldRow->save();
            }

            $oForm->save();

            $this->addMessage( \Yii::t('forms', 'from_cloned' ) );

        } else {

            $this->addError( 'From not found.' );
        }

        $this->actionList();
    }


    protected function actionFieldList() {

        // -- обработка данных
        $aData = $this->getInData();

        $iFormId = isset( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;
        $this->iCurrentForm = $iFormId ? $iFormId : $this->iCurrentForm;

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aItems = forms\FieldTable::find()
            ->where( 'form_id', $this->iCurrentForm )
            ->order( 'param_priority' )
            ->getAll();

        // Преобразовать языковые метки
        if ($aItems) {
            /** @var forms\FieldRow $oItem */
            foreach ($aItems as $oItem)
                $oItem->param_title = \Yii::tSingleString($oItem->param_title);
        }

                $sHeadText = sprintf(
            '%s "%s"',
            \Yii::t('forms', 'field_list'),
            $this->iCurrentForm ? $oFormRow->form_title : \Yii::t('forms', 'new_form')
        );

        $this->setPanelName( $sHeadText );


        // -- построение интерфейсов
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->field('param_title', \Yii::t('forms', 'param_title'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('param_name', \Yii::t('forms', 'param_name'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('param_type', \Yii::t('forms', 'param_type'), 'string', ['listColumns' => ['flex' => 1]])
            ->field('param_required', \Yii::t('forms', 'param_required'), 'check', ['listColumns' => ['width' => 140]])
            ->widget( 'param_type', 'skewer\\components\\forms\\FieldTable', 'getTypeTitle' )
            //->setEditableFields( array( 'form_active' ), 'save' )
            ->enableDragAndDrop( 'sortFieldList' )

            // добавляем данные
            ->setValue( $aItems )

            // элементы управления
            ->buttonRowUpdate( 'editField' )
            ->buttonRowDelete( 'deleteField' )
            ->buttonAddNew('EditField')
            ->buttonBack('list')

            ->buttonSeparator()
            ->buttonEdit('EditForm', \Yii::t('forms', 'from_settings'))
            ->buttonIf( Type::hasCatalogModule(), \Yii::t('forms', 'elConnectText'), 'linkList', 'icon-link' )

            ->buttonSeparator()
            ->buttonEdit('answer', \Yii::t('forms', 'answerDetailText'))
            ->buttonEdit('agreed', \Yii::t('forms', 'license'))
        ;
        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    protected function actionSortFieldList() {

        $aData = $this->getInData();
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['param_id']) || !$aData['param_id'] ||
            !isSet($aDropData['param_id']) || !$aDropData['param_id'] || !$sPosition )
            $this->addError( \Yii::t('forms', 'sortParamError') );

        if( ! forms\FieldTable::sort( $aData['param_id'], $aDropData['param_id'], $sPosition ) )
            $this->addError( \Yii::t('forms', 'sortParamError') );

    }


    /**
     * Отображение формы
     */
    protected function actionEditField() {

        // -- обработка данных
        $aData = $this->getInData();
        $iFieldId = isset( $aData['param_id'] ) ? (int)$aData['param_id'] : 0;

        $oFieldRow = $iFieldId ? forms\FieldTable::find( $iFieldId ) : forms\FieldTable::getNewRow();

        if ( !$oFieldRow )
            throw new \Exception( \Yii::t('forms', 'field_not_found') );


        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newEdit();

        $this->setPanelName( \Yii::t('forms', 'edit_field') );

        // добавляем поля
        $oFormBuilder
            ->field('param_id', 'id', 'hide')
            ->field('param_title', \Yii::t('forms', 'param_title'), 'string')
            ->field('param_name', \Yii::t('forms', 'param_name'), 'string')
            ->field('param_description', \Yii::t('forms', 'param_description'), 'text', ['labelAlign' => 'left'])
            ->fieldSelect( 'param_type', \Yii::t('forms', 'param_type'), forms\FieldTable::getTypeList(), [], false )
            ->field('param_required', \Yii::t('forms', 'param_required'), 'check')
            ->field('param_default', \Yii::t('forms', 'param_default'), 'text', array(
                'labelAlign' => 'left',
                'subtext' => \Yii::t('forms', 'defaultDesc')))
            ->field('param_maxlength', \Yii::t('forms', 'param_maxlength'), 'string', [
                'labelAlign' => 'left',
                'subtext' => sprintf(\Yii::t('forms', 'maxlength_desc', [forms\FieldTable::getUploadMaxSize()]))
            ])
            ->fieldSelect( 'param_validation_type', \Yii::t('forms', 'param_validation_type'), forms\FieldTable::getValidatorList(), [], false )
        ;

        if ( CurrentAdmin::isSystemMode() )
        $oFormBuilder
            ->fieldSelect( 'label_position', \Yii::t('forms', 'label_position'),
                forms\FieldTable::getLabelPositionList(), ['margin'=>'15 5 0 0'], false )
            ->field('new_line', \Yii::t('forms', 'new_line'), 'check')
            ->fieldSelect( 'width_factor', \Yii::t('forms', 'width_factor'), forms\FieldTable::getWidthFactorList(), [], false )
            ->field('param_man_params', \Yii::t('forms', 'param_man_params'), 'string', array(
                'subtext' => \Yii::t('forms', 'manParamsDesc')
            ))
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oFieldRow );

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('SaveField')
            ->buttonBack('FieldList');

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Сохранение
     */
    protected function actionSaveField() {

        try {

            $aData = $this->getInData();

            $iFieldId = isset( $aData['param_id'] ) ? (int)$aData['param_id'] : 0;

            $oFieldRow = $iFieldId ? forms\FieldTable::find( $iFieldId ) : forms\FieldTable::getNewRow();
            $oFieldRow->setData( $aData );
            $oFieldRow->form_id = $this->iCurrentForm;

            $oFieldRow->save();

            $oForm = forms\Table::getById($oFieldRow->form_id);
            $oForm->last_modified_date = date( "Y-m-d H:i:s", time() );
            $oForm->save();

            if ( $oFieldRow->getError() )
                throw new \Exception( $oFieldRow->getError() );

            /** @var forms\Row $oFormRow */
            $oFormRow =  forms\Table::find( $this->iCurrentForm );
            if ( $oFormRow->form_handler_type == 'toBase' )
                $oFormRow->preSave();

            $this->actionFieldList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаляет запись
     */
    protected function actionDeleteField() {

        try {

            $aData = $this->getInData();

            $aData['form_id'] = $this->iCurrentForm;

            $iFieldId = iSset( $aData['param_id'] ) ? $aData['param_id'] : false;

            if ( !$iFieldId )
                throw new \Exception( "not found form field id=$iFieldId" );

            /** @var forms\FieldRow $oFieldRow */
            $oFieldRow = forms\FieldTable::find( $iFieldId );

            if ( !$oFieldRow )
                throw new \Exception( "not found form field id=$iFieldId" );

            $oFieldRow->delete();

            $oForm = forms\Table::getById($oFieldRow->form_id);
            $oForm->last_modified_date = date( "Y-m-d H:i:s", time() );
            $oForm->save();

            $this->actionFieldList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

    }


    protected function actionAnswer() {

        $sInfoText = \Yii::t('forms', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $this->iCurrentForm )->asArray()->getOne();

        $aValues = array(
            'form_answer' => $oFormRow->form_answer,
            'answer_title' => $aAddParams['answer_title'],
            'answer_body' => $aAddParams['answer_body']
        );

        // --
        $oFormBuilder = ui\StateBuilder::newEdit();

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'answer_head'),
            $oFormRow->form_title
        ));

        // добавляем поля
        $oFormBuilder
            ->field('form_answer', \Yii::t('forms', 'form_answer'), 'check')
            ->field('answer_title', \Yii::t('forms', 'answer_title'), 'str')
            ->field('answer_body', \Yii::t('forms', 'answer_body'), 'wyswyg')
            ->fieldWithValue( 'info', \Yii::t('forms', 'replace_label'), 'show', $sInfoText )
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $aValues );

        // добавляем элементы управления
        $oFormBuilder
            ->buttonSave('answerSave')
            ->buttonBack('FieldList');

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;

    }


    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception('не выбрана форма');

        if ( !isSet($aData['form_answer']) OR !isSet($aData['answer_title']) OR !isSet($aData['answer_body']) )
            throw new \Exception('не пришли данные');

        Query::InsertInto( 'forms_add_data' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'answer_title', $aData['answer_title'] )
            ->set( 'answer_body', $aData['answer_body'] )
            ->onDuplicateKeyUpdate()
            ->set( 'answer_title', $aData['answer_title'] )
            ->set( 'answer_body', $aData['answer_body'] )
            ->get();

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $oFormRow->form_answer = $aData['form_answer'];
        $oFormRow->save();

        // вывод списка
        $this->actionFieldList();
    }


    /**
     * Форма редактирования лицензионного соглашения
     */
    protected function actionAgreed() {

        $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $this->iCurrentForm )->asArray()->getOne();

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );

        $aValues = array(
            'form_agreed' => $oFormRow->form_agreed,
            'agreed_title' => $aAddParams['agreed_title'],
            'agreed_text' => $aAddParams['agreed_text']
        );

            // --
        $oFormBuilder = ui\StateBuilder::newEdit();

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'agreed_head'),
            $oFormRow->form_title
        ));

        // добавляем поля
        $oFormBuilder
            ->field('form_agreed', \Yii::t('forms', 'form_agreed'), 'check')
            ->field('agreed_title', \Yii::t('forms', 'field_agreed_title'), 'str')
            ->field('agreed_text', \Yii::t('forms', 'field_agreed_text'), 'html')

            // устанавливаем значения
            ->setValue( $aValues )

            // добавляем элементы управления
            ->buttonSave('agreedSave')
            ->buttonCancel('FieldList')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }

    /**
     * Сохранение лицензионного соглашения
     * @throws \Exception
     * @return void
     */
    protected function actionAgreedSave() {

        // запросить данные
        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception('не выбрана форма');

        if ( !isSet($aData['form_agreed']) OR !isSet($aData['agreed_title']) OR !isSet($aData['agreed_text']) )
            throw new \Exception('не пришли данные');

        Query::InsertInto( 'forms_add_data' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'agreed_title', $aData['agreed_title'] )
            ->set( 'agreed_text', $aData['agreed_text'] )
            ->onDuplicateKeyUpdate()
            ->set( 'agreed_title', $aData['agreed_title'] )
            ->set( 'agreed_text', $aData['agreed_text'] )
            ->get();

        /** @var forms\Row $oFormRow */
        $oFormRow = forms\Table::find( $this->iCurrentForm );
        $oFormRow->form_agreed = $aData['form_agreed'];
        $oFormRow->save();

        // вывод списка
        $this->actionFieldList();
    }


    /**
     * Список полей формы связанных с каталогом
     */
    protected function actionLinkList() {

        // -- обработка данных
        if ( !$this->iCurrentForm )
            throw new \Exception('Не найдена форма!');

        $aItems = Query::SelectFrom( 'forms_links' )->where( 'form_id', $this->iCurrentForm )->getAll();


        // -- сборка интерфейса
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->field('form_field', \Yii::t('forms', 'form_field'), 'string', array('listColumns' => array('flex' => 1)))
            ->field('card_field', \Yii::t('forms', 'card_field'), 'string', array('listColumns' => array('flex' => 1)))

            // добавляем данные
            ->setValue( $aItems )

            // элементы управления
            ->buttonAddNew('addLink')
            ->buttonCancel('FieldList')
            ->buttonRowDelete( 'delLink' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Форма добавление связи
     * @throws \Exception
     * @return int
     */
    protected function actionAddLink() {

        // поля формы
        if ( !$this->iCurrentForm )
            throw new \Exception('Не найдена форма!');

        $aRows = forms\FieldTable::find()->where( 'form_id', $this->iCurrentForm )->getAll();
        $aFormFields = array();
        foreach ( $aRows as $oRow )
            $aFormFields[$oRow->param_name] = $oRow->param_title;

        // поля карточки товара
        $oCard = catalog\Card::get( catalog\Card::DEF_BASE_CARD );
        $aRows = $oCard->getFields();
        $aCardFields = array( 'id' => 'id' );
        if ( $aRows )
            foreach ( $aRows as $oRow )
                $aCardFields[$oRow->name] = $oRow->title;


        // --
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->fieldSelect( 'form_field', \Yii::t('forms', 'form_field'), $aFormFields, [], false )
            ->fieldSelect( 'card_field', \Yii::t('forms', 'card_field'), $aCardFields, [], false )

            // устанавливаем значения
            ->setValue( array() )

            // добавляем элементы управления
            ->buttonSave('saveLink')
            ->buttonCancel('LinkList')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    protected function actionSaveLink() {

        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        if ( !isSet($aData['form_field']) OR !isSet($aData['card_field']) )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        Query::InsertInto( 'forms_links' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'form_field', $aData['form_field'] )
            ->set( 'card_field', $aData['card_field'] )
            ->get();

        $this->actionLinkList();
    }


    protected function actionDelLink() {

        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        if ( !isSet($aData['link_id']) OR !$aData['link_id'] )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        Query::DeleteFrom( 'forms_links' )
            ->where( 'link_id', $aData['link_id']  )
            ->where( 'form_id', $this->iCurrentForm )
            ->get();

        $this->actionLinkList();
    }


    /**
     * Установка служебных данных
     * @param ui\state\BaseInterface $oIface
     */
    protected function setServiceData(ui\state\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'form_id' => $this->iCurrentForm,
            'enableSettings' => $this->enableSettings
        ) );

    }

} //class