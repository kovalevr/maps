<?php

namespace skewer\build\Tool\SEOTemplates;


use skewer\base\site\Type;
use skewer\build\Catalog\Goods\SeoGood;
use skewer\build\Tool\SEOTemplates\view\CloneForm;
use skewer\build\Tool;
use skewer\base\ui;
use skewer\components\seo;
use skewer\components\auth\CurrentAdmin;
use yii\base\UserException;

/**
 * Модуль редактирования шаблонов для SEO параметров
 * Class Module
 * @package skewer\build\Tool\SEOTemplates
 */
class Module extends Tool\LeftList\ModulePrototype {

    public function actionInit(){

        $this->actionList();

    }


    /**
     * Список шаблонов
     */
    public function actionList(){

        $this->setPanelName('', true );

        $aItems = seo\Template::getAll();

        // создаем форму
        $oFormBuilder = ui\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            //->addField( 'id', 'id', 'i', 'hide', array('listColumns' => array('width' => 40)) )
            ->field('fullAlias', \Yii::t('SEO', 'fullAlias'), 'string', array('listColumns' => array('width' => 200)))
            ->field('name', \Yii::t('SEO', 'name'), 'string', array('listColumns' => array('flex' => 1)))
            // добавляем данные
            ->setValue( $aItems )
            // элементы управления
            ->buttonRowUpdate( 'editForm' )
            ->buttonIf( Type::hasCatalogModule(), \Yii::t('SEO','clone'), 'cloneForm', 'icon-clone' )
        ;

        if ( CurrentAdmin::isSystemMode() ) {
            $oFormBuilder
                ->buttonRowDelete( 'delete' )
            ;
        }

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * ФОрма редактирование seo шаблона
     */
    public function actionEditForm(){

        /** @var seo\TemplateRow $oTpl */
        if ( !($iTplId = $this->getInDataValInt('id', 0)) || !($oTpl = seo\Template::find( $iTplId )) )
            throw new UserException(\Yii::t('SEO', 'template_not_found'));

        $oTpl->info = seo\Template::getLabelsInfo(); // todo убрать из базы данных поле

        $oFormBuilder = ui\StateBuilder::newEdit();

        $this->setPanelName( \Yii::t('SEO', 'editseo'), true );

        if ( ($oSeo = seo\SeoPrototype::getInstanceByAlias($oTpl->alias)) === null )
            throw new UserException(\Yii::t('SEO','unknown_template'));

        // добавляем поля
        $oFormBuilder
            ->field('id', 'id', 'hide')
            ->field('name', \Yii::t('SEO', 'name'), 'string')
            ->field('title', \Yii::t('SEO', 'title'), 'string')
            ->field('description', \Yii::t('SEO', 'description'), 'text')
            ->field('keywords', \Yii::t('SEO', 'keywords'), 'text')
            ->fieldIf($oSeo->doSupportNameImage(), 'nameImage', \Yii::t('SEO', 'nameImage'), 'text')
            ->fieldIf($oSeo->doSupportAltTitle(), 'altTitle', \Yii::t('SEO', 'altTitle'), 'text')
            ->field('info', \Yii::t('SEO', 'info'), 'show')

            ->setValue( $oTpl )

            ->buttonSave('update')
            ->buttonCancel('list')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /** Состояние клонирования seo-шаблона */
    public function actionCloneForm() {
        $this->render( new CloneForm() );
    }


    /** Метод ajax-обновления формы клонирования шаблона */
    public function actionUpdateCloneForm() {

        $aData = $this->get('formData', []);

        $this->render( new CloneForm([
            'aValues' => $aData
            ])
        );
    }


    /**
     * Метод клонирования seo-шаблона
     * @throws UserException
     */
    public function actionClone() {

        $aData = $this->getInData();

        if (empty($aData['section']) && empty($aData['card']))
            throw new UserException(\Yii::t('SEO', 'must_specify_one_parameter'));

        $sExtraAlias = (!empty($aData['section']))
                     ? $aData['section']
                     : $aData['card']
        ;

        if ( $oRow = seo\Template::getByAliases(SeoGood::getAlias(), $sExtraAlias) )
            throw new UserException(\Yii::t('SEO', 'template_already_exist'));

        /** @var seo\TemplateRow $oRow */
        $oRow = seo\Template::getByAliases(SeoGood::getAlias());

        $oRow->id = 'NULL';
        $oRow->alias = SeoGood::getAlias();
        $oRow->extraalias = $sExtraAlias;
        $oRow->save();

        $this->actionInit();
    }


    public function actionDelete(){

        try {

            $aData = $this->getInData();

            $iTplId = isSet( $aData['id'] ) ? $aData['id'] : 0;

            if ( !$iTplId )
                $this->addError( 'SEO Tempate not found!' );

            $oTpl = seo\Template::find( $iTplId );

            if ( !$oTpl->delete() )
                throw new \Exception( \Yii::t('SEO', 'template_not_deleted') );
            
            $this->addModuleNoticeReport( \Yii::t('SEO', 'template_deleting'), ['id' => $iTplId] );

        } catch( \Exception $e ) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;
    }


    public function actionUpdate(){

        try {

            $aData = $this->getInData();

            $iTplId = isSet( $aData['id'] ) ? $aData['id'] : 0;

            if ( !$iTplId )
                $this->addError( 'SEO Tempate not found!' );

            $oTpl = seo\Template::find( $iTplId );

            $oTpl->setData( $aData );

            if ( !$oTpl->save() )
                throw new \Exception( \Yii::t('SEO', 'template_not_saved') );

            $this->addModuleNoticeReport( \Yii::t('SEO', 'template_changing'), ['id' => $iTplId] );
            
        } catch( \Exception $e ) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;
    }



}
