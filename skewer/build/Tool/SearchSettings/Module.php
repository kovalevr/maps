<?php

namespace skewer\build\Tool\SearchSettings;

use skewer\components\search;
use skewer\base\section\Parameters;
use skewer\base\site\Type;
use skewer\base\ui\StateBuilder;
use skewer\build\Tool;
use skewer\build\Page\CatalogViewer;
use skewer\base\SysVar;

/**
 * Модуль Настройки поиска
 * Class Module
 * @package skewer\build\Tool\SearchSettings
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $oForm = StateBuilder::newEdit();

        $iType = (int)SysVar::get('Search.default_type');

        if ( Type::hasCatalogModule() )
            $oForm->fieldSelect( 'search_type', \Yii::t('search', 'search_type'), search\Type::getTypeList(), [], false );

        $oForm->fieldSelect('type', \Yii::t('search', 'title_default_type'), search\Type::getSearchTypeList(), [], false );

        if ( Type::hasCatalogModule() )
            $oForm->fieldSelect( 'tpl_name', \Yii::t('search', 'typeTpl'), CatalogViewer\State\ListPage::getTemplates(), [], false  );

        $oForm
            ->field('showTypeSelect', \Yii::t('search', 'showTypeSelect'), 'check')
            ->field('showSectionSelect', \Yii::t('search', 'showSectionSelect'), 'check');

        $oForm->setValue([
            'type'              => $iType,
            'search_type'       => SysVar::get('Search.search_type'),
            'showSort'          => SysVar::get('Search.showSort'),
            'tpl_name'          => SysVar::get('Search.CatalogListTemplate'),
            'showTypeSelect'    => SysVar::get('Search.showTypeSelect'),
            'showSectionSelect' => SysVar::get('Search.showSectionSelect'),
        ]);

        $oForm->buttonSave('save');
        $oForm->buttonCancel();

        $this->setInterface($oForm->getForm());
    }

    /**
     * Сохранение
     */
    protected function actionSave(){
        SysVar::set('Search.default_type', $this->getInDataVal('type'));
        if ( Type::hasCatalogModule() ){
            SysVar::set('Search.search_type', (int)$this->getInDataVal('search_type'));

            SysVar::set('Search.CatalogListTemplate', $this->getInDataVal('tpl_name'));
            $this->setParams( 'sCatalogListTemplate', $this->getInDataVal('tpl_name') );
            $this->setParams( 'search_type', (int)$this->getInDataVal('search_type') );
        }

        SysVar::set('Search.showTypeSelect', (bool)$this->getInDataVal('showTypeSelect'));
        SysVar::set('Search.showSectionSelect', (bool)$this->getInDataVal('showSectionSelect'));

        $this->setParams( 'showTypeSelect', (bool)$this->getInDataVal('showTypeSelect') );
        $this->setParams( 'showSectionSelect', (bool)$this->getInDataVal('showSectionSelect') );

        $this->actionInit();
    }

    /**
     * Устанавливает параметр во все поисковые разделы
     * @param $sParamName
     * @param $value
     */
    private function setParams( $sParamName, $value){

        $aSearchParam = Parameters::getListByModule('Search', 'content');

        if ($aSearchParam){

            foreach( $aSearchParam as $iParam ){
                Parameters::setParams( $iParam, 'content', $sParamName, $value);
            }
        }
    }

} 