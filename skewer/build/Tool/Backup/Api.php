<?php
namespace skewer\build\Tool\Backup;

use skewer\base\orm\Query;
use skewer\helpers\Files;
use skewer\base\queue\ar\Schedule;
use skewer\base\queue as QM;
use skewer\components\gateway;
use yii\base\UserException;

/**
 * API работы с резевным копированием
 */
class Api {

    public static function getListItems(){

        return Service::getBackupList();

    }


    /**
     * Проверка бэкапа
     * @param $iBackupId
     * @return bool
     * @throws \Exception
     */
    public static function checkBackup( $iBackupId ){

        $oClient = gateway\Api::createClient();

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'checkBackup', [$iBackupId], function($mResult, $mError) {

            if (isset($mResult['error'])){
                throw new \Exception($mResult['error']);
            }

            if($mError)  throw new \Exception($mError);

        });

        if(!$oClient->doRequest()) return false;

        return true;

    }

    /**
     * обновление времени запуска создания резервной копии (используется сервисом sms)
     * @static
     * @param string $sTime
     * @return bool
     * @throws UserException
     */
    public static function updBackupTime($sTime){

        $aTaskGlobalTime = explode(':', $sTime);

        $mTaskId = Schedule::getIdByName('make_backup');

        // постановка/снятие задачи в планировщике

        if( !$scheduleItem = Schedule::findOne($mTaskId) ){

            $scheduleItem = new Schedule;
            $command = array('class'=>'skewer\build\Tool\Backup\Service',
                'method'=>'makeBackup',
                'parameters'=>array());

            $command = json_encode($command);

            $aData = [
                'title' => 'Создание резервной копии',
                'name' => 'make_backup',
                'command' => $command,
                'priority' => '1',
                'resource_use' => '7',
                'target_area' => '3',
                'status' => '1',
                'c_min' => $aTaskGlobalTime[1],
                'c_hour' => $aTaskGlobalTime[0],
                'c_day' => NULL,
                'c_month' => NULL,
                'c_dow' => NULL
            ];

        }
        else {

            $aData = [
                'c_min' => $aTaskGlobalTime[1],
                'c_hour' => $aTaskGlobalTime[0]
            ];

        }

        $scheduleItem->setAttributes($aData);

        if ( !$scheduleItem->save() )
            throw new UserException( $scheduleItem );

        return true;

    }


    /**
     * установка параметров резервного копирования
     * @static
     * @param array $aSetting
     * @return bool
     * @throws \Exception|gateway\Exception
     */
    public static function setBackupSetting($aSetting){

        $oClient = gateway\Api::createClient();

        $aParam = array($aSetting);

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'setLocalBackupSetting', $aParam, function($mResult, $mError) {

            if($mError)  throw new \Exception($mError);

        });

        if( !$oClient->doRequest() )
            throw new gateway\Exception("Ошибка соединения с SMS");


        $mTaskId = Schedule::getIdByName('make_backup');

        // постановка/снятие задачи в планировщике
        if( !$mTaskId ){

            $aGlobalSetting = Service::getBackupGlobalSetting();
            $aTaskGlobalTime = explode(':', $aGlobalSetting['bs_time']);

            $scheduleItem = new Schedule;
            $command = array('class'=>'skewer\build\Tool\Backup\Service',
                'method'=>'makeBackup',
                'parameters'=>array());

            $command = json_encode($command);

            $aData = [
                'title' => 'Создание резервной копии',
                'name' => 'make_backup',
                'command' => $command,
                'priority' => '1',
                'resource_use' => '7',
                'target_area' => '3',
                'status' => '1',
                'c_min' => $aTaskGlobalTime[1],
                'c_hour' => $aTaskGlobalTime[0],
                'c_day' => NULL,
                'c_month' => NULL,
                'c_dow' => NULL
            ];

            $scheduleItem->setAttributes($aData);

            if ( !$scheduleItem->save() )
                throw new UserException( $scheduleItem );

        }

        return true;

    }


    /**
     * создание новой резервной копии
     * @static
     * @return int
     * @throws \Exception|gateway\Exception
     */
    public static function createNewBackup(){

        $iTaskId = QM\Api::addTask([
            'class' => '\skewer\base\queue\MethodTask',
            'priority' => QM\Task::priorityHigh,
            'resource_use' => QM\Task::weightCritic,
            'title' => 'create user backup',
            'parameters' => [
                'class' => 'skewer\build\Tool\Backup\Service',
                'method' => 'makeBackup',
                'parameters' => ['user']
            ]
        ]);

        if ( !$iTaskId )
            throw new \Exception('При создании бэкапа задача не создана');

        $oTask = QM\Api::getTaskById( $iTaskId );

        if (!$oTask)
            throw new \Exception('При создании бэкапа задача не получена');

        $oManager = QM\Manager::getInstance();
        $oManager->executeTask( $oTask );

        return $oTask->getStatus();
    }


    /**
     * удаление ранее созданной резервной копии
     * @static
     * @param array $aData
     * @return bool
     * @throws \Exception|gateway\Exception
     */
    public static function removeBackup($aData){

        $oClient = gateway\Api::createClient();

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'removeBackup', $aData, function($mResult, $mError) {

            if($mError)  throw new \Exception($mError);

        });

        if(!$oClient->doRequest()) return false;

        return true;

    }


    /**
     * запрос на восстановление сайта из резервной копии
     * @static
     * @param array $aData
     * @return bool
     * @throws \Exception|gateway\Exception
     */
    public static function recoverBackup($aData){

        // todo положить задачу в очередь

        // дернуть

        $oClient = gateway\Api::createClient();

        /** @noinspection PhpUnusedParameterInspection */
        $oClient->addMethod('HostTools', 'recoverBackup', $aData, function($mResult, $mError) {

            if($mError)  throw new \Exception($mError);

        });

        if(!$oClient->doRequest()) return false;

        return true;
    }

    /**
     * @param $path
     * @throws \Exception
     */
    public static function createDBbackup($path){
        set_time_limit(0);

        $fcache = '';
        $tables = Query::SQL("SHOW TABLES");
        $h = fopen($path,'wb+');

        while($table = $tables->fetchArray()){

            $tableName = end($table);
            $r = Query::SQL("SHOW CREATE TABLE `$tableName`");
            $item = $r->fetchArray();

            $fcache .= "#\tTC`$tableName`\t;\n{$item['Create Table']}\t;\n";

            // Определяем типы полей
            $notNum = array();
            $r = Query::SQL("SHOW COLUMNS FROM `$tableName`");
            $fields = 0;
            while($col = $r->fetchArray()) {
                $notNum[$fields] = preg_match("/^(tinyint|smallint|mediumint|bigint|int|float|double|real|decimal|numeric|year)/", $col['Type']) ? 0 : 1;
                $fields++;
            }

            $aCnt = Query::SQL("SELECT count(*) as cnt FROM `$tableName`;")->fetchArray();
            if ($aCnt['cnt']==0) continue;

            $i     = 0;
            $sline = 0;
            $eline = 100;

            while($i<=$aCnt['cnt']){

                $fcache .= "#\tTD`$tableName`\t;\nINSERT INTO `$tableName` VALUES \n";
                $r = Query::SQL("SELECT * FROM `$tableName` LIMIT $sline, $eline;");

                while($row = $r->fetchArray()){
                    $aFields = array();
                    for($k = 0; $k < $fields; $k++){

                        $field = array_shift($row);
                        if(is_null($field)) {$field = '\N';}
                        elseif($notNum[$k]) {$field =   \Yii::$app->db->pdo->quote($field);} // TODO: Потестить скорость эскэйпинга строк

                        $aFields[] = $field;
                    }
                    $fcache .= '(' . implode(',', $aFields) . "),\n";
                }

                $i     += $eline;
                $sline += $eline;


                $fcache = substr_replace($fcache, "\t;\n",  -2, 2);
                if (strlen($fcache) >= 61440) {
                    fwrite($h,$fcache);
                    $fcache = "";
                }

            }// while data
        }

        fwrite($h,$fcache);
        fclose($h);
    }

    /**
     * @param string $filepath
     * @param string $filter
     * @return array
     */
    static function getDumpFiles($filepath = "", $filter="sql"){
        $out = array();
        if(empty($filepath))   return $out;
        if(!is_dir($filepath)) return $out;
        $d = dir($filepath);
        if($d->handle)
            while(false !== ($entry = $d->read())){
                if($entry != '.' and $entry != '..')
                    if(strtolower($filter) == strtolower(self::geText($entry))){
                        $out[$entry] = array('filename'=>$entry,'filesize'=>Files::getFileSize($filepath.'/'.$entry));
                    }


            }// h
        $d->close();
        return $out;
    }// func

    /**
     * перенесено с двойки, форматирование названия
     * @param string $filename
     * @return string
     */
    static function geText($filename = ""){
        $out = "";
        if(empty($filename))   return $out;
        $filename = explode('.', $filename);
        count($filename) ? $out = $filename[count($filename)-1] : $out = "";
        return $out;
    }// func

    public static function restoreDbase($fileName){
        $query = self::readfile($fileName);

        self::truncateDbase();
        Query::SQL($query);
    }

    private static function readfile($filename = ""){
        $out = "";
        if(!$f = fopen($filename,"r"))return $out;
        while (!feof($f)){
            $out .= fread($f, 8000);
        }// while
        fclose($f);
        return $out;
    }// func read url

    private static function truncateDbase(){

        do {
            $tables = Query::SQL("SHOW TABLES;");
            $aTableNames= array();
            $bFlagDel = false;
            while ($row = $tables->fetchArray()) {
                $bFlagDel = true;
                $tableName = end($row);
                $aTableNames[] = "`$tableName`";

                try{
                    Query::SQL("DROP TABLE IF EXISTS " . implode(',', $aTableNames) . " ;");
                } catch(\Exception $e){

                }
            }
        }while($bFlagDel);
    }// func





}
