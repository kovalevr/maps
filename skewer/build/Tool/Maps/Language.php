<?php

$aLanguage = array();

$aLanguage['ru']['Maps.Tool.tab_name'] = 'Карты';
$aLanguage['ru']['type_map'] = 'Тип карты';
$aLanguage['ru']['settingsGoogle'] = 'Настройки Google';
$aLanguage['ru']['settingsYandex'] = 'Настройки Yandex';
$aLanguage['ru']['autoScale'] = 'Автомасштабирование';
$aLanguage['ru']['api_key'] = 'Ключ Api';
$aLanguage['ru']['clusterize'] = 'Кластеризация';
$aLanguage['ru']['iconMarkers'] = 'Иконка маркера';
$aLanguage['ru']['yandex_addinfo'] = 'Ключ указывается только для коммерческих версий API';

$aLanguage['en']['Maps.Tool.tab_name'] = 'Maps';
$aLanguage['en']['type_map'] = 'Type maps';
$aLanguage['en']['settingsGoogle'] = 'Google Maps settings';
$aLanguage['en']['settingsYandex'] = 'Yandex Maps settings';
$aLanguage['en']['autoScale'] = 'AutoScale';
$aLanguage['en']['api_key'] = 'Api Key';
$aLanguage['en']['clusterize'] = 'Clusterization';
$aLanguage['en']['iconMarkers'] = 'The icon of masrkers';
$aLanguage['en']['yandex_addinfo'] = 'The key is indicated only for the commercial version of the API';
return $aLanguage;