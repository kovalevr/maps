<?php

namespace skewer\build\Page\CatalogFilter;


use skewer\components\catalog;
use skewer\base\site_module;
use skewer\base\section\Page;
use skewer\base\section\Parameters;


/**
 * Модуль вывода формы поиска/фильтрации товарных позиций в каталоге
 * Class Module
 * @package skewer\build\Page\CatalogFilter
 */
class Module extends site_module\page\ModulePrototype {

    /** @var string Заголовок в форме */
    public $title = '';

    /** @var string Шаблон для вывода формы */
    public $tpl = 'filter.twig';

    /** @var int Раздел из которого будет взята форма */
    public $linkedSection = 0;

    public function execute() {

        $aOut = [];

        if ( $this->linkedSection ) {

            // вывод формы поиска с другой станицы
            if ( $sSearchCard = $this->getSearchCard( $this->linkedSection ) ) {

                $aOut = catalog\Filters::get4Card( $sSearchCard );

                $this->setData( 'action', \Yii::$app->router->rewriteURL( "[" . $this->linkedSection . "]" ) );
            }

        } elseif ( $sSearchCard = $this->getSearchCard( $this->sectionId() ) ) {

            // форма поиска
            $aOut = catalog\Filters::get4Card( $sSearchCard );

        } else {

            // форма фильтра
            if ( $this->isShowFilter() )
                $aOut = catalog\Filters::get4Section( $this->sectionId() );

        }


        if ( $aOut ) {

            $this->setData( 'flex', $this->zone == 'content' );
            $this->setData( 'Field4Filter', $aOut );
            $this->setData( 'title', $this->title ? $this->title : \Yii::t('catalogFilter', 'selection') );
            $this->setTemplate( $this->tpl );
        }

        return psComplete;
    }


    private function isShowFilter() {

        return Page::getVal('content', 'showFilter');

    }


    private function getSearchCard( $section ) {

        return Parameters::getValByName( $section, 'content', 'searchCard');

    }


}