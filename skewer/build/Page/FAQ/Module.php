<?php

namespace skewer\build\Page\FAQ;

use skewer\build\Adm\FAQ as AdmFAQ;
use skewer\base\site;
use skewer\base\site_module;
use skewer\components\seo;
use skewer\components\seo\SeoPrototype;
use yii\web\NotFoundHttpException;
use skewer\components\forms;
use skewer\base\section\Tree;

/**
 * Class Module
 * @package skewer\build\Page\FAQ
 */
class Module extends site_module\page\ModulePrototype{

    var $altTitle = '';

    public $onPage = 10;

    private $oFAQRow = null;

    /** @var int */
    public $revert = 0;

    public function init(){
        $this->setParser(parserTwig);
    }

    protected function dateToText($sDate) {

        $arr = explode(' ',$sDate);
        $res = array();
        $res['date'] = $arr[0]; $res['time'] = $arr[1];
        list($res['year'],$res['month'],$res['day']) = explode('-',$arr[0]);
        list($res['hour'],$res['min'],$res['sec']) = explode(':',$arr[1]);

        $sNewDate = $res['day'].' '.\Yii::t('app', 'month_'.$res['month']).' '.$res['year'];
        return $sNewDate;
    }

    public function actionDetail($alias = '', $id = 0) {

        if (!$alias and !$id)
            return $this->actionIndex();

        if ($alias){
            $this->oFAQRow = Api::getFAQByAlias($alias);
        }else{
            $this->oFAQRow = Api::getFAQById($id);
        }

        return $this->showDetail();

    }

    /**
     * Вывод детальной
     * @return int
     * @throws NotFoundHttpException
     */
    private function showDetail(){

        if (!$this->oFAQRow)
            throw new NotFoundHttpException();

        if (!isset($this->oFAQRow['parent']) || $this->oFAQRow['parent'] != $this->sectionId())
            throw new NotFoundHttpException();

        \Yii::$app->router->setLastModifiedDate($this->oFAQRow['last_modified_date']);

        $this->setData('list', array('items' => array($this->oFAQRow)));

        $this->setTemplate('detal.twig');

        // Убрать статический контент
        if ( !site\Page::isCompleteStaticContent() )
            return psWait;
        site\Page::clearStaticContent();

        if ( !site\Page::isCompleteStaticContent2() )
            return psWait;
        site\Page::clearStaticContent2();

        /** H1 */
        if (isset($this->oFAQRow['content']))
            site\Page::setTitle(nl2br(strip_tags($this->oFAQRow['content'])));

        /** Pathline */
        site\Page::setAddPathItem( strip_tags($this->oFAQRow['content']) );

        /** SEO */
        $this->setSeo(new AdmFAQ\Seo(0, $this->sectionId(), $this->oFAQRow));

        return psComplete;
    }

    /**
     * todo нужно вынести метод отправки сообщения из базового метода
     * @return int
     */
    public function actionSend() {
        return $this->actionIndex();
    }

    /**
     * Вывод списка
     * @param int $page
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionIndex($page=1){

        $iCount = 0;
        $aItems = Api::getItems($this->sectionId(), $page, $this->onPage, $iCount);

        \Yii::$app->router->setLastModifiedDate(AdmFAQ\ar\FAQ::getMaxLastModifyDate());

        if ($page != 1 && (!$aItems || !$iCount))
            throw new NotFoundHttpException();

        // расставить отформатированииые даты в записях
        foreach($aItems as $iKey=>$aItem){
            $aItem['date_time'] = date("d.m.Y", strtotime($aItem['date_time']));
            $aItem['content'] = strip_tags($aItem['content']);
            $aItem['full_content'] = $aItem['content'];

            $aItem['full_answer'] = $aItem['answer'];
            $aItem['answer'] = strip_tags($aItem['answer']);

            // обрезаем слишком длинные тексты до 40 слов
            if (count(explode(' ',$aItem['content']))>40){
                $aItem['content'] = implode(' ',array_slice(explode(' ',$aItem['content']),0,39))."...";
            }

            if (count(explode(' ',$aItem['answer']))>40){
                $aItem['answer'] = implode(' ',array_slice(explode(' ',$aItem['answer']),0,39))."...";
            }

            $aItem['content'] = nl2br($aItem['content']);
            $aItem['answer'] = nl2br($aItem['answer']);

            $aItems[$iKey] = $aItem;
        }

        $this->getPageLine($page, $iCount, $this->sectionId(), array(), array('onPage'=>$this->onPage));

        $this->setData('list', array('items' => $aItems));
        $this->setData('section', $this->sectionId());

        $this->showForm();

        $this->setTemplate('view.twig');
        return psComplete;
    }

    /** Вывод формы / обработка данных формы */
    private function showForm() {

        if (!$oForm = forms\Table::getByName('form_faq'))
            return;

        $oForm    = new forms\Entity($oForm, $this->getPost());
        $sLabel   = $this->oContext->getLabel();
        $formHash = $oForm->getHash((int)\Yii::$app->request->post('section', $this->sectionId()), $sLabel);

        if ( ($oForm->getState($sLabel) == forms\Entity::STATE_SEND_FORM) and $oForm->validate($formHash) ) {
            // Отправить форму

            $this->sendAnswer();

            /*Установим флаг о отправке и редиректим на себя же*/
            \Yii::$app->session->set('faq_send','1');
            header('Location: '.Tree::getSectionAliasPath($this->sectionId()));
            exit;

        } elseif(\Yii::$app->session->get('faq_send',0)=='1') {
            /*Снимаем флаг отправки и выводим сообщение*/
            \Yii::$app->session->set('faq_send','0');
            $this->setData('msg', \Yii::t('faq', 'ans_success'));
            $this->setData('back_link', 1);

        } else {
            // Показать форму

            $this->setData('label', $sLabel);
            $this->setData('section', $this->sectionId());
            $this->setData('formHash', $formHash);

            $this->setData('parent', $this->sectionId());

            $this->setData('oForm', $oForm);
        }
    }

    /** Отправить вопрос пользователя в БД */
    private function sendAnswer() {

        $aData = $this->getPost();

        foreach($aData as &$psValue)
            $psValue = strip_tags($psValue);

        $aData['date_time'] = date('Y-m-d H:i:s');
        $aData['status']    = AdmFAQ\Api::statusNew;

        $oFAQRow = AdmFAQ\ar\FAQ::getNewRow($aData);
        if (!$oFAQRow->save())
            return false;

        AdmFAQ\Api::sendMailToAdmin( $oFAQRow->email );
        AdmFAQ\Api::sendMailToClient( $oFAQRow->email );

        return true;
    }

    public function setSeo(SeoPrototype $oSeo){

        $this->setEnvParam(seo\Api::SEO_COMPONENT, $oSeo );
        $this->setEnvParam(seo\Api::OPENGRAPH, '');
        site\Page::reloadSEO();
    }
}