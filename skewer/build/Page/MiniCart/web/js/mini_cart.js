$(document).ready(function(){

    var oCartOpt = {
        count: '.js-b-basketmain .js-goods-count', // Путь к html-элементу числа текущего заказа
        total: '.js-b-basketmain .js-goods-total', // Путь к html-элементу общего числа заказа
        contentCls: 'minicart__content',     // Тело мини-корзины
        emptyCls: 'minicart__empty'          // Скрытый контент мини-корзины
    };

    // Глобальная ф-ция обновления позиций мини-корзины. Используется и в cart.js
    window.updateMiniCart = function (jsonData) {

        var aData = jQuery.parseJSON(jsonData);

        var contentBlock = $('.' + oCartOpt.contentCls);
        var emptyBlock = $('.' + oCartOpt.emptyCls);
        if ( contentBlock.is( '.' + oCartOpt.contentCls + '-hidden' ) ) {
            contentBlock.removeClass(oCartOpt.contentCls + '-hidden');
            emptyBlock.addClass(oCartOpt.emptyCls + '-hidden');
        }

        if (!aData.count) {
            contentBlock.addClass(oCartOpt.contentCls + '-hidden');
            emptyBlock.removeClass(oCartOpt.emptyCls + '-hidden');
        }

        if (aData.count) $(oCartOpt.count).text(aData.count);
        if (aData.total)
            $(oCartOpt.total).text(aData.total);
        else
            $(oCartOpt.total).text(0);
    };

    // Добавление позиции в корзину
    $('.js-btnBuy[href="#tocart"]').click(function() {

        var objectId = $(this).attr('data-id');
        var context = $(this).closest(".js_goods_container");

        if (!context.length)
            context = '';

        var countInput = $('input[data-id=' + objectId + ']',context);
        var count = 1;

        if (countInput.length)
            count = parseInt( countInput.val() );

        var params = {};
        params.moduleName = 'Cart';
        params.cmd = 'setItem';
        params.objectId = objectId;
        params.count = count;
        params.language = $('#current_language').val();

        $.post('/ajax/ajax.php', params, function(response) {

            if (window.updateMiniCart !== undefined)
                updateMiniCart(response);

            var aData = $.parseJSON(response);

            if (aData.errorCount != undefined) {
                container = $(".js_cart_fancy_error:first");
            } else {
                container = $(".js_cart_fancy_add:first");
                container.find('.js-basket-name').html(aData.lastItem.title);
            }

            container.find('.js-basket-name').html(aData.lastItem.title);

            if (count > 0) {

                $.fancybox.open(container.html(),{
                    dataType : 'html',
                    autoSize: true,
                    autoCenter: true,
                    openEffect: 'none',
                    closeEffect: 'none'
                });
            } else alert('Введено неправильное количество товара');
        });

        return false;
    });

    $(document).on('click', '.js-basket-close', function(){

        $.fancybox.close();
        return false;
    });
});