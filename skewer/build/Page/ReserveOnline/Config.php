<?php
/* main */

use skewer\base\site\Layer;

$aConfig['name']         = 'ReserveOnline';
$aConfig['version']      = '1.0';
$aConfig['title']        = 'Виджет бронирования гостиниц';
$aConfig['description']  = 'Модуль виджета бронирования гостиниц online';
$aConfig['revision']     = '0001';
$aConfig['layer']        = Layer::PAGE;
$aConfig['useNamespace'] = true;

$aConfig['dependency'] = [
    ['ReserveOnline', Layer::ADM],
];

return $aConfig;
