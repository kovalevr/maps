<?php

namespace skewer\build\Page\ReserveOnline;

use skewer\components\config\InstallPrototype;
use skewer\build\Adm\ReserveOnline\Api;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {

        // Добавить изображение акции
        @copy(__DIR__ . "/web/images/" . Api::DISCOUNT_DEF_IMG, WEBPATH . 'images/' . Api::DISCOUNT_DEF_IMG);

        return true;
    }// func

    public function uninstall() {

        $this->removeObjectFromLayouts(Api::ADM_GROUP_NAME);

        // Удалить изображение акции
        @unlink(WEBPATH . 'images/' . Api::DISCOUNT_DEF_IMG);

        return true;
    }// func
}
