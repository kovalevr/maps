<?php

namespace skewer\build\Page\ReserveOnline;

use skewer\base\site_module;
use skewer\build\Adm\ReserveOnline\Api;
use skewer\base\section;

/**
 * Модуль онлайн бронирования гостиниц
 * Class Module
 * @package skewer\build\Page\ReserveOnline
 */
class Module extends site_module\page\ModulePrototype {

    /** @var int Флаг состояния модуля */
    public $isWidget = 0;

    /** @var int Флаг показа акции */
    public $show_discount = 0;

    /** @var string Картинка акции */
    public $discount_img = '';

    /** @var string Ссылка акции */
    public $discount_link = '';

    public function init() {

        $this->setParser(parserTwig);
        return true;
    }

    public function execute() {

        // Вывод виджета
        if ($this->isWidget) {

            // Найти раздел с установленным модулем бронирования
            $iSectionId = Api::getReserveOnlineSectionByLang(\Yii::$app->language);

            /** url-адрес раздела с модулем настройки параметров */
            $sSectionPath = ($iSectionId) ? section\Tree::getSectionAliasPath($iSectionId, true) : '';

            if ($sSectionPath) {
                if ($this->show_discount)
                    $this->setData('discount_img', $this->discount_img);
                $this->setData('discount_link', $this->discount_link);
                $this->setData('reserve_section_path', $sSectionPath);
                $this->setData('dateFrom', date('d.m.Y', time()));
                $this->setData('dateTo', date('d.m.Y', time() + 60 * 60 * 24 * 2));
                $this->setTemplate('reserve_online_widget.twig');
            }
            return psComplete;
        }

        // Вывод библиотеки сервиса
        if ( $mHotelId = Api::getHotelId($this->sectionId()) ) {
            $this->setData('hotelId', $mHotelId);
            // Пока нет немецкой версии у стороннего виджета, использовать английскую
            $this->setData('currentLang', ( (\Yii::$app->language == 'ru') ? 'ru' : 'en') );
            $this->setTemplate('reserve_online_lib.twig');
        }

        return psComplete;
    }
}
