<?php

namespace skewer\build\Page\GalleryViewer;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/GalleryViewer/web/';

    public $css = [
        'css/skin.css'
    ];

    public $js = [
        'js/init.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\libs\jquery\CarouFredSelAsset',
        'skewer\libs\jquery\Asset'
    ];

}