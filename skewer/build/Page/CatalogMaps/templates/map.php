<?php

use skewer\base\SysVar;
use skewer\build\Page\CatalogMaps;

/**
 * @var $this \yii\web\View
 * @var array $settings
 * @var string $MarkerClusterAssetUrl
 */

?>

<?php
    if ( SysVar::get('Maps.type_map', '') == CatalogMaps\Api::providerYandexMap ){
        CatalogMaps\Assets\AssetYandexMap::register( \Yii::$app->view );
    } elseif ( CatalogMaps\Api::providerGoogleMap ){
        CatalogMaps\Assets\AssetGoogleMap::register( \Yii::$app->view );
    }

?>

<div class="js_maps">
    <div class="js_map_settings" style="display: none;"><?=$settings?></div>

</div>
<div class="js_asset_url" style="display: none;"><?=$MarkerClusterAssetUrl?></div>
