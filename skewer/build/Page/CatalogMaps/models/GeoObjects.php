<?php

namespace skewer\build\Page\CatalogMaps\models;

use Yii;

/**
 * This is the model class for table "geoObjects".
 *
 * @property integer $id
 * @property integer $map_id
 * @property string $latitude
 * @property string $longitude
 */
class GeoObjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geoObjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'map_id'], 'required'],
            [['latitude'],  'double', 'min'=> -90, 'max' => 90],
            [['longitude'], 'double', 'min'=> -180, 'max' => 180]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('maps', 'ID'),
            'map_id' => Yii::t('maps', 'map_id'),
            'latitude' => Yii::t('maps', 'latitude'),
            'longitude' => Yii::t('maps', 'longitude'),
        ];
    }
}
