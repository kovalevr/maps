<?php

namespace skewer\build\Page\CatalogMaps;

use skewer\base\site_module;
use yii\helpers\StringHelper;


class Module extends site_module\page\ModulePrototype {

    /** @const Имя группы параметров модуля */
    const group_params_module = 'CatalogMaps';

    /** @var string Список каталожных разделов, объекты которых выводятся на карту */
    public $sSourceSections;

    /** @var int id карты */
    public $iMapId;

    public function init(){

        $this->setParser(parserPHP);
        return true;
    }

    public function execute() {

        $aSourceSections = StringHelper::explode($this->sSourceSections, ',', true, true);

        $aMarkers = Api::getMarkersFromCatalogSections(
            $aSourceSections,
            $this->getModuleDir() . DIRECTORY_SEPARATOR . $this->getTplDirectory() . DIRECTORY_SEPARATOR . 'infoWindow.php'
        );

        $sHtmlMap = Api::buildMap($aMarkers, $this->iMapId);

        if ( $sHtmlMap === false )
            return psBreak;

        $this->setData( 'map', $sHtmlMap );
        $this->setTemplate( 'main.php' );

        return psComplete;
    }


}
