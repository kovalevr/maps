<?php

$aLanguage = array();

$aLanguage['ru']['CatalogMaps.Page.tab_name'] = 'Просмотр карты';
$aLanguage['ru']['param_map'] = 'Карта';
$aLanguage['ru']['param_sourceSections'] = 'Разделы с объктами';
$aLanguage['ru']['param_groupTitle'] = 'Настройки карты';

$aLanguage['en']['CatalogMaps.Page.tab_name'] = 'View map';
$aLanguage['en']['param_map'] = 'Map';
$aLanguage['en']['param_sourceSections'] = 'Sections with objects';
$aLanguage['en']['param_groupTitle'] = 'Map settings';

return $aLanguage;
