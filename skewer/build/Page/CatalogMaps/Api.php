<?php

namespace skewer\build\Page\CatalogMaps;

use skewer\base\ft\Editor;
use skewer\base\SysVar;
use skewer\build\Page\CatalogMaps\models\Maps;
use skewer\components\catalog\GoodsSelector;
use skewer\libs\GoogleMarkerClusterer;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Api{

    /** @const Тех.имя провайдера карт - Yandex */
    const providerYandexMap = 'yandex';

    /** @const Тех.имя провайдера карт - Google */
    const providerGoogleMap = 'google';

    /**
     * Вернёт глобальные настройки выбранного типа карты
     * @param string $sTypeMap - тип карты
     * @return array
     */
    public static function getGlobalSettingMap($sTypeMap ){
        return array(
          'clusterize'  => (bool)SysVar::get( self::getSysVarName($sTypeMap, 'clusterize'), false),
          'iconMarkers' => SysVar::get( self::getSysVarName($sTypeMap, 'iconMarkers'), '')
        );
    }

    /**
     * Список провайдеров карт
     * @return array
     */
    public static function getMapProviders(){
        return array(
            self::providerYandexMap => 'Yandex',
            self::providerGoogleMap => 'Google',
        );
    }

    /**
     * Получить полное имя SysVar переменной
     * @param string $sProviderMap - провайдер карты
     * @param string $sParamName   - название параметра
     * @return string
     */
    public static function getSysVarName( $sProviderMap, $sParamName ){
        // todo Язык надо брать из модуля
        $sLanguageCategory = 'Maps';
        return $sLanguageCategory . '.' . $sProviderMap . '_' . $sParamName;
    }

    /**
     * Вернёт урл скрипта активного провайдера карт с подключенной библиотекой поиска
     * или false - если не выбран провайдер
     * @param string $sLang - текущий язык
     * @return bool|string
     */
    public static function getUrlMapScriptWithSearch( $sLang = 'en' ){

        $sTypeProvider = SysVar::get('Maps.type_map', '');

        switch ( $sTypeProvider ){
            case self::providerGoogleMap:
                $sUrl = Assets\AssetGoogleMap::buildSrcScript(['places']);

                break;
            case self::providerYandexMap:
                $sUrl = Assets\AssetYandexMap::buildUrlScript( $sLang );

                break;
            default:
                $sUrl = false;
        }

        return $sUrl;

    }

    /**
     * Построит карту
     * @param array $aMarkers  - массив маркеров
     * @param int|null $iMapId - id карты
     * @return bool|string - вернет результат html с картой или false - в случае ошибки
     */
    public static function buildMap( $aMarkers = [], $iMapId = null ){

        if ( !($sTypeMap = SysVar::get('Maps.type_map', '')) )
            return false;

        // Глобальные настройки модуля
        $aGlobalSettingsMaps = Api::getGlobalSettingMap( $sTypeMap );

        // Локальные настройки модуля
        $aLocalSettingsMaps  = [];

        if ( $iMapId )
            $aLocalSettingsMaps = Maps::getSettingsMapById($iMapId);

        $aSettingsMaps = ArrayHelper::merge( $aGlobalSettingsMaps, $aLocalSettingsMaps );

        $aSettings = [
            'markers'     => $aMarkers,
            'mapSettings' => $aSettingsMaps
        ];

        return \Yii::$app->getView()->renderPhpFile(
            __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'map.php',
            [
                'settings' => Json::htmlEncode($aSettings),
                'MarkerClusterAssetUrl' => \Yii::$app->assetManager->getPublishedUrl( (new GoogleMarkerClusterer\Asset())->sourcePath )
            ]
        );

    }


    /**
     * Вернет маркеры для карты из товаров $aSections разделов
     * @param array $aSections - каталожные разделы
     * @param string $sPathTemplate4PopupMessage - путь к шаблону всплывающего сообщения
     * @return array массив маркеров
     */
    public static function getMarkersFromCatalogSections(array $aSections, $sPathTemplate4PopupMessage = '' ){

        if ( !$aSections )
            return [];

        $aGoods = GoodsSelector::getList4Section( $aSections )
            ->condition('active', 1)
            ->parse()
        ;
        $aMarkers = [];

        foreach ($aGoods as $aGood) {
            foreach ($aGood['fields'] as $aField) {
                if ( $aField['type'] === Editor::MAP_SINGLE_MARKER ){

                    $iLatitude  = ArrayHelper::getValue($aField, 'latitude', null);
                    $iLongitude = ArrayHelper::getValue($aField, 'longitude', null);

                    if ( is_null($iLatitude) || is_null($iLongitude) )
                        continue;

                    $aMarker = [
                        'latitude'  => $iLatitude,
                        'longitude' => $iLongitude,
                        'title'     => $aGood['title']
                    ];

                    if ( $sPathTemplate4PopupMessage ){

                        $aMarker['popup_message'] = \Yii::$app->getView()->renderPhpFile(
                            $sPathTemplate4PopupMessage,
                            [ 'aGood' => $aGood ]
                        );
                    }

                    $aMarkers[] = $aMarker;
                }

            }

        }

        return $aMarkers;

    }

}