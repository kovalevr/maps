<?php

namespace skewer\build\Page\Subscribe;

use skewer\components\config\installer\Api;
use skewer\components\config\InstallPrototype;
use skewer\components\i18n\models\ServiceSections;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {
        //Добавим в шаблон нового раздела fastSubscribe

        $iTplNew = \Yii::$app->sections->getValue('tplNew');

        $sQuery = "INSERT INTO `parameters` ( `parent`, `group`, `name`, `value`, `title`, `access_level`, `show_val`) VALUES
                    ( '$iTplNew', 'fastSubscribe', 'layout', 'left,right,center', '', 0, ''),
                    ( '$iTplNew', 'fastSubscribe', 'object', 'Subscribe', '', 0, ''),
                    ( '$iTplNew', 'fastSubscribe', 'enable', '1', '', 0, ''),
                    ( '$iTplNew', 'fastSubscribe', 'mini', '1', '', 0, '');";

        \Yii::$app->db->createCommand($sQuery)->execute();

        return true;
    }// func

    public function uninstall() {
        //Удалим метку "fastSubscribe"
        \Yii::$app->db->createCommand('DELETE FROM `parameters` WHERE `group`="fastSubscribe"')->execute();

        //Удаление из лаяутов метки
        \Yii::$app->db->createCommand("UPDATE `parameters` SET `value`= REPLACE(`value`, 'fastSubscribe', '') WHERE `group`='.layout'")->execute();
        return true;
    }// func

} //class