<?php

namespace yii\web;

namespace skewer\build\Page\CatalogViewer\State;

use yii\base\Exception;

class FastCompleteException extends Exception{

    public $iStatusProcess = psComplete;

    public function getStatusProcess(){
        return $this->iStatusProcess;
    }

    public function __construct($iStatusProcess, $message = "", $code = 0, Exception $previous = null){
        $this->iStatusProcess = $iStatusProcess;
        parent::__construct($message, $code, $previous);
    }


}
