<?php
/**
 * @var $this \yii\base\View
 *  @var array $aGood
 */
use yii\helpers\ArrayHelper;

?>
<? foreach ($aGood['fields'] as $aField): ?>
    <? if ( ArrayHelper::getValue($aField, 'attrs.show_in_map') && ($sHtml = ArrayHelper::getValue($aField, 'html'))  ): ?>
        <div><? if ($sTitle = ArrayHelper::getValue($aField, 'title')) :?><strong><?=$sTitle?>:</strong> <?endif;?><?=$sHtml?></div>
    <? endif; ?>
<? endforeach; ?>
