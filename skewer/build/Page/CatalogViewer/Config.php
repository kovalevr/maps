<?php


/* main */
use skewer\base\site\Layer;

$aConfig['name']     = 'CatalogViewer';
$aConfig['title']    = 'Просмотр каталога';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль вывода каталога';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::PAGE;
$aConfig['languageCategory']     = 'catalog';

$aConfig['dependency'] = array(
    array('CatalogFilter', Layer::PAGE),

    array('Order', Layer::ADM),
    array('Catalog', Layer::ADM),

    array('CardEditor', Layer::CATALOG),
    array('Dictionary', Layer::CATALOG),
    array('Goods', Layer::CATALOG),
    array('LeftList', Layer::CATALOG),
);

/* Настраиваемые параметры модуля */
$aConfig['param_settings'] = 'skewer\build\Page\CatalogViewer\ParamSettings';

return $aConfig;
