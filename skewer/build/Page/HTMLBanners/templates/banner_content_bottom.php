<?php use skewer\components\design\Design;

if ((isset($dataProvider)) and(count($dataProvider)>0)): ?>
    <? foreach($dataProvider as $item): ?>
        <div class="b-bannercenter"<? if (Design::modeIsActive()): ?> sklabel="<?= $_objectId ?>"<? endif; ?>>
            <?= $item['content'] ?>
        </div>
    <?endforeach?>
<?endif?>