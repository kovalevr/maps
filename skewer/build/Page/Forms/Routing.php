<?php

namespace skewer\build\Page\Forms;

use skewer\base\router\RoutingInterface;


/**
 * Class Routing
 * @package skewer\build\Page\Forms
 */
class Routing implements RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/*response/',
        );
    }

    /**
     * Проверяем используется ли в данный момент какой то из паттернов
     */
    public static function patternUsed(){

        foreach (self::getRoutePatterns() as $sInRule) {
            $sInRule = str_replace('*','',$sInRule);
            $sInRule = str_replace('/','',$sInRule);
            if (strpos($_SERVER['REQUEST_URI'],$sInRule)!==false){
                return true;
            }
        }
        return false;

    }

}
