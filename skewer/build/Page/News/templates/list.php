<?php

use skewer\components\design\Design;
use skewer\components\gallery\Photo;
use skewer\components\gallery\models\Photos;
use skewer\base\SysVar;
use skewer\helpers\Html;
/**
 * @var \skewer\build\Adm\News\models\News $item
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var string $_objectId
 * @var bool $showDetailLink
 * @var string $date фильр по дате
 */
?>
<?php if ($dataProvider->count>0): ?>
<div class="b-news b-news_list"<? if (Design::modeIsActive()): ?> sktag="modules.news" sklabel="<?= $_objectId ?>"<? endif ?>>
    <? foreach($dataProvider->getModels() as $item): ?>
        <?php
        /* @var \skewer\build\Adm\News\models\News $item */
        $hrefParam = ($item->news_alias)?"news_alias={$item->news_alias}":"news_id={$item->id}";
        $href = ($item->hyperlink)?$item->hyperlink:("[{$item->parent_section}][News?".$hrefParam."]");
        ?>
        <div class="news__item">
            <? if (!SysVar::get('News.hideGallery')) :?>
                <div class="news__imgbox">
                    <? /** @var Photos $oFirstImage */
                    $oFirstImage = ($aPhotos = Photo::getFromAlbum($item->gallery, true, 1)) ? $aPhotos[0] : false;
                    $sFirstImgPath = ($oFirstImage && isset($oFirstImage->images_data['list']['file'])) ? $oFirstImage->images_data['list']['file'] : false;

                    if ($sFirstImgPath):?>
                        <img src="<?=$sFirstImgPath?>" title="<?= htmlspecialchars($oFirstImage->title,ENT_QUOTES) ?>" alt="<?= htmlspecialchars($oFirstImage->alt_title,ENT_QUOTES) ?>">
                    <? else: ?>
                        <? $oBundle = \skewer\build\Page\News\Asset::register($this); ?>
                        <img src="<?= $oBundle->baseUrl.'/images/news.noimg_list.gif' ?>">
                    <? endif;?>
                </div>
            <? endif; ?>
            <div class="news__wrap">
                <div class="news__title">
                    <? if (Html::hasContent($item->full_text) || $item->hyperlink):?>
                        <a<?= Design::write(' sktag="modules.news.title"') ?> href="<?= $href ?>"><?= $item->title ?></a>
                    <? else: ?>
                        <span<?= Design::write(' sktag="modules.news.normal"') ?>><?= $item->title ?></span>
                    <? endif ?>
                </div>
                <div class="news__date"<?= Design::write(' sktag="modules.news.date"') ?>><?= Yii::$app->getFormatter()->asDate($item->publication_date,'php:d.m.Y') ?></div>
                <div class="b-editor"<?= Design::write(' sktag="editor"') ?>>
                    <?= $item->announce ?>
                </div>
                <div class="g-clear"></div>
                <? if ($showDetailLink && ($item->full_text || $item->hyperlink)): ?>
                    <p class="news__linkback">
                        <a href="<?= $href ?>"><?= \Yii::t('page', 'readmore') ?></a>
                    </p>
                <? endif ?>
            </div>
        </div>
    <? endforeach ?>
</div>
<?= ($dataProvider->getModels())?yii\widgets\LinkPager::widget([
        'pagination'=>$dataProvider->getPagination(),
        'firstPageLabel'=>\Yii::t('page', 'page_first'),
        'lastPageLabel'=>\Yii::t('page', 'page_last'),
        'nextPageLabel'=> '&gt;',
        'prevPageLabel'=> '&lt;',
        'options'=>['class'=>'b-pageline']]):""
    ?>
<?php endif ?>
