<?php

namespace skewer\build\Page\RecentlyViewed;
use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }// func

    public function install() {
        $iNewPageSection = \Yii::$app->sections->tplNew();

        $this->addParameter($iNewPageSection, 'object', 'RecentlyViewed', '', 'RecentlyViewed', '');
        $this->addParameter($iNewPageSection, 'layout', 'head,content', '', 'RecentlyViewed', '');

        return true;
    }// func

    public function uninstall() {
        $iNewPageSection = \Yii::$app->sections->tplNew();

        $this->removeParameter($iNewPageSection, 'object', 'RecentlyViewed');
        $this->removeParameter($iNewPageSection, 'layout', 'RecentlyViewed');

        return true;
    }// func

}// class