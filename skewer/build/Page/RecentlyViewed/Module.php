<?php

namespace skewer\build\Page\RecentlyViewed;

use skewer\base\site_module;
use skewer\components\catalog\Card;
use skewer\components\catalog\GoodsSelector;

/**
 * Публичный модуль вывода недавно просмотренных товаров
 * Class Module
 * @package skewer\build\Page\RecentlyViewed
 */
class Module extends site_module\page\ModulePrototype {

    /** @var \skewer\components\catalog\GoodsRow AR товарной позиции */
    private $aCurrentGoods = null;

    /** @var array Набор товарных позиций для вывода */
    private $aGoods = array();

    /** @var int Кол-во позиций на страницу */
    public $onPage = 10;

    /** @var string Шаблон для вывода */
    private $sTpl = 'RecentlyViewed.twig';

    /** @var int ID текущего товара */
    private $iGoodsId;

    public function init() {
    }

    public function build() {

        $this->iGoodsId = $this->getEnvParam('iCatalogObjectId');

        // создаем в сессии массив "недавно смотрели"
        if (!isset($_SESSION['recentlyViewedItems']))
            $_SESSION['recentlyViewedItems'] = array();

        $sBaseCardName = Card::DEF_BASE_CARD;

        // получение id товара если на детальной и добавление в конец массива сессии
        if ($this->iGoodsId != false) {
            $this->aCurrentGoods = GoodsSelector::get($this->iGoodsId , $sBaseCardName);

            // проверка на существование и активность
            if (!empty($this->aCurrentGoods) AND $this->aCurrentGoods['active']) {
                $iObjectId = $this->aCurrentGoods['id'];

                //проверка если не первый раз смотрят этот товар
                if (array_search($iObjectId,$_SESSION['recentlyViewedItems']) !== false) {
                    $key = array_search($iObjectId,$_SESSION['recentlyViewedItems']);
                    unset($_SESSION['recentlyViewedItems'][$key]);
                }
            }
        }

        // получение набора товаров
        if ($this->onPage > 0 && !empty($_SESSION['recentlyViewedItems'])) {
            $this->getGoods();
        }

        // если на детальной странице товара - добавляем его всессию
        if (isset($iObjectId))
            array_unshift($_SESSION['recentlyViewedItems'], $iObjectId);

        // проверяем кол-во просмотренных товаров
        if (count($_SESSION['recentlyViewedItems'])>$this->onPage)
            array_pop($_SESSION['recentlyViewedItems']);

        // парсинг
        $this->setData('aObjectList', $this->aGoods);
        $this->setTemplate($this->sTpl);
    }

    public function execute(){

        // Откладываем запуск если не отработал модуль каталога
        $oCatalogViewerModule = $this->getProcess( 'out.content', psAll );

        if (($oCatalogViewerModule instanceof site_module\Process ) && !$oCatalogViewerModule->isComplete()) {
            return psWait;
        } else {
            $this->build();
        }

        return psComplete;
    }

    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    private function getGoods() {

        $this->aGoods = GoodsSelector::getList( Card::DEF_BASE_CARD )
            ->condition( 'active', 1 )
            ->condition('id',$_SESSION['recentlyViewedItems'])
            ->parse()
        ;

        // Сортировка по порядку просмотра
        usort($this->aGoods, function ($a, $b) {
            $i = array_search($a['id'], $_SESSION['recentlyViewedItems']);
            $j = array_search($b['id'], $_SESSION['recentlyViewedItems']);
            if ($i > $j) return 1;
            if ($i < $j) return -1;
            return 0;
        });

        return true;
    }
}