<?php

namespace skewer\build\Page\Main\templates\head\example;

use yii\web\AssetBundle;

/**
 * Класс для примера в #47194.
 * todo Удалить при выпуске релиза + подключенные в нем файлы тоже
 * Class ExampleAsset
 * @package skewer\build\Page\Main
 */
class ExampleAsset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Main/templates/head/example/web/';

    public $css = [
        'css/example.css'
    ];

    public $js = [
        'js/example.js'
    ];

//    public $jsOptions = [
//        'position'=>View::POS_HEAD
//    ];

//    public $depends = [
//        'skewer\build\Page\Main\PrintAsset',
//    ];


}
