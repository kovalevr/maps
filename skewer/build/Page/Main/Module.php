<?php

namespace skewer\build\Page\Main;


use skewer\base\section\Page;
use skewer\base\section\Tree;
use skewer\base\section\Parameters;
use skewer\base\site\Layer;
use skewer\base\SysVar;
use \skewer\build\Page\Main\Seo as SeoData;
use skewer\build\Design\Zones\Api;
use skewer\components\content_generator\Asset;
use skewer\components;
use skewer\components\design\Design;
use skewer\base\site\Site;
use skewer\base\site_module;
use skewer\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;
use skewer\build\Design\Zones;


/**
 * Модуль построения раздела публичной части сайта
 */
class Module extends site_module\page\ModulePrototype  {
    /**
     * Id текущего раздела
     * @var int
     */
    public $sectionId = 0;

    /** @var string Шаблон вывода */
    public $templateFile = '';

    /**
     * Зоны
     * @var array
     */
    protected $aZonesByGroup = [];

    /**
     * параметр если установлен - раздел скрывется из поиска и sitemap
     * Называеться должен как значение skewer\base\section\Tree::removeFromSearchParam
     * @var mixed
     */
    public $removeFromSearch = false;

    public function init() {

        $this->setParser(parserPHP);

        if ( !$this->templateFile )
            throw new ServerErrorHttpException( 'Root process template is not set' );

    }// func

    /**
     * Исполнение модуля
     * @return int
     */
    public function execute() {

        $this->sectionId =  $this->getInt('sectionId', $this->sectionId);


        /**
         * #fix404
         * на 404 может перекинуть после отработки нескольких модулей, надо сбросить уже установленные параметры окружения
         */
        if ($this->sectionId == \Yii::$app->sections->page404()){
            \Yii::$app->environment->clear();
            // и не учитывать неразобранный хвост URL
            \Yii::$app->router->setUriParsed();
        }

        $this->setEnvParam('sectionId', $this->sectionId);

        $this->setData('sectionId',$this->sectionId);

        $this->setData('mainId', \Yii::$app->sections->main());

        if ( Design::modeIsActive() ) {
            $this->setData('designMode', Design::getDirList() );
        }

        foreach( Page::getByGroup(Parameters::settings) as $aParamsSet){
            $this->setData($aParamsSet['name'], ['value' => $aParamsSet['value'], 'text' => $aParamsSet['show_val']]);
        }

        $this->setLayout();

        $this->setProcess();

        Asset::setSectionId($this->sectionId);

        $this->setSEO(new SeoData($this->sectionId(), $this->sectionId()));

        $this->setData('SiteName', Site::domain());

        $this->setData('canonical_url', false);

        $this->setTemplate( $this->templateFile );

        return psComplete;

    }

    /**
     * Обрабатываем шаблон расположения (layout)
     */
    protected function setLayout(){

        $aZones = [];
        $this->aZonesByGroup = [];
        foreach (Page::getByGroup(Api::layoutGroupName) as $sZone => $sZoneVal){

            if ($sZone != '.title' && $sZoneVal['show_val']){
                $aZoneSource = explode(',', $sZoneVal['show_val']);
                foreach ($aZoneSource as $label){

                    if( mb_substr($sZone, 0, 1) === '.' )
                        continue;

                    $aZones[$sZone][] = trim($label);
                    $this->aZonesByGroup[trim($label)] = $sZone;

                } // foreach

            }// if

        } // foreach

        $this->setData(Api::layoutGroupName, $aZones);

    }


    /**
     * Формирование списка процессов
     * В список будут включены модули:
     *  * активированные в дизайнерском режиме
     *  * не содержащие параметра layout (системные)
     *  * имеющие флаг force_include
     */
    protected function setProcess(){

        foreach(Page::getGroups() as $sGroupName) {

            if ($sGroupName == Parameters::settings)
                continue;
            
            if (Page::getVal($sGroupName, Parameters::object)) {

                // флаг добавления процесса в список на обработку
                $bAdd = false;
                
                $aParams = Page::getByGroup($sGroupName);
                $aParams = ArrayHelper::map($aParams, 'name', 'value');

                // добавление зоны, если есть
                if ( isset($this->aZonesByGroup[$sGroupName]) ) {
                    $aParams['zone'] = $this->aZonesByGroup[$sGroupName];
                    $bAdd = true;
                }
                
                // если нет метки зоны вывода, то модуль системный - добавляем принудительно
                if ( !isset($aParams['layout']) )
                    $bAdd = true;

                // флаг принудаительно включения модйля в список на обработку
                if (isset($aParams['force_include']) and $aParams['force_include'])
                    $bAdd = true;

                // Добавление процесса, если есть флаг
                if ( $bAdd ) {
                    $className = site_module\Module::getClass(Page::getVal($sGroupName, Parameters::object), Layer::PAGE);
                    $this->addChildProcess(new site_module\Context($sGroupName, $className, ctModule, $aParams));
                }

            }

        }// each

    }


    public function setSEO(components\seo\SeoPrototype $oSeo) {

        $this->setEnvParam(components\seo\Api::SEO_COMPONENT, $oSeo );

        $this->setEnvParam( components\seo\Api::OPENGRAPH,
            \Yii::$app->getView()->renderPhpFile(
                $this->getModuleDir() . DIRECTORY_SEPARATOR . $this->getTplDirectory() . DIRECTORY_SEPARATOR . 'OpenGraph.php',
                ['oTree' => Tree::getSection($this->sectionId()), 'oSeoComponent' => $oSeo]
            )
        );

    }

    /** @inheritdoc */
    public function beforeRender() {

        if (!SysVar::get('lock_section_flag',false))
            return;

        $bHasContent = false;

        // перебираем все инициализированные процессы в дереве
        foreach ( \Yii::$app->processList->aProcessesPaths as $oProcess ) {

            // берем активные модули ...
            $oModule = $oProcess->getModule();
            if ( !$oModule)
                continue;

            // ... наследники прототипа клиентского модуля ...
            if ( !$oModule instanceof site_module\page\ModulePrototype)
                continue;

            // ... которые могут иметь контент ...
            if ( !$oModule->canHaveContent() )
                continue;

            // для отладки раскомментировать следующую строку
            // var_dump( $oModule->getModuleName().' - '.(Html::hasContent($oModule->oContext->getOuterText()) ? 'HAS CONTENT' : ''), $oModule->oContext->getOuterText() );

            if ( Html::hasContent($oModule->oContext->getOuterText()) )
                $bHasContent = true;

        }

        if (!$bHasContent) {
            $Layout = $this->getData(Zones\Api::layoutGroupName);
            $aContentZone = ArrayHelper::getValue($Layout, 'content');
            if ( $aContentZone ) {
                // добавить в зону вывода
                $aContentZone[] = 'devSection';
                $Layout['content'] = $aContentZone;
                $this->setData(Zones\Api::layoutGroupName, $Layout);
                $this->setData('devSection', \Yii::t('page', 'lock_section_text_value'));
            }
        }

    }

}
