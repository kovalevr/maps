<?php

namespace skewer\build\Catalog\ViewSettings;


use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\base\section\Parameters;
use skewer\base\ui;
use skewer\build\Page\CatalogViewer;
use skewer\base\SysVar;


class Module extends ModulePrototype {


    public function actionInit() {

        $form = ui\StateBuilder::newEdit();

        $form
            ->headText( \Yii::t( 'catalog', 'head_title' ) )
            ->fieldSelect( 'listTemplate', \Yii::t( 'catalog', 'listTpl'), self::getTplList() )
        ;


        if ( SysVar::get('catalog.goods_related') )
            $form->fieldSelect( 'relatedTpl', \Yii::t( 'catalog', 'relatedTpl'), self::getTplList() );

        if ( SysVar::get('catalog.goods_include') )
            $form->fieldSelect( 'includedTpl', \Yii::t( 'catalog', 'includedTpl'), self::getTplList() );

        $form

            ->fieldInt( 'onPage', \Yii::t('Catalog', 'listCnt'), ['subtext' => \Yii::t('catalog', 'onPage_label'), 'minValue' => 0] )
            ->fieldSelect( 'showFilter', \Yii::t('Catalog', 'showListFilters'), self::getCheckList(), [], false )
            ->fieldSelect( 'showSort', \Yii::t('Catalog', 'showListSort'), self::getCheckList(), [], false )

            ->setValue([
                'listTemplate' => '',
                'relatedTpl' => '',
                'includedTpl' => '',
                'onPage' => '',
                'showFilter' => '0',
                'showSort' => '0',
            ])

            ->buttonConfirm('save', \Yii::t('adm', 'save'), \Yii::t('catalog', 'save_msg'), 'icon-save')
        ;


        $this->setInterface( $form->getForm() );
    }


    public function actionSave() {

        $keys = ['listTemplate','relatedTpl','includedTpl','onPage','showFilter','showSort'];
        $data = $this->getInData();

        foreach ( $keys as $key ) {
            $val = isSet( $data[$key] ) ? $data[$key] : '';
            if ( $val ) {
                if ( $val == -1 ) $val = 0;
                $this->saveParam( $key, $val );
            }
        }

        $this->addMessage( '', \Yii::t('catalog', 'good_save_msg' ) );
        \Yii::$app->router->updateModificationDateSite();
        
        $this->actionInit();
    }


    protected function saveParam( $key, $val ) {
        /* @todo обновление может затронуть некаталожные разделы! */
        return Parameters::updateByName( 'content', $key, $val );
    }


    protected function getTplList() {

        $aList = array( 'list', 'gallery', 'table' );

        $aOut = array();
        foreach ($aList as $sName)
            $aOut[$sName] = \Yii::t('catalog', 'tpl_'.$sName);

        return $aOut;
    }


    protected function getCheckList() {

        $aList = array( 'list', 'gallery', 'table' );

        $aOut = array();
        foreach ($aList as $sName)
            $aOut[$sName] = \Yii::t('catalog', 'tpl_'.$sName);

        return [
            0 => \Yii::t('catalog', 'dontchange'),
            1 => \Yii::t('catalog', 'yes'),
            -1 => \Yii::t('catalog', 'no'),
        ];
    }

}