<?php

namespace skewer\build\Catalog\Goods;

use skewer\build\Catalog\Goods\model\GoodsEditor;
use skewer\build\Catalog\Goods\view\ModGoodsEditor;
use skewer\build\Catalog\Goods\view\SectionList;
use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\components\catalog;
use skewer\base\section\Tree;
use skewer\base\ui;
use skewer\base\SysVar;
use skewer\components\seo\Service;
use skewer\components\seo\Api;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Каталог. Админка.
 * Class Module
 * @package skewer\build\Catalog\Goods
 */
class Module extends ModulePrototype {

    const categoryField = '__category';
    const mainLinkField = '__main_link';

    /** @var int число элементов на страницу */
    public $iOnPage = 30;

    /** @var int текущий номер страницы ( с 0, а приходит с 1 ) */
    public $iPage = 0;

    /** @var array Данные для фильтрации списка товаров, чтобы исключить выбор самих себя */
    public $aFilterData = array();

    /**
     * Получение идентификатора текущего раздела
     * @return int|mixed
     */
    public function getCurrentSection() {
        $section = $this->getFilterSection();
        if ( !$section )
            $section = $this->sectionId();
        return $section;
    }


    /**
     * Возвращает имя карточки для раздела $section
     * @param int $section ID раздела
     * @return mixed
     */
    protected function getCard4Section( $section ) {
        return catalog\Section::getDefCard( $section );
    }


    /**
     * Возвращает имя текущей каталожной карточки
     * @return string
     * @throws UserException
     */
    public function getCardName() {
        return $this->getCard4Section( $this->getCurrentSection() );
    }


    protected function preExecute() {
        $this->iPage = $this->getInt( 'page', $this->getInnerData( 'page', 0 ) );
        $this->setInnerData( 'page', $this->iPage );
    }


    /**
     * Получение значений полей фильтра
     * @param string[] $aFilterFields Список полей фильтра
     * @return array
     */
    protected function getFilterVal( $aFilterFields ) {

        $aFilter = array();

        foreach ( $aFilterFields  as $sField ) {
            $sName = 'filter_' . $sField;
            $sVal = $this->getStr( $sName, $this->getInnerData( $sName, '' ) );
            $this->setInnerData( $sName, $sVal );
            $aFilter[ $sField ] = $sVal;
        }

        return $aFilter;
    }


    /**
     * Возвращает значения поля раздел из фильтра
     * @return mixed
     */
    public function getFilterSection() {
        return ArrayHelper::getValue( $this->getFilterVal( ['section'] ), 'section', 0 );
    }


    /**
     * Набор выделенных позиций при мультивыделении в списке
     * @return array
     */
    protected function getMultipleData() {

        $aData = $this->get( 'data' );
        $aItems = [];

        if ( $this->getInDataVal( 'multiple' ) ) {

            if ( isset($aData['items']) && is_array($aData['items']) )
                foreach ( $aData['items'] as $aItem )
                    $aItems[] = ArrayHelper::getValue( $aItem, 'id', 0 );

        } else {

            if ( $id = $this->getInDataValInt( 'id' ) )
                $aItems = [$id];

        }

        return $aItems;
    }


    protected function actionInit() {

        $this->iOnPage = SysVar::get('catalog.countShowGoods');
        if ( !$this->iOnPage ) $this->iOnPage = 30;

        $this->sectionId = 0;

        $iInitParam = (int)$this->get('init_param');

        if ($iInitParam)
            $this->actionEdit($iInitParam);
        else
            $this->actionList();
    }


    /**
     * Интерфейс задания карточки для категории
     */
    public function actionSetCard() {

        $this->setPanelName( \Yii::t('catalog', 'goods_card_select') );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect( 'card', \Yii::t('catalog', 'section_goods' ), catalog\Card::getGoodsCardList( true ), [], false )
            ->setValue( ['card' => $this->getCardName()] )
            ->buttonSave( 'saveCard' )
            ->buttonCancel( 'list' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Состояние сохранения карточки для категории
     */
    public function actionSaveCard() {

        if ( !( $sCardName = $this->getInDataVal( 'card' ) ) ) {

            $this->addError( \Yii::t('catalog', 'error_no_card') );
            $this->actionSetCard();
        } else {

            catalog\Section::setDefCard( $this->getCurrentSection(), $sCardName );

            if (isset($this->defCard))
                $this->defCard = $sCardName;

            $this->actionInit();
        }

    }


    protected function actionList() {

        $this->setInnerData( 'currentGoods', 0 );
        $this->setPanelName( \Yii::t('catalog', 'goods_list') );

        $model = model\SectionList::get( $this->getCurrentSection() )
            ->setFilter( $this->getFilterVal( ['article', 'title', 'price', 'active'] ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->render(new view\SectionList([
            'model' => $model
        ]));
    }


    /**
     * Сортировка товарных позиций внутри раздела
     */
    protected function actionSort() {

        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        $iDropId = isSet( $aDropData['id'] ) ? $aDropData['id'] : false;
        $aItems = $this->getMultipleData();

        if( !count($aItems) || !$iDropId || !$sPosition )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

        if ( $sPosition == 'after' )
            $aItems = array_reverse($aItems);

        // todo научить сортировать в один проход
        foreach ( $aItems as $iSelectId )
            catalog\model\SectionTable::sortSwap( $this->getCurrentSection(), $iSelectId, $iDropId, $sPosition );
    }


    /**
     * Установка товарной позиции на первое место в списке
     */
    protected function actionSetFirst() {

        $aData = $this->getInData();

        if( !isSet($aData['id']) || !$aData['id'] )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

        catalog\model\SectionTable::sortUp( $this->getCurrentSection(), $aData['id'] );

        $this->actionList();
    }


    protected function actionEdit($iId=0) {

        $this->setPanelName( \Yii::t('catalog', 'good_editor') );

        $aData = $iId ? ['id' => $iId] : $this->get('data');

        /** @var model\GoodsEditor $model */
        $model = model\GoodsEditor::get()
            ->setSectionData( $this->getCurrentSection(), $this->getCardName(), $iId ?: $this->getInnerData( 'currentGoods' ) )
            ->setData( $aData, $this->get( 'from' ) )
        ;
        $model->setUpdField( $this->get( 'field_name' ) );

        $aOldData = empty($aData['id']) ? ['id' => null] : catalog\GoodsSelector::get($aData['id']);

        if ( $model->save() ) {

            /** @var bool $bIsCommonList Это общий список товаров(Фильтр = Раздел:Все)?  */
            $bIsCommonList = !(bool)$this->getCurrentSection();

            if (!$bIsCommonList){
                $aGood = catalog\GoodsSelector::get($model->getGoodsRow()->getBaseRow()->getVal('id'));

                $oOldSeo = new SeoGood(ArrayHelper::getValue($aOldData, 'id', 0), $this->getCurrentSection(), $aOldData);
                $oOldSeo->setExtraAlias( catalog\Section::getDefCard($this->getCurrentSection()) );

                $oNewSeo = new SeoGood(ArrayHelper::getValue($aGood, 'id', 0), $this->getCurrentSection(), $aGood);
                $oNewSeo->setExtraAlias( catalog\Section::getDefCard($this->getCurrentSection()) );

                Api::saveJSData( $oOldSeo, $oNewSeo, $aData);

            }

            $aItem = $model->getGoodsRow()->getData();

            if (Service::$bAliasChanged)
                $this->addMessage(\Yii::t('tree','urlCollisionFlag',['alias'=>$aItem['alias']]));

            $this->actionList();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

            if ( isset($aData['price']) )
                $model->getGoodsRow()->getBaseRow()->setVal('price', $aData['price']);

            $this->setInnerData( 'currentGoods', $model->getGoodsRow()->getRowId() );
            $this->setInnerData( 'page', 0 );

            $this->setInnerData('viewClass',view\GoodsEditor::className());
            view\GoodsEditor::$bOnlyActive = true;
            $this->render(new view\GoodsEditor([
                'model' => $model
            ]));
        }

    }


    /**
     * Метод, меняющий на лету значения поля "Основной раздел" при
     * изменении поля "Категория"
     */
    protected function actionLoadSection() {

        // todo ref
        $aFormData = $this->get('formData', array());
        $mSectionList = isset($aFormData[self::categoryField]) ? $aFormData[self::categoryField] : '';
        $iSection = isset($aFormData[self::mainLinkField]) ? $aFormData[self::mainLinkField] : 0;

        $aSectionKeys = explode( ',', $mSectionList );
        $aSectionList = Tree::getSectionsTitle( $aSectionKeys );

        if ( count($aSectionKeys) && ( !$iSection || !in_array($iSection, $aSectionKeys) ) )
            $iSection = array_shift( $aSectionKeys );

        $oForm = ui\StateBuilder::newEdit();
        $oForm
            ->fieldSelect(self::mainLinkField,  '', $aSectionList, [], false)

            ->setValue([
                self::mainLinkField => $iSection,
            ]);

        $this->setInterfaceUpd($oForm->getForm());
    }


    /**
     * Дублирование товара
     */
    protected function actionClone() {

        /** @var model\GoodsEditor $model */
        $model = model\GoodsEditor::get()
            ->setSectionData( $this->getCurrentSection(), '' )
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
        ;

        if ( $id = $model->saveNew() ) {
            $this->actionList();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

        }

    }


    /**
     * Удалнение записи
     */
    public function actionDelete() {

        /** @var model\GoodsEditor $model */
        $model = model\GoodsEditor::get()
            ->setSectionData( $this->getCurrentSection(), '' )//
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
        ;

        if ( $model->multipleDelete() ) {

            $this->actionList();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

        }

    }

    // Сопутствующие
    // -- RELATED --
    public function actionRelatedItems() {

        $this->setPanelName( \Yii::t('catalog', 'relatedItems_title') );

        $this->setInnerData('filter_section',$this->sectionId());

        $model = model\RelatedList::get( $this->getInnerData( 'currentGoods' ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->render(new view\RelatedList([
            'model' => $model
        ]));
    }


    public function actionAddRelatedItem() {

        $this->setPanelName( \Yii::t('catalog', 'relatedItems_add_title') );

        $model = model\FullCustomList::get( $this->getStr( 'filter_section', $this->getInnerData( 'filter_section', 0 ) ) )
            ->getWithoutRelated( $this->getInnerData( 'currentGoods' ) )
            ->setFilter( $this->getFilterVal( array('title', 'section') ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->render(new view\AddRelatedList([
            'model' => $model
        ]));
    }


    public function actionLinkRelatedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $item )
                    $list[] = isset( $item['id'] ) ? $item['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        /*Сброс фильтра по секции*/
        $this->setInnerData('filter_section',$this->sectionId());

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    catalog\model\SemanticTable::link( catalog\Semantic::TYPE_RELATED, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionRelatedItems();

    }


    public function actionRemoveRelatedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $aItem )
                    $list[] = isset( $aItem['id'] ) ? $aItem['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    catalog\model\SemanticTable::unlink( catalog\Semantic::TYPE_RELATED, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionRelatedItems();

    }

    // В комплекте
    // -- INCLUDED --
    public function actionIncludedItems() {

        $this->setPanelName( \Yii::t('catalog', 'includedItems_title') );

        $this->setInnerData('filter_section',$this->sectionId());

        $model = model\IncludedList::get( $this->getInnerData( 'currentGoods' ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->render(new view\IncludedList([
            'model' => $model
        ]));

    }


    public function actionAddIncludedItem() {

        $this->setPanelName(\Yii::t('catalog', 'includedItems_add_title') );

        $model = model\FullCustomList::get( $this->getStr( 'filter_section', $this->getInnerData( 'filter_section', 0 ) ) )
            ->getWithoutIncluded( $this->getInnerData( 'currentGoods' ) )
            ->setFilter( $this->getFilterVal( array('title', 'section') ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->render(new view\AddIncludedList([
            'model' => $model
        ]));

    }


    public function actionLinkIncludedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $item )
                    $list[] = isset( $item['id'] ) ? $item['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        /*Сброс фильтра по секции*/
        $this->setInnerData('filter_section',$this->sectionId());

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    catalog\model\SemanticTable::link( catalog\Semantic::TYPE_INCLUDE, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionIncludedItems();

    }


    public function actionRemoveIncludedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $aItem )
                    $list[] = isset( $aItem['id'] ) ? $aItem['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    catalog\model\SemanticTable::unlink( catalog\Semantic::TYPE_INCLUDE, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionIncludedItems();

    }

    // Модификации
    // -- Modifications --
    public function actionModificationsItems() {
        // получаем идентификатор основного товара
        if ( !( $id = $this->getInnerData( 'currentGoods' ) ) ) {
            $aData = $this->get( 'data' );
            if ( isSet( $aData['id'] ) && $aData['id'] ) {
                $id = $aData['id'];
                $this->setInnerData( 'currentGoods', $id );
            }

        }

        $model = model\ModificList::get( $id )
            //->limit( $this->iPage, $this->iOnPage )
        ;

        $data = $model->getCurrentObject()->getData();
        $this->setPanelName( \Yii::t('catalog', 'modificationsItems_title') . ' "' . $data['title'] . '"' );

        $this->render(new view\ModificList([
            'model' => $model
        ]));

    }


    public function actionEditModificationsItem() {

        $aData = $this->get( 'data' );

        /** @var model\ModGoodsEditor $model */
        $model = model\ModGoodsEditor::get( $this->getInnerData( 'currentGoods' ) )
            ->setData( $aData, $this->get( 'from' ) )
            ->setUpdField( $this->get( 'field_name' ) )
        ;

        $data = $model->getMainObject()->getData();
        $this->setPanelName( \Yii::t('catalog', 'modificationsItems_edit_title') . ' "' . $data['title'] . '"' );

        $aOldData = empty($aData['id']) ? catalog\GoodsSelector::get($model->getMainObject()->getRowId()) : catalog\GoodsSelector::get($aData['id']);

        if ( $model->save() ) {

            /** @var bool $bIsCommonList Это общий список товаров(Фильтр = Раздел:Все)? */
            $bIsCommonList = !(bool)$this->getCurrentSection();

            if (!$bIsCommonList){
                $aGood = catalog\GoodsSelector::get($model->getGoodsRow()->getBaseRow()->getVal('id'), catalog\Card::DEF_BASE_CARD);
                
                Api::saveJSData(
                    new SeoGoodModifications(ArrayHelper::getValue($aOldData, 'id', 0), $this->getCurrentSection(), $aOldData),
                    new SeoGoodModifications(ArrayHelper::getValue($aGood, 'id', 0),    $this->getCurrentSection(), $aGood),
                    $model->getData()
                );
            }

            $this->actionModificationsItems();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );
            $this->setInnerData('viewClass',view\ModGoodsEditor::className());
            $this->render(new view\ModGoodsEditor([
                'model' => $model
            ]));
        }

    }


    public function actionDeleteModificationsItem() {

        $model = model\ModGoodsEditor::get( $this->getInnerData( 'currentGoods' ) )
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
        ;

        if ( $model->multipleDelete() ) {

            $this->actionModificationsItems();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

        }
    }


}
