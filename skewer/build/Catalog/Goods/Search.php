<?php


namespace skewer\build\Catalog\Goods;

use skewer\base\orm\Query;
use skewer\components\search\Prototype;
use skewer\components\search\Row;
use skewer\components\catalog;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\base\SysVar;
use skewer\components\catalog\Parser;
use skewer\components\seo\Api;

class Search extends Prototype{

    const CLASS_NAME = 'CatalogViewer';

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return self::CLASS_NAME;
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {
        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        // todo здесь нузно знать с какой базовой карточкой мы работаем
        // если появится множество базовых карточек, то можно применить подход с
        //      вычислением базовой карточки как в коллекциях
        $iBaseCard = 1;

        $oGoods = catalog\Card::getItemRow( $iBaseCard, ['id' => $oSearchRow->object_id] );

        /*Если нет товаров - выходим*/
        if (!$oGoods) return false;

        // Если отключены модификации, то не включать в индекс и карту сайта модификации товара
        if (!SysVar::get('catalog.goods_modifications')) {
            
            $bIsModifications = (bool)catalog\model\GoodsTable::find()
                ->where('base_id', $oSearchRow->object_id)
                ->where('parent !=?', $oSearchRow->object_id)
                ->asArray()
                ->getOne();

            $oSearchRow->use_in_search = false;
            $oSearchRow->use_in_sitemap = false;

            if ($bIsModifications) return $oSearchRow->save();
        }

        // Если у товара слетел базовый раздел (например удалён), то здесь произведётся попытка установить новый
        $iSectionId = catalog\Section::getMain4Goods( $oSearchRow->object_id );

        if (!$iSectionId)
            return false;

        $oSearchRow->section_id = $iSectionId;

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        /* У длинных артикулов заиндексировать первые 3 символа для возможности поиска по ним */
        $sArticleSearch = $oGoods->article;
        if ($aArticleParts = explode(' ', $oGoods->article)) {
            if (mb_strlen($aArticleParts[0]) > 3)
                $sArticleSearch = mb_substr($aArticleParts[0], 0, 3) . ' ' . $sArticleSearch;
        }

        // todo пересмотреть формирование текста - должно быть по галочке
        $sText = trim(
            "$sArticleSearch "
            . $this->stripTags(isSet($oGoods->announce) ? $oGoods->announce . ' ' : '')
            . $this->stripTags(isSet($oGoods->obj_description) ? ' ' . $oGoods->obj_description . ' ' : '')
        );

        /** условия для удаления товара из поискового индекса */
        if ( !isSet($oGoods->active) || !$oGoods->active )
            return false;

        /**
         * @todo добавляться должны несколько записей ???
         */
        $oSearchRow->language      = Parameters::getLanguage($iSectionId);
        $oSearchRow->search_text   = $this->stripTags($sText);
        $oSearchRow->search_title  = $this->stripTags($oGoods->title);
        $oSearchRow->href          = Parser::buildUrl($iSectionId, $oSearchRow->object_id, $oGoods->alias);
        $oSearchRow->status        = 1;
        $oSearchRow->use_in_search = true;
        $oSearchRow->use_in_sitemap = true;

        $oSeoComponent = new SeoGood($oSearchRow->object_id, $oSection->id);
        $oSeoComponent->loadDataEntity();
        $oSEOData = Api::get( SeoGood::getGroup(), $oSearchRow->object_id, $oSection->id );

        $oSearchRow->priority = ($oSEOData && !empty($oSEOData->priority))
            ? $oSEOData->priority
            : $oSeoComponent->calculatePriority();

        $oSearchRow->frequency = ($oSEOData && !empty($oSEOData->frequency))
            ? $oSEOData->frequency
            : $oSeoComponent->calculateFrequency();

        $oSearchRow->modify_date = catalog\model\GoodsTable::getChangeDate($oSearchRow->object_id,$iBaseCard);

        return $oSearchRow->save();

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',base_id  FROM c_goods WHERE 1";
        Query::SQL($sql);
    }

}