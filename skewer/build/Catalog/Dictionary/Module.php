<?php

namespace skewer\build\Catalog\Dictionary;

use skewer\build\Catalog\CardEditor;
use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\components\catalog;
use skewer\base\ui;
use skewer\base\ft;
use yii\base\UserException;
use skewer\components\gallery\Profile;

/**
 * Модуль для редактирования записей в каталоге
 * Class Module
 * @package skewer\build\Catalog\Dictionary
 */
class Module extends ModulePrototype {


    // число элементов на страницу
    public $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    public $iPage = 0;

    protected function getCard() {

        $card = $this->getInnerData( 'card', 0 );

        if ( !$card )
            $card = $this->getInDataVal( 'id' );


        $this->setInnerData( 'card', $card );

        return $card;
    }

//    protected function preExecute() {
//        $this->iPage = $this->getInt( 'page', $this->getInnerData( 'page', 0 ) );
//        $this->setInnerData( 'page', $this->iPage );
//    }


    /**
     * Иницализация
     */
    protected function actionInit() {
        $this->actionList();
    }


    /**
     * Список справочников
     */
    protected function actionList() {

        $this->setInnerData( 'card', 0 );

        $oFormBuilder = ui\StateBuilder::newList();

        // установка заголовка
        $this->setPanelName( \Yii::t('dict', 'dict_list_for_cat') );

        $oFormBuilder
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'dict_name'), ['listColumns.flex' => 1] )
            ->setValue( catalog\Card::getDictionaries() )
            ->buttonAddNew('New', \Yii::t('dict', 'create_dict'))
            ->buttonRowUpdate( 'View' )

            ->setEditableFields(['title'], 'ChangeDictName')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }

    /**
     *  Действие. Изменение имени справочника
     */
    protected function actionChangeDictName() {
        $iDictid    = (int)$this->getInDataVal( 'id', 0 );
        $sDictTitle = $this->getInDataVal( 'title', '' );

        if ($iDictid AND $sDictTitle) {

            $oDict = catalog\Card::get($iDictid);

            if ($oDict) {
                $oDict->title = $sDictTitle;
                $oDict->save();

                ui\StateBuilder::updRow($this, $oDict->getData());

            }
        }

    }

    /**
     * Набор значений справочника
     * todo сделать постраничный вывод
     * todo сделать вывод дополнительных полей в списке
     */
    protected function actionView() {

        // обработка входных данных
        $card = $this->getCard();

        $aItems = catalog\Dict::getValues($card);

        // установка заголовка
        $this->setPanelName( \Yii::t('dict', 'dict_panel_name', catalog\Card::getTitle( $card )) );

        // контентная форма
        $oFormBuilder = ui\StateBuilder::newList();

        $oFormBuilder
            ->fieldShow( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'title'), ['listColumns.flex' => 1] )
            ->setValue( $aItems )

            ->buttonAddNew('ItemEdit', \Yii::t('dict', 'add'))
            ->buttonEdit('FieldList', \Yii::t('dict', 'struct'))
            ->buttonCancel('List', \Yii::t('dict', 'back'))

            ->buttonSeparator( '->' )

            ->buttonDelete('Remove', \Yii::t('dict', 'del'))
            ->buttonRowUpdate( 'ItemEdit' )
            ->buttonRowDelete( 'ItemRemove' )

            ->setEditableFields(['title'], 'ItemSave')
        ;

        $oFormBuilder->enableDragAndDrop('sort');

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Список полей справочника
     * @throws UserException
     */
    protected function actionFieldList() {

        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        // генерация объектов для работы
        $oCard = catalog\Card::get( $card );


        // создаем интерфейс
        $oFormBuilder = ui\StateBuilder::newList();

        $sHeadText = \Yii::t( 'card', 'head_card_name', $oCard->title );
        $this->setPanelName( \Yii::t( 'card', 'title_field_list',$oCard->title) );

        $oFormBuilder->headText( sprintf('<h1>%s</h1>', $sHeadText) );

        // добавляем поля
        $oFormBuilder
            ->fieldShow( 'id', 'id' )
            ->fieldString( 'name', \Yii::t( 'card', 'field_f_name') )
            ->fieldString( 'title', \Yii::t( 'card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'editor', \Yii::t( 'card', 'field_f_editor') )
            ->widget( 'editor', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyEditorWidget' );

        // устанавливаем значения, исключая поле для сортировки
        $aFields = $oCard->getFields();
        foreach ($aFields as $iKey => $oField)
            if ($oField->name == catalog\Card::FIELD_SORT)
                unset($aFields[$iKey]);
        $oFormBuilder->setValue($aFields);

        // элементы управления
        $oFormBuilder
            ->buttonAddNew('FieldEdit', \Yii::t('card', 'btn_add_field'))
            ->buttonCancel('View', \Yii::t('card', 'btn_back'));
        ;

        // обработчики на список
        $oFormBuilder
            ->buttonRowUpdate( 'FieldEdit' )
            ->buttonRowDelete( 'FieldRemove' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Интерфейс создания/редактирования поля справочника
     */
    protected function actionFieldEdit() {

        // входные параметры
        $card = $this->getInnerData( 'card' );
        $iFieldId = $this->getInDataVal( 'id', false );

        if ( !$card )
            throw new UserException( "Card not found!" );

        /** @var catalog\model\FieldRow $oItem */
        $oCard = catalog\Card::get( $card );

        if ( $iFieldId ) {
            $oItem = catalog\Card::getField( $iFieldId );
            $this->setPanelName( \Yii::t('dict', 'title_edit_field') );
        } else {
            $oItem = catalog\Card::getField();
            $this->setPanelName( \Yii::t('dict', 'title_new_field') );
        }

        $aProfiles = Profile::getAll(true, true, true);

        // Добавить использующийся профиль галереи в независимости от его активности
        if ($oItem->editor == ft\Editor::GALLERY) {
            if ($aProfileCurrent = Profile::getById($oItem->link_id))
                $aProfiles[$oItem->link_id] = $aProfileCurrent['title'];
        }

        // создание формы
        $oFormBuilder = ui\StateBuilder::newEdit();

        // добавление полей
        $oFormBuilder
            ->headText( '<h1>' . \Yii::t( 'card', 'head_card_name', $oCard->title ) . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t( 'card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t( 'card', 'field_f_name'), ['disabled' => (bool)$iFieldId] )
            ->fieldSelect( 'editor', \Yii::t( 'card', 'field_f_editor'), CardEditor\Api::getSimpleTypeList( false ), ['onUpdateAction'=>'UpdFieldLinkId'], false )
            ->fieldSelect( 'link_id', \Yii::t('card', 'field_f_link_id'), $aProfiles, ['hidden' => !$oItem->isLinked()], false )
            ->fieldString( 'def_value', \Yii::t( 'card', 'field_f_def_value') )
        ;

        // добавление значений
        $oFormBuilder
            ->setValue( $oItem );

        // элементы управления
        $oFormBuilder
            ->buttonSave('FieldSave')
            ->buttonCancel('FieldList')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }

    /** Обработчик изменения значения поля editor ("Тип отображения") */
    public function actionUpdFieldLinkId() {

        $aFormData = $this->get('formData', array());
        $sEditor = isset($aFormData['editor']) ? $aFormData['editor'] : '';
        $iTypeId = isset($aFormData['link_id']) ? $aFormData['link_id'] : '';

        $id = isset($aFormData['id']) ? $aFormData['id'] : null;
        $oField = catalog\Card::getField($id);
        $oField->editor = $sEditor;

        $oForm = ui\StateBuilder::newEdit();
        $oForm->fieldSelect('link_id',  \Yii::t( 'gallery', 'profiles_select'), Profile::getAll(true, true, true), ['hidden' => !$oField->isLinked()], false)
            ->setValue([
                'link_id' => $iTypeId,
            ]);

        $this->setInterfaceUpd($oForm->getForm());
    }


    protected function actionFieldSave() {

        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( \Yii::t( 'card', 'error_card_not_found') );

        $data = $this->getInData();
        $id = $this->getInDataVal( 'id', null );

        if ( !$this->getInDataVal( 'title' ) )
            throw new UserException( \Yii::t( 'card', 'error_no_field_name') );

        if ( !$this->getInDataVal( 'editor' ) )
            throw new UserException( \Yii::t( 'card', 'error_no_editor_for_field') );

        $oField = catalog\Card::getField( $id );
        $oField->setData( $data );
        $oField->entity = $card;
        $oField->save();

        $oCard = catalog\Card::get( $card );
        $oCard->updCache();

        $this->actionFieldList();
    }


    protected function actionFieldRemove() {

        $id = $this->getInDataVal( 'id', null );

        if ( !$id )
            throw new UserException( \Yii::t('dict', 'error_field_not_found') );

        $oField = catalog\Card::getField( $id );

        if ( in_array( $oField->name, ['id', 'title'] ) )
            throw new UserException( \Yii::t('dict', 'error_field_cant_removed') );

        $oField->delete();
        $oCard = catalog\Card::get( $oField->entity );
        $oCard->updCache();

        $this->actionFieldList();
    }


    /**
     * Форма редактирования значения для справочника
     * @throws UserException
     */
    protected function actionItemEdit() {

        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        $mItem = $id ? catalog\Dict::getValues($card, $id) : ft\Cache::getMagicTable($card)->getNewRow();

        // установка заголовка
        $oDict = catalog\Card::get( $card );
        $this->setPanelName( \Yii::t('dict', 'dict_panel_name', catalog\Card::getTitle( $card )) );

        $oFormBuilder = ui\StateBuilder::newEdit();

        $oFormBuilder
            ->fieldHide( 'id', 'id' )
        ;

        $aFields = $oDict->getFields();
        foreach ( $aFields as $oField ) {
            if ($oField->name != catalog\Card::FIELD_SORT)
                $oFormBuilder->fieldByEntity($oField);
        }

        $oFormBuilder
            ->setValue( $mItem ? : [] )
            ->buttonSave('ItemSave')
            ->buttonCancel('View')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Сохранение значения для справочника
     */
    protected function actionItemSave() {

        $aData = $this->getInData();
        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        catalog\Dict::setValue($card, $aData, $id);

        $this->actionView();
    }


    /**
     * Удаление записи из справочника
     */
    protected function actionItemRemove() {

        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        catalog\Dict::removeValue($card, $id);

        $this->actionView();
    }


    /**
     * Добавление нового справочника
     */
    protected function actionNew() {

        $oCard = catalog\Card::get();
        $this->setPanelName( \Yii::t('dict', 'new_dict') );

        $oFormBuilder = ui\StateBuilder::newEdit();
        $oFormBuilder
            ->headText( '<h1>' . \Yii::t('dict', 'new_dict') . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('dict', 'dict_name'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('dict', 'system_name') )
            ->setValue( $oCard )
            ->buttonSave('Add')
            ->buttonCancel('List')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Состояние сохранения нового справочника
     */
    protected function actionAdd() {

        $aData = $this->getInData();

        $sDictTitle = $this->getInDataVal( 'title' );

        if ( !$sDictTitle )
            throw new UserException( \Yii::t('dict', 'error_noname_seted') );

        $oCard = catalog\Card::addDictionary( $aData );

        $this->setInnerData( 'card', $oCard->id );

        $this->actionView();

    }


    /**
     * Действие удаления справочника
     */
    protected function actionRemove() {

        $mCardId = $this->getInnerData( 'card' );

        if (!$mCardId)
            throw new UserException( \Yii::t('dict', 'error_not_selected') );

        if ( !catalog\Card::get($mCardId) )
            throw new UserException( \Yii::t('dict', 'error_dict_not_found') );

        $aErrorMessages = [];
        catalog\Dict::removeDict($mCardId, $aErrorMessages);

        if ($aErrorMessages)
            throw new UserException( \Yii::t('dict', 'error_del_usage_dict'). '<br>' . join('<br>', $aErrorMessages) );

        $this->setInnerData( 'card', 0 );

        $this->actionList();

    }

    /**
     * Сортировка значений справочников
     */
    protected function actionSort() {

        $aItemDrop   = $this->get('data');
        $aItemTarget = $this->get('dropData');
        $sOrderType  = $this->get('position');

        if ($aItemDrop and $aItemTarget and $sOrderType)
            catalog\Dict::sortValues($this->getCard(), $aItemDrop, $aItemTarget, $sOrderType);

        $this->actionView();
    }

} 