/**
 * Библиотека для отображения слайда для баннера
 */
Ext.define('Ext.Adm.SlideShower',{

    extend: 'Ext.form.field.Base',

    border: 0,
    padding: 5,
    minWidth: 1000,
    width: 1280,

    addWyswygConfig: {},

    fieldSubTpl:  [
        '<div class="b-banner">' +
            '<div class="banner__item">' +
            '<div id="ban_dd_lab_1" class="banner__text1" style="' +
            '<tpl if="text1_h !== false">left: {text1_h}px;</tpl>' +
            '<tpl if="text1_v !== false">top: {text1_v}px;</tpl>' +
            '"><div id="ban_edit_lab_1" contenteditable="true" class="builder-show-field">{text1}</div><span class="js_sdd ban-dd">text1</span></div>' +
            '<div id="ban_dd_lab_2" class="banner__text2" style="' +
            '<tpl if="text2_h !== false">left: {text2_h}px;</tpl>' +
            '<tpl if="text2_v !== false">top: {text2_v}px;</tpl>' +
            '"><div id="ban_edit_lab_2" contenteditable="true" class="builder-show-field">{text2}</div><span class="js_sdd ban-dd">text2</span></div>' +
            '<div id="ban_dd_lab_3" class="banner__text3" style="' +
            '<tpl if="text3_h !== false">left: {text3_h}px;</tpl>' +
            '<tpl if="text3_v !== false">top: {text3_v}px;</tpl>' +
            '"><div id="ban_edit_lab_3" contenteditable="true" class="builder-show-field">{text3}</div><span class="js_sdd ban-dd">text3</span></div>' +
            '<div id="ban_dd_lab_4" class="banner__text4" style="' +
            '<tpl if="text4_h !== false">left: {text4_h}px;</tpl>' +
            '<tpl if="text4_v !== false">top: {text4_v}px;</tpl>' +
            '"><div id="ban_edit_lab_4" contenteditable="true" class="builder-show-field">{text4}</div><span class="js_sdd ban-dd">text4</span></div>' +
            '</div>'+
            '<img class="js_banner_bg" src="{img}" alt="{title}" {addAttr}></div>'
    ],
    listeners: {
        // кроп интерфейс вызывается только при необходимости
        afterrender: function( me ) {

            $("[id^=ban_dd_lab_]").draggable({
                containment:".b-banner",
                handle: ".js_sdd",
                stop: function(){
                    var cur_id = this.id.substr(11);
                    sk.removeCKEditorOnPlace( 'ban_edit_lab_' + cur_id );
                    sk.initCKEditorOnPlace( 'ban_edit_lab_' + cur_id, me.addWyswygConfig );
                }
            });

        },
        beforedestroy: function() {

            for( var i=1; i<5; i++ )
                sk.removeCKEditorOnPlace( 'ban_edit_lab_' + i );

            $("[id^=ban_dd_lab_]").remove();

        }
    },

    execute: function( data ) {

        if ( data[0]['addWyswygConfig'] )
            this.addWyswygConfig = data[0]['addWyswygConfig'];

        var me = this;
        var img = $("img.js_banner_bg");
        $(me.bodyEl.dom).width( img.size() ? img.width() : 1000);
        img.on( 'load', function() {
            $(me.bodyEl.dom).width( img.width() );
        });

        for( var i=1; i<5; i++ )
            sk.initCKEditorOnPlace( 'ban_edit_lab_' + i, this.addWyswygConfig );

    },

    /**
     * Определяет является ли браузер ie9
     * @return bool
     */
    isIE9: function() {
        return /msie 9\./.test(navigator.userAgent.toLowerCase());
    },

    initComponent: function() {

        var me = this;

        // хак для ie9
        if ( me.isIE9() ) {
            for( var i=1; i<=4; i++ ) {
                me.value['text'+i] = me.value['text'+i]+'&nbsp;&nbsp;';
            }
        }

        // если есть спец параметр - заменить стандартный
        if ( me.value )
            me.subTplData = me.value;

        me.callParent();

    },

    getSubmitData: function() {

        var data = {};
        var image_w = $(".js_banner_bg").width();
        var image_h = $(".js_banner_bg").height();

        for( var i=1; i<5; i++ ) {

            var block_y = parseInt( $('#ban_dd_lab_' + i).css('top') );
            var block_x = parseInt( $('#ban_dd_lab_' + i).css('left') );

            if ( block_x < 0 )
                block_x = -block_x;

            if ( block_x > image_w )
                block_x = parseInt( image_w * 0.8 );

            if ( block_y < 0 )
                block_y = -block_y;

            if ( block_y > image_h )
                block_y = parseInt( image_h * 0.8 );

//            if( (i > 2) && image_w )
//                block_x = image_w - $('#ban_dd_lab_' + i).width() - block_x;

            data['text' + i] = $('#ban_edit_lab_' + i).html();
            data['text' + i + '_v'] = block_y;
            data['text' + i + '_h'] = block_x;

        }

        return data;
    }


});
