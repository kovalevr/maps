<?php

namespace skewer\build\Adm\FAQ;


use skewer\components\i18n\Languages;
use skewer\components\i18n\ModulesParams;
use skewer\base\section\Parameters;
use skewer\base\section\Tree;
use skewer\components\config\InstallPrototype;
use yii\helpers\ArrayHelper;

class Install extends InstallPrototype {

    private $moduleParamKeys = [
        'title_admin', 'content_admin',
        'title_user', 'content_user', 'onNotif',
        'notifTitleApprove', 'notifContentApprove', 'notifTitleReject',
        'notifContentReject'
    ];

    public function init() {
        return true;
    }// func

    public function install() {

        /** Перестройка таблиц */
        ar\FAQ::rebuildTable();

        /** Шаблонный раздел */
        $iTplSectionId = $this->addSectionByTemplate(\Yii::$app->sections->templates(), \Yii::$app->sections->tplNew(), 'faq', 'Вопросы-Ответы');

        $this->setParameter($iTplSectionId, 'object', 'content', 'FAQ');
        $this->setParameter($iTplSectionId, 'objectAdm', 'content', 'FAQ');

        $this->setParameter($iTplSectionId, 'object', 'forms', '');
        $this->setParameter($iTplSectionId, 'objectAdm', 'forms', '');

        foreach (ArrayHelper::map(Languages::getAllActive(), 'name', 'name') as $lang) {
            foreach ($this->moduleParamKeys as $key) {
                ModulesParams::setParams('data/faq', $key, $lang, \Yii::t('faq', $key, [], $lang));
            }
        }

        return true;
    }// func

    public function uninstall() {

        // удаление основной таблицы
        $this->executeSQLQuery("DROP TABLE  `faq` ;");

        $iTplSections = Tree::getSectionByAlias('faq', \Yii::$app->sections->templates());

        if (!is_null($iTplSections)){
            $aSections = Parameters::getChildrenList($iTplSections);

            foreach( $aSections as $iSection )
                Tree::removeSection($iSection);

            Tree::removeSection($iTplSections);

        }

        ModulesParams::deleteByModule('faq');

        return true;
    }// func

}//class
