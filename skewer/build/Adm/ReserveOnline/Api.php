<?php

namespace skewer\build\Adm\ReserveOnline;

use skewer\base\section;
use skewer\components\i18n\Languages;

/**
 * Набор методов для настройки модуля "Бронирования гостиниц"
 * Class Module
 * @package skewer\build\Adm\ReserveOnline
 */
class Api {

    /** Имя метки параметров настройки раздела */
    const ADM_GROUP_NAME = 'ReserveOnline';

    /** Имя метки параметров настройки раздела для внутреннего виджета */
    const ADM_WIDGET_GROUP_NAME = 'ReserveOnlineWidget';

    /** Имя параметра настройки id гостиницы */
    const ADM_ID_HOTEL_PARAM_NAME = 'id_hotel';

    /** Картинка акции по умолчанию */
    const DISCOUNT_DEF_IMG = 'bronline.akcii.png';

    /**
     * Получить текущий id гостиницы из настроек
     * @param int $iSectionId Id Секции с модулем
     * @return string|int
     */
    public static function getHotelId($iSectionId) {

        $oParamIdHotel = section\Parameters::getByName($iSectionId, self::ADM_GROUP_NAME, self::ADM_ID_HOTEL_PARAM_NAME);
        return ($oParamIdHotel) ? $oParamIdHotel->value : '';
    }

    /**
     * Получение значения параметра настройки для состояния модуля: виджет
     * @param string $ParamName Имя параметра
     * @return string|int
     */
    public static function getParamForWidget($ParamName) {

        $oParam = section\Parameters::getByName(\Yii::$app->sections->languageRoot(), self::ADM_WIDGET_GROUP_NAME, $ParamName);
        return ($oParam) ? $oParam->value : '';
    }

    /**
     * Обновление параметров модуля в разделе
     * @param int $iSectionId Id раздела
     * @param string $sHotelId Id гостиницы
     * @param int $iShowDiscount Показывать акцию?
     * @param string $sDiscountImg Изображение акции
     * @param string $sDiscountLink Ссылка акции
     */
    public static function updateModuleParams($iSectionId, $sHotelId, $iShowDiscount, $sDiscountImg, $sDiscountLink) {

        $sLang = section\Parameters::getLanguage($iSectionId);

        // Сохранить Id гостиницы
        self::setSectionParam([
                                  'name'   => self::ADM_ID_HOTEL_PARAM_NAME,
                                  'group'  => self::ADM_GROUP_NAME,
                                  'value'  => $sHotelId,
                                  'parent' => $iSectionId,
                              ]);

        // Сохранить показ акции
        self::setLanguageParam([
                                  'name'   => 'show_discount',
                                  'group'  => self::ADM_WIDGET_GROUP_NAME,
                                  'value'  => $iShowDiscount,
                              ], [$sLang]);

        // Сохранить изображение акции
        self::setLanguageParam([
                                  'name'   => 'discount_img',
                                  'group'  => self::ADM_WIDGET_GROUP_NAME,
                                  'value'  => $sDiscountImg,
                              ], [$sLang]);

        // Сохранить ссылку для акции
        self::setLanguageParam([
                                   'name'   => 'discount_link',
                                   'group'  => self::ADM_WIDGET_GROUP_NAME,
                                   'value'  => $sDiscountLink,
                               ], [$sLang]);

        /* Вывод параметра в раздел для пользовательского слоя */
        $oParamContent = section\Parameters::getByName($iSectionId, '.layout', 'content', true);

        if ($oParamContent and (stripos(','. $oParamContent->value .',', self::ADM_GROUP_NAME) === false)) {

            $sContent  = ',' . $oParamContent->value . ',';
            $iRepCount = 0;
            $sContent  = trim(str_ireplace(',content,', ',content,' . self::ADM_GROUP_NAME . ',', $sContent, $iRepCount), ',');
            if (!$iRepCount)
                $sContent .= ($sContent) ? ',' . self::ADM_GROUP_NAME : self::ADM_GROUP_NAME;

            self::setSectionParam([
                                      'parent' => $iSectionId,
                                      'value'  => $sContent,
                                  ] + $oParamContent->getAttributes());
        }
    }

    /** Установка/обновление параметра раздела */
    public static function setSectionParam($aData) {

        if ( !isset($aData['parent']) or !isset($aData['name']) or !isset($aData['group']) )
            return;

        $oParam = section\Parameters::getByName($aData['parent'], $aData['group'], $aData['name']);
        if (!$oParam)
            $oParam = section\Parameters::createParam($aData);
        else
            $oParam->setAttributes($aData);

        $oParam->save();
    }

    /**
     * Установка/обновление языкового параметра
     * @param array $aData Данные параметра
     * @param array $aLangs Массив языковых меток, куда он добавляется. Если пуст, то во все.
     */
    public static function setLanguageParam(array $aData, array $aLangs = []) {

        ($aLangs) or ($aLangs = Languages::getAllActiveNames());

        // Установить значение для каждой языковой ветки, если не задан языковой раздел напрямую
        foreach($aLangs as $sLang) {
            $aData['parent'] = \Yii::$app->sections->getValue(section\Page::LANG_ROOT, $sLang);
            self::setSectionParam($aData);
        }

        // Установить языковой параметр в шаблон новой страницы
        $aData['parent'] = \Yii::$app->sections->tplNew();
        $aData['access_level'] = section\params\Type::paramLanguage;
        $aData['value'] = $aData['show_val'] = '';
        self::setSectionParam($aData);
    }

    /**
     * Найти раздел с установленным модулем бронирования для языка
     * @param string $sLang Псевдоним языка
     * @return int Id раздела
     */
    public static function getReserveOnlineSectionByLang($sLang) {

        $aParamsReserveOnline = section\Parameters::getList()
            ->group(Api::ADM_GROUP_NAME)
            ->name('objectAdm')
            ->get();

        foreach($aParamsReserveOnline as $oParam)
            if (section\Parameters::getLanguage($oParam->parent) == $sLang)
                return $oParam->parent;
    }
}