<?php

namespace skewer\build\Adm\ReserveOnline;

use skewer\build\Adm;
use skewer\base\ui;

/**
 * Модуль настроек бронирования гостиниц онлайн
 * Class Module
 * @package skewer\build\Adm\ReserveOnline
 */
class Module extends Adm\Tree\ModulePrototype {

    /** Состояние: Инициализация */
    protected function actionInit() {

        $oForm = ui\StateBuilder::newEdit();

        $oForm->fieldString(Api::ADM_ID_HOTEL_PARAM_NAME, \Yii::t('ReserveOnline', 'id_hotel'));
        $oForm->fieldCheck('show_discount', \Yii::t('ReserveOnline', 'show_discount'), ['groupTitle' => \Yii::t('ReserveOnline', 'widget_group')]);
        $oForm->field('discount_img', \Yii::t('ReserveOnline', 'discount_img'), 'file', ['groupTitle' => \Yii::t('ReserveOnline', 'widget_group')]);
        $oForm->fieldString('discount_link', \Yii::t('ReserveOnline', 'discount_link'), ['groupTitle' => \Yii::t('ReserveOnline', 'widget_group')]);

        $oForm->setValue([
                             'show_discount'              => Api::getParamForWidget('show_discount'),
                             'discount_img'               => Api::getParamForWidget('discount_img'),
                             'discount_link'              => Api::getParamForWidget('discount_link'),
                             Api::ADM_ID_HOTEL_PARAM_NAME => Api::getHotelId($this->sectionId()),
                         ]);

        $oForm->buttonSave('saveSettings');

        return $this->setInterface($oForm->getForm());
    }

    /** Действие: Сохранение настроек */
    protected function actionSaveSettings() {

        Api::updateModuleParams($this->sectionId(),
                                $this->getInDataVal(Api::ADM_ID_HOTEL_PARAM_NAME),
                                $this->getInDataVal('show_discount'),
                                $this->getInDataVal('discount_img'),
                                $this->getInDataVal('discount_link')
        );
        $this->actionInit();
    }

}
