<?php
/* main */

use skewer\base\site\Layer;

$aConfig['name']         = 'ReserveOnline';
$aConfig['version']      = '1.0';
$aConfig['title']        = 'Бронирование гостиниц';
$aConfig['description']  = 'Модуль бронирования гостиниц online';
$aConfig['revision']     = '0001';
$aConfig['layer']        = Layer::ADM;
$aConfig['useNamespace'] = true;

$aConfig['dependency'] = [
    ['ReserveOnline', Layer::PAGE],
];

return $aConfig;
