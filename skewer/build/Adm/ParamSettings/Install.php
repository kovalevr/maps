<?php

namespace skewer\build\Adm\ParamSettings;

use skewer\base\section\Parameters;
use skewer\components\config\InstallPrototype;

class Install extends InstallPrototype {

    public function init() {
        return true;
    }

    public function install() {

        /** Обойдем все параметры, вынесенные для редактирования в модуле и скроем их из редакторов */
        foreach(Api::getParameters() as $oParam)
            if ($oParam->access_level != 0) {
                $oParam->access_level = 0;
                $oParam->save(false);
            }

        return true;
    }

    public function uninstall() {
        return true;
    }
}