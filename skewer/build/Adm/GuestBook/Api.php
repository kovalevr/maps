<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 11.05.2016
 * Time: 16:59
 */
namespace skewer\build\Adm\GuestBook;

use skewer\build\Adm\GuestBook\ar\GuestBook;
use skewer\components\modifications\GetModificationEvent;


class Api{

    public static function getLastModification(){

        $aRow = GuestBook::find()
            ->order('id','DESC')
            ->asArray()
            ->getOne();

        if (isset($aRow['date_time']))
            return strtotime($aRow['date_time']);
        else
            return 0;

    }

    public static function className(){
        return 'skewer\build\Adm\GuestBook\Api';
    }

    public static function getLastMod(GetModificationEvent $event ) {

        $event->setLastTime( Api::getLastModification() );
    }

}