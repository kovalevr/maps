<?php

use skewer\base\site\Layer;
use \skewer\build\Tool\Rss;
use skewer\base\section\models\TreeSection;
use skewer\components\search;


$aConfig['name']     = 'News';
$aConfig['title']    = 'Новости (админ)';
$aConfig['version']  = '2.0';
$aConfig['description']  = 'Админ-интерфейс управления новостной системой';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => \skewer\build\Adm\News\models\News::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => search\Api::EVENT_GET_ENGINE,
    'class' => \skewer\build\Adm\News\models\News::className(),
    'method' => 'getSearchEngine',
];

$aConfig['events'][] = [
    'event' => Rss\Api::EVENT_REBUILD_RSS,
    'class' => Rss\Api::className(),
    'method' => 'rebuildRss'
];

$aConfig['events'][] = [
    'event' => Rss\Api::EVENT_GET_DATA,
    'class' => \skewer\build\Adm\News\models\News::className(),
    'method' => 'getRssRows',
];

return $aConfig;
