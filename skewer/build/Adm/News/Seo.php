<?php
namespace skewer\build\Adm\News;

use skewer\base\section\Tree;
use skewer\base\site\Site;
use skewer\build\Adm\News\models\News;
use skewer\components\seo\SeoPrototype;
use yii\helpers\ArrayHelper;

class Seo extends SeoPrototype{

    public static function getGroup(){
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public static function getAlias(){
        return 'newsDetail';
    }

    /**
     * @return array
     */
    public function extractReplaceLabels(){
        return array(
            'label_news_title_upper' => ArrayHelper::getValue($this->aDataEntity, 'title', ''),
            'label_news_title_lower' => $this->toLower( ArrayHelper::getValue($this->aDataEntity, 'title', '') )
        );
    }


    public function loadDataEntity(){

        if ($oNews = News::findOne($this->iEntityId)){
            $this->aDataEntity = $oNews->getAttributes();
        }

    }

    /**
     * @inheritdoc
     */
    protected function getSearchClassName(){
        return Search::className();
    }

    /**
     * @inheritdoc
     */
    public function getRecordWithinEntityByPosition($iPosition){

        $aResult = News::find()
            ->where(['parent_section' => $this->iSectionId])
            ->orderBy(['publication_date' => SORT_DESC])
            ->limit(1)->offset($iPosition)
            ->all();

        if (!isset($aResult[0]))
            return false;

        /** @var News $oCurrentRecord */
        $oCurrentRecord = $aResult[0];
        $this->setDataEntity($oCurrentRecord->getAttributes());

        $aRow = array_merge($oCurrentRecord->getAttributes(),[
            'url' => Site::httpDomain() . \Yii::$app->router->rewriteURL($oCurrentRecord->getUrl()),
            'seo' => $this->parseSeoData()
        ]);

        return $aRow;

    }

    /**
     * @inheritdoc
     */
    public function doExistRecord($sPath){

        $sTail = '';
        Tree::getSectionByPath($sPath, $sTail);
        $sTail = trim($sTail, '/');

        return ($oRecord = News::getPublicNewsByAlias($sTail))
            ? $oRecord->id
            : false
        ;

    }

}