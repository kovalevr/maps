<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 11.05.2016
 * Time: 16:59
 */
namespace skewer\build\Adm\Order;

use skewer\build\Adm\Order\ar\Order;
use skewer\components\modifications\GetModificationEvent;

class Api {

    public static function getLastModification(){

        $aRow = Order::find()
            ->order('id','DESC')
            ->asArray()
            ->getOne();

        return (isset($aRow['date'])) ? strtotime($aRow['date']) : 0;
    }

    public static function className() {
        return 'skewer\build\Adm\Order\Api';
    }

    public static function getLastMod(GetModificationEvent $event) {

        $event->setLastTime( self::getLastModification() );
    }

    /**
     * Получить информацию о заказе/заказах: число позиций, общую стоимость
     * @param int|array $mOrderId
     * @return array Проиндексированный по id_order массив с полями id_order, count, sum
     */
    public static function getGoodsStatistic($mOrderId) {

        if (!$mOrderId) return [];

        return ar\Goods::find()
                ->fields('id_order, COUNT(`id`) AS count, SUM(`total`) AS sum')
                ->where('id_order', $mOrderId)
                ->groupBy('id_order')
                ->index('id_order')
                ->asArray()
                ->getAll();
    }

    /**
     * Получить общую стоимость позиций заказа
     * @param int $iOrderId
     * @return int
     */
    public static function getOrderSum($iOrderId) {

        if ($aGoodsSatistic = self::getGoodsStatistic($iOrderId))
            return $aGoodsSatistic[$iOrderId]['sum'];

        return 0;
    }
}