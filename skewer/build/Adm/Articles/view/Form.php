<?php

namespace skewer\build\Adm\Articles\view;


use skewer\build\Adm\Articles\Module;
use skewer\build\Page\Articles\Model\ArticlesRow;
use skewer\components\ext\view\FormView;
use skewer\components\seo;

class Form extends FormView{

    /** @var ArticlesRow */
    public $item;

    /**
     * Выполняет сборку интерфейса
     * @return void
     */
    function build() {

        /** @var Module $this->_module */

        $this->_form
            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('articles', 'field_title'), 'string')
            ->field('author', \Yii::t('articles', 'field_author'), 'string')
            ->field('publication_date', \Yii::t('articles', 'field_date'), 'datetime')
            ->field('announce', \Yii::t('articles', 'field_preview'), 'wyswyg')
            ->field('full_text', \Yii::t('articles', 'field_fulltext'), 'wyswyg')
            ->field('on_main', \Yii::t('articles', 'field_on_main'), 'check')
            ->field('active', \Yii::t('articles', 'field_active'), 'check')
            ->field('hyperlink', \Yii::t('articles', 'field_hyperlink'), 'string')
            ->field('articles_alias', \Yii::t('articles', 'field_alias'), 'string')

            ->setValue( $this->item->getData() )

            ->buttonSave()
            ->buttonCancel()
        ;

        if ( $this->item->id ) {

            $this->_form
                ->buttonSeparator( '-' )
                ->buttonDelete()
            ;

        }

        // добавление SEO блока полей
        seo\Api::appendExtForm( $this->_form, new \skewer\build\Adm\Articles\Seo($this->item->id, $this->_module->sectionId(), $this->item->getData()),['none_search', 'seo_gallery'] );


    }
}