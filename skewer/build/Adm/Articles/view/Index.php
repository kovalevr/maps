<?php

namespace skewer\build\Adm\Articles\view;


use skewer\components\ext\view\ListView;

class Index extends ListView {

    /** @var array News[] */
    public $items = [];

    /**
     * @inheritdoc
     */
    function build() {

        $this->_list

            ->field('id', 'ID', 'hide')
            ->field('title', \Yii::t('articles', 'field_title'), 'string', array('listColumns' => array('flex' => 3)))
            ->field('author', \Yii::t('articles', 'field_author'), 'string')
            ->field('publication_date', \Yii::t('articles', 'field_date'), 'string', array('listColumns' => array('flex' => 2)))
            ->field('active', \Yii::t('articles', 'field_active'), 'check')
            ->field('on_main', \Yii::t('articles', 'field_on_main'), 'check')

            ->setValue( $this->items, $this->onPage, $this->page, $this->total )

            ->setEditableFields( array('active','on_main'), 'fastSave' )

            ->buttonRowUpdate()
            ->buttonRowDelete()

            ->buttonAddNew('show')

        ;

    }

}